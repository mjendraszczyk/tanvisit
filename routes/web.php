<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


$front_prefix = 'front_';

Route::get('/', function () {
    return redirect()->route('login');
    //return view('welcome');
});

Auth::routes();
/* Panel admina routing */
Route::group(['middleware' => ['auth']], function () {
    $admin_path = 'backoffice';
    $admin_prefix = 'admin_';
    $controllers = ['Countries','Callendars','Users','Employees','Services','Subscriptions','Visits','Configurations','Sms', 'Roles', 'Companies', 'Settings','ServicesCategories','ServicesPrices', 'Customers', 'Openings', 'Galleries', 'Reviews', 'VisitsQuotes', 'Users','EmailTemplates'];

    foreach ($controllers as $controller) {
        //index
        Route::get($admin_path.'/'.str_slug($controller), 'admin\Admin'.$controller.'Controller@index')
        ->name($admin_prefix.strtolower($controller).'_index');
        //create
        Route::get($admin_path.'/'.str_slug($controller).'/create', 'admin\Admin'.$controller.'Controller@create')->name($admin_prefix.strtolower($controller).'_create');
       //edit
        Route::get($admin_path.'/'.str_slug($controller).'/{id}/edit', 'admin\Admin'.$controller.'Controller@edit')->name($admin_prefix.strtolower($controller).'_edit');
       //update
        Route::put($admin_path.'/'.str_slug($controller).'/{id}/edit', 'admin\Admin'.$controller.'Controller@update')->name($admin_prefix.strtolower($controller).'_update');
        //store
        Route::post($admin_path.'/'.str_slug($controller).'/create', 'admin\Admin'.($controller).'Controller@store')->name($admin_prefix.strtolower($controller).'_store');
        //delete
        Route::delete($admin_path.'/'.str_slug($controller).'/{id}/destroy', 'admin\Admin'.$controller.'Controller@destroy')->name($admin_prefix.strtolower($controller).'_destroy');
    }
    Route::get('/home', function() {
    return redirect()->route('admin_callendars_index');
})->name($admin_prefix.'home_index');



Route::post($admin_path.'/visits/history/filtr', 'admin\AdminVisitsController@filtr')->name('admin_visits_history_filter');


/**
 * Wizyty > cron
 */
Route::get($admin_path.'/visits/cron', 'admin\AdminVisitsController@cron')->name('admin_visits_cron');

/**
 * Raporty > pracownicy
 */
Route::get($admin_path.'/raports/employees', 'admin\AdminRaportsController@employees')->name('admin_raport_employees');
Route::get($admin_path.'/raports/employees/export', 'admin\AdminRaportsController@employees_export')->name('admin_raports_employees_export');

/**
 * Raporty > klienci
 */
Route::get($admin_path.'/raports/customers', 'admin\AdminRaportsController@customers')
->name('admin_raport_customers');
Route::get($admin_path.'/raports/customers/export', 'admin\AdminRaportsController@customers_export')->name('admin_raports_customers_export');
Route::get($admin_path.'/customers/export', 'admin\AdminCustomersController@export')->name('admin_customers_export');

Route::get($admin_path.'/raports/customers/{id_customer}/show', 'admin\AdminRaportsController@customer_detail')->name('admin_raports_customer_detail');

Route::get($admin_path.'/raports/employees/{id_employee}/show', 'admin\AdminRaportsController@employee_detail')->name('admin_raports_employee_detail');


Route::post($admin_path.'/customers/filter', 'admin\AdminRaportsController@customers_filter')->name('admin_raports_customers_filter');
Route::post($admin_path.'/employees/filter', 'admin\AdminRaportsController@employees_filter')->name('admin_raports_employees_filter');

Route::post($admin_path.'/raports/employees/{id}/show', 'admin\AdminRaportsController@employees_detail_filter')->name('admin_raports_employees_details_filter');

Route::post($admin_path.'/raports/customers/{id}/show', 'admin\AdminRaportsController@customers_detail_filter')->name('admin_raports_customers_details_filter');

/**
 * Zapytaj o termin > filtr
 */
Route::post($admin_path.'/visitsquotes/filter', 'admin\AdminVisitsQuotesController@filtr')->name('admin_visitsquotes_filter');


/** 
 * Kalendarz > filtr
 */
Route::post($admin_path.'/callendars', 'admin\AdminCallendarsController@filter')->name('admin_callendars_filter');

Route::get('/visits/history', 'admin\AdminVisitsController@history')->name($admin_prefix.'visits_history');

/**
 * API
 */

 Route::post($admin_path.'/api/callendars/{id_callendar}/{id_employee}/{id_service}', 'admin\AdminCallendarsController@getVisitsByFilter')->name($admin_prefix.'callendars_api_visits');


Route::get($admin_path.'/api/customers/{phone}/name', 'admin\AdminCustomersController@getNameByPhone')->name($admin_prefix.'customers_api_name');

Route::get($admin_path.'/api/servicesprices/{id}/{id_company}/time', 'admin\AdminServicesPricesController@getServiceTime')->name($admin_prefix.'services_api');

Route::get($admin_path.'/api/customers/phones', 'admin\AdminCustomersController@getPhones')->name($admin_prefix.'customers_api_phones');

Route::post($admin_path.'/api/visits/{id}/update', 'admin\AdminVisitsController@apiVisits')->name($admin_prefix.'visits_api_update');
   

/** 
 * Upload > croped
 */

Route::post($admin_path.'/galleries/croped', 'admin\AdminGalleriesController@cropUpload')->name('admin_galleries_croped');


});

/**
 * Frontend
 */


Route::get('/profil/{id}-{name}', 'front\FrontCompaniesController@profil')->name($front_prefix.'profil');

Route::get('/profil/{id}/widget', 'front\FrontCompaniesController@renderWidget')->name($front_prefix.'profil_widget');


Route::post('/profil/{id}-{name}/review', 'front\FrontReviewsController@store')->name($front_prefix.'profil_reviews_store');

Route::post('/profil/{id}-{name}/visit', 'front\FrontVisitsController@store')->name($front_prefix.'profil_visits_store');

Route::get('/visits/accept/{id}', 'front\FrontVisitsController@acceptVisit')->name($front_prefix.'visits_accept');

Route::post('/visits/accept/{id}', 'front\FrontVisitsController@processVisit')->name($front_prefix.'visits_process');

Route::get('/visits', 'front\FrontVisitsController@index')->name($front_prefix.'visits_index');

Route::post('/visits/store', 'front\FrontVisitsController@store')->name($front_prefix.'visits_store');

Route::get('/visits/cron','front\FrontVisitsController@cronSmsEmail')->name($front_prefix.'visits_cron');




