﻿<link rel="stylesheet" href="{{asset('admin/css/bootstrap.min.css')}}">
<div style="text-align:center;background:#fff;width:100%;padding:25px 0;">
    {{-- <img src="{{asset('admin/img/logo.png')}}" /> --}}
    <h3>{{env('APP_NAME')}}</h3>
</div>
<div class="container">
    <div style="background: #2664d8;color:#fff;padding:50px 0;">
        <h1 style="margin:50px 0;display:block;text-align:center;">{{$temat}}</h1>
    </div>
    <div class='card' style="border:0px;border-radius:0px;width:85%;margin:20px auto;display:block;">
        <div class='card-body'>
            <div style="color:#000000;text-align:center;width:100%;display:block;margin:25px 0;">
                @if($konto['custom'] == '1')
                {{$konto['content']}}
                @else
                Potwierdzamy Twoją wizytę {{$konto['service']}}: {{$konto['datetime_from']}}.
                {{-- <strong style="color:#000000;">Szczegóły wizyty</strong>
                <br /> --}}
                {{-- Termin: {{$konto['datetime_from']}}
                <br />
                Usługa: {{$konto['service']}}
                <br />
                Status: {{$konto['status']}}
                <br />
                Firma: {{$konto['company']}}
                <br />
                Pracownik: {{$konto['employee']}} --}}
                @endif
                <br>
            </div>
        </div>
    </div>

    <div style="width:100%;clear:both;text-align:center;background:#eee;padding:50px 0;">
        {{-- Pozdrawiam,<br> --}}
        &copy; {{date('Y')}} {{env('APP_NAME')}}
    </div>
</div>