<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ env('APP_NAME') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('front/js/app.js') }}" defer></script>
    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">




    <!-- Styles -->
    <link href="{{ asset('front/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('front/css/global.css') }}" rel="stylesheet">
    <link href="{{ asset('front/css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('front/css/owl.theme.default.min.css') }}" rel="stylesheet">
    

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin="" />

 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css"
    integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />


    <script src="{{asset('admin/js/jquery.js')}}"></script>
        <script src="{{asset('admin/js/moment.js')}}"></script>

        <script src="{{asset('admin/js/growl.js')}}"></script>
        
        
        <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
            integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
            crossorigin=""></script>

        @if(Route::currentRouteName() == 'front_profil')
        
        <script src="{{ asset('front/js/widget-tanvisit.js') }}"></script>
        
        @endif

      {{-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script> --}}
        <script src="{{ asset('admin/js/app.js') }}"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"
            integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>

        <script src="{{ asset('front/js/custom.js') }}"></script>
        <script src="{{ asset('admin/js/jquery-ui.js') }}"></script>
      
        

         

        <script src="{{asset('admin/js/fullCalendar.js')}}"></script>
        <script src="{{asset('admin/js/fullCalendar-locale-all.js')}}"></script>
        <script src="{{asset('admin/js/tinymce/tinymce.min.js')}}"></script>
        <script src="{{asset('front/js/owl.carousel.min.js')}}"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar profil">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{asset('front/img/profil_logo_top.png')}}"/>
                </a>
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{asset('front/img/profil_logo_top2.png')}}" />
                </a>
                {{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button> --}}

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
<li class="nav-item">
    <a href="{{route('front_visits_index')}}" class="active nav-link">Zarezerwuj wizytę</a>
</li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                       
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Logowanie') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Rejestracja') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main>
            @yield('content')
        </main>
    </div>
    <script type="text/javascript">
//     $(function () {
// $( "#dialog" ).dialog({
// autoOpen: false
// });


 
$(document).ready(function() {

$("#root-tanvisit").click(function() {
//alert("K");
// $("#dialog").dialog('open');
if($('#exampleModalCenter').is(':visible')) {

} else {
$('#exampleModalCenter').modal()
}

});

@if(Route::currentRouteName() == 'front_profil') 
var lat = {{$companies->lat}};//$("#lat").val()
var lon = {{$companies->lon}};//$("#lon").val()
@else
var lat = 52.3009024;//$("#lat").val()
var lon = 19.7999369;//$("#lon").val()
@endif

var map = L.map('map').setView([lat, lon], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
attribution: '',
maxZoom: 18,
id: 'mapbox/streets-v11',
tileSize: 512,
zoomOffset: -1,
accessToken: 'pk.eyJ1IjoibWFnZXMiLCJhIjoiY2tyZ2VjbDd3MDJnNTJ2bzVpNDYxa2xhNyJ9.1rwLEUSxxrz0cSjIYQd7wQ'
}).addTo(map);


var icon = L.divIcon({
iconSize:null,
html:`<div class="btn-floating icon" style="background-image:url({{asset('front/img/marker.png')}});">
</div>`});

// var marker = L.marker([data.lat, data.lon],{icon:
// icon}).addTo(mymap);
// });

L.marker([lat, lon],{icon: icon}).addTo(map);

 
            
         
        });

        $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        autoplay:true,
        responsiveClass:true,
        responsive:{
        0:{
        items:1
        },
        600:{
        items:3
        },
        1000:{
        items:4
        }
        }
        })

        function autocompeteFnc(source,input,output) {
        
        var src = source;
        $(input).autocomplete({
        source: function(request, response) {
        $.ajax({
        url: src,
        dataType: "json",
        data: {
        term : request.term
        },
        success: function(data) {
        response(data);
        
        }
        });
        },
        minLength: 3,
        select: function(event, ui) {
        $(output).val(ui.item.id);
        },
        autoFocus: true,
        open: function() {
        $(".ui-autocomplete").width( $(this).innerWidth() );
        
        }
        
        });
        
        }
    </script>
</body>
</html>
