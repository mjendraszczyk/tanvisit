﻿@extends('front.layouts.app')

@section('content')
<div class="container">
    <div class="card" style="margin: 25px 0;">
        <div class="card-header">
            Stan wizyty
        </div>
        <div class="card-body">
            
            <form method="POST" action="{{route('front_visits_process',['id' => $id_visit])}}">
                @csrf
                <label>Wybierz co chcesz zrobic</label>
            <select name="visits_process" onchange="showDate(this)" class="form-control">
                <option value="1">Potwierdź wizytę</option>
                <option value="0">Odwołaj wizytę</option>
                <option value="2">Inny termin</option>
            </select>
            <div class="hidden">
                <label>Wybierz inny termin</label>
                <input type="datetime" name="inny_termin" class="form-control" value="{{date('Y-m-d H:i:s')}}" />
            </div>
            <button type="submit" class="btn btn-primary">Zapisz</button>
            </form>
            <script type="text/javascript">
                    function showDate(object) {
                        if(object.value == 2)  {
                            document.querySelector(".hidden").style.display = 'block';
                        } else { 
                            document.querySelector(".hidden").style.display = 'none';
                        }
                    }
            </script>
        </div>
    </div>
    @endsection