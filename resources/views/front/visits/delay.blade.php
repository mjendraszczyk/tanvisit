﻿@extends('front.layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Wizyta
        </div>
        <div class="card-body text-center">
            <i class="fa fa-refresh big-icon"></i>
            Twoja wizyta została przeniesiona
        </div>
    </div>
    @endsection