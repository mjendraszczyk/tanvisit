﻿@extends('front.layouts.app')

@section('content')
<div class="container">
    <div class="card" style="margin: 25px 0">
        <div class="card-header">
            Wizyta
        </div>
        <div class="card-body text-center">
        <i class="fa fa-check big-icon"></i>
            Twoja wizyta potwierdzona pomyslnie
    </div>
    </div>
@endsection