﻿@extends('front.layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Zarezerwuj wizytę
        </div>
        <div class="card-body">
            
            <form method='POST' action="{{route('front_visits_store')}}">
                @csrf

<div class="col s12 m12 l12">
    @include('admin/core/notification')

    <p>
                    Chcesz się dowiedzieć czy mamy wolny termin? Wypełnij poniższe pola i poczekaj na kontakt z naszej strony.
                </p>
    <label>Usługa</label>
    <select name="id_service"   onchange="changeService(this)" onload="changeService(this)" class="form-control">
        @foreach ($services as $service)
        <option value="{{$service->id_service}}">{{$service->name}}
        </option>
        @endforeach

    </select>
 <script>
    function changeService(object) {
        $.get("/backoffice/api/servicesprices/"+object.value+"/time/", function(data) {
            console.log(data);
            document.querySelector("#hours").value = data;
            setEventDate();
        })
        
    }
    changeService(document.querySelector("#id_service"));
</script>
 

    <div class="card" style="margin: 25px 0;">
        <div class="card-header">
            Termin
        </div>
        <div class="card-body">

            <div class="row">
                <div class="col-md-4">
                    <label>Dzień:</label>
                    <input onchange="setEventDate()" type="date" class="form-control" name="date" id="date"
                        value="{{date('Y-m-d')}}">
                </div>
                <div class="col-md-4">
                    <label>Godzina</label>
                    <select onchange="setEventDate()" name="hour" id="hour" class="form-control">
                        @for ($i=0;$i<24;$i++) @if($i < 10) <option value="0{{$i}}">0{{$i}}</option>
                            @else
                            <option value="{{$i}}">{{$i}}</option>
                            @endif
                            @endfor
                    </select>
                </div>
                <div class="col-md-4">
                    <label>Minuta</label>
                    <select onchange="setEventDate()" name="minute" id="minute" class="form-control">
                        @for ($j=0;$j<60;$j++) @if($j < 10) <option value="0{{$j}}">0{{$j}}</option>
                            @else
                            <option value="{{$j}}">{{$j}}</option>
                            @endif
                            @endfor
                    </select>
                </div>
            </div>
        </div>
    </div>

    {{-- <label>Termin wizyty:</label> --}}

    <div style="display: none;">
    <input id="datetimepicker_from" type="datetime-local" name="datetime_from" class="form-control"
        @if(empty(old('datetime_from'))) value="{{date('Y-m-d H:i')}}" @else value="{{old('datetime_from')}}" @endif>
 </div>
    <script type="text/javascript">
        function addHoursToDate(date, hours) {
  return new Date(new Date(date).setHours(date.getHours() + hours));
}

    function setEventDate() {
        var getDate =  document.querySelector('#date').value;
   var getHour = document.querySelector('#hour').value;
   var getMinute = document.querySelector('#minute').value;

   document.querySelector("#datetimepicker_from").value=""+getDate+" "+getHour+":"+getMinute;

//alert(getDate+ " "+getHour+":"+getMinute+":00");
var selectedDate = new Date(getDate+ " "+getHour+":"+getMinute+":00");
var newDate = addHoursToDate(selectedDate, 1);
   
document.querySelector("#hours").value = '01:00'; // czas trawania wizyty

if(newDate.getDate() < 10) {
var newDay = "0"+newDate.getDate();
} else {
var newDay = newDate.getDate();
}

if(newDate.getMonth()+1 < 10) {
var newMonth = "0"+(parseInt(newDate.getMonth())+1);
} else {
var newMonth = (parseInt(newDate.getMonth())+1);
}

if(newDate.getHours() < 10) {
var newHour = "0"+newDate.getHours();
} else {
var newHour = newDate.getHours();
}


if(newDate.getMinutes() < 10) {
var newMinute = "0"+newDate.getMinutes();
} else {
var newMinute = newDate.getMinutes();
}

   document.querySelector("#datetimepicker_to").value=newDate.getFullYear()+"-"+newMonth+"-"+newDay+" "+newHour+":"+newMinute+"";
    }
    </script>
    
    <label>Czas wizyty</label>
 
    <input id="hours" type="time" name="hours" class="form-control" @if(empty(date('H:i:s', strtotime(old('hours')))))
        value="00:00:00" step="1" @else value="{{date('H:i:s', strtotime(old('hours')))}}" step="1" @endif>
    

    <div style="display:none;">
        <input type="datetime-local" id="datetimepicker_to" name="datetime_to" class="form-control"
            value="{{old('datetime_to')}}">
    </div>


    <label>Pracownik</label>
    <select name="id_employee" class="form-control">
        @foreach ($employees as $employee)
        <option value="{{$employee->id_employee}}">{{$employee->firstname}}
            {{$employee->lastname}}
        </option>
        @endforeach

    </select>


    <label>Telefon klienta</label>
    
    <input type="text" name="phone" id="phone" class="form-control" value="{{old('phone')}}">
    
    
<label>Imię klienta</label>
@if(Route::currentRouteName()=='admin_visits_edit' )

<input type="text" name="firstname" id="firstname" required class="form-control"
    value="{{\App\Http\Controllers\admin\AdminCustomersController::getCustomerName($visits->id_customer)}}">
@else
<input type="text" name="firstname" id="firstname" required class="form-control" value="{{old('firstname')}}">
@endif

    <script type="text/javascript">
        var getPhones = $.get('/backoffice/api/customers/phones', function (response) {
        $( "#phone" ).autocomplete({
        source: response
        });
    })
    
    
    </script>
          

                <button class="btn btn-primary btn-lg" style="margin: 25px 0;">Zarezerwuj wizytę</button>
            </form>
        </div>
    </div>
    @endsection