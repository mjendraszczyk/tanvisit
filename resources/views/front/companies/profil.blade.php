﻿@extends('front.layouts.app')

@section('content')
<div class="container">
    
    {{-- {{dd($companies)}} --}}
    <div class="row">
    <div class="col-md-8">
        <div class="company_name">
    <h1 class="section-title">
        {{$companies->name}}
    </h1>
    @if($companies->certificate)
    <div class="cert">
        Certyfikat TanExpert
    </div>
    @endif
    </div>
    <p class="address">
        <i class="fa fa-map-marker" aria-hidden="true"></i> 
        {{$companies->address}}, {{$companies->postalcode}} {{$companies->city}}
    </p>
    
 
  @if($countGallery > 0)
<div class="feature_gallery" style="background-image: url({{asset('front/img/uploads/galleries')}}/{{$featureGallery->filename}})">
</div>
@else
<div class="feature_gallery">
</div>
@endif
{{-- <img src="{{asset('front/img/profil_feature_image.png')}}" /> --}}
<div class="block">
    <h1 class="section-title">Opis</h1>

    <div class="inner-block">
        {{$companies->description}}
    
    </div>

    <div class="block">
        <h1 class="section-title">Cennik</h1>
         
  

        @foreach ($services_prices as $sp)
        
            <p class="list-subtite bold gold-text">{{App\Http\Controllers\admin\AdminServicesCategoriesController::getServiceCategoryName($sp->id_service_category)}}</p>
            <ul class="services_price">
                @foreach (App\Http\Controllers\front\FrontCompaniesController::getServicesFromCategoryCompany($id, $sp->id_service_category) as $sfcc)
                    <li>
                    <span class="name">
                        {{$sfcc->name}} 
                        </span>
                        <span class="price">
                        {{\App\Http\Controllers\Controller::formatPrice($sfcc->price)}}
                        </span>
                        </li>
                    
                    @endforeach
            </ul>
        @endforeach
        
    </div>

    <div class="block">
        <h1 class="section-title">Galeria</h1>
        <div class="owl-carousel owl-theme">
    
            @foreach ($galleries as $gallery)
                <div class="item">
                    <a href="{{asset('front/img/uploads/galleries')}}/{{$gallery->filename}}" data-fancybox="gallery">
                        <img src="{{asset('front/img/uploads/galleries')}}/{{$gallery->filename}}" />
                    </a>
                </div>
            @endforeach
        </div>
    </div>

    <div class="block">
        <h1 class="section-title">Opinie</h1>
        <p class="bold">Dbamy aby opinie w naszym serwisie były w 100% prawdziwe.</p>
         
        <div class="reviews-form">
        
            <form class="review_form" method="POST" action="{{route('front_profil_reviews_store',['id'=>$id,'name'=>str_slug('name')])}}">
                @csrf
                {{-- @include('backend/_main/message') --}}
                <span class="ratings"><i class="fa fa-star yellow add"></i> <i class="fa fa-star yellow add"></i> <i
                        class="fa fa-star yellow add"></i> <i class="fa fa-star yellow add"></i> <i
                        class="fa fa-star yellow add"></i></span>
                <input name="rate" type="hidden" value="5" id="ranga" class="col-sm-12
                        form-control">
                <input class="form-control" type="text" name="name" placeholder="Imię i nazwisko">
                <textarea class="form-control" name="comment" placeholder="Treść"></textarea>
                @if(Auth::check())
                <input type="hidden" name="id_user" value="{{Auth::user()->id}}">
                @else
                <input type="hidden" name="id_user" value="">
                @endif
                <input type="hidden" name="id_company" value="{{$id}}">
                {{-- <input type="hidden" name="id_ekspert" value="{{$id_ekspert}}"> --}}
                <input type="submit" class="btn btn-primary btn-outline" value="Dodaj">
            </form>
        </div>
        <div class="reviews-content">
            @if(count($reviews) > 0)
            <ul>
                @foreach($reviews as $review)
                <li>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="text-center">
                                {{-- <img src="{{asset('/img/ekspertka_user.png')}}" alt=""> --}}
                                <i class="fa fa-user"></i>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <h6>
                                 {{$review->name}} 
                            </h6>
                            <p>
                                {{$review->comment}}
                            </p>
                        </div>
                        <div class="col-md-2">
                            <div class="komentarz-ocena">
                                @for($i=0;$i<$review->rate;$i++)
                                    <i class="fa fa-star yellow add"></i>
                                    @endfor
                            </div>
                            <p class="komentarz-data">
                                  {{$review->created_at}} 
                            </p>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        
            @else
            <div class="alert">
                Brak opini
            </div>
        
            @endif
        </div>
    </div>
</div>

    </div>
    <div class="col-md-4">
        <div id="map"></div>
    {{-- <img src="{{asset('front/img/profil_map.png')}}" /> --}}
        <h2 class="section-subtitle text-center">
            Kontakt i godziny otwarcia
        </h2>
        <div class="text-center">
           <h3> <img src="{{asset('front/img/profil_phone_icon.png')}}"/> 
        {{$companies->phone}}
        </h3>
        </div>

        <ul class="nomargin nopadding openlist">
            @foreach ($openings as $opening)
            @if($opening->day > 0)
            <li>
                <span class="name">
                    {{$days[$opening->day]}}
                    </span>
                    <span class="value">
                        @if($opening->from == $opening->to) 
                        Zamknięte
                        @else
                        {{$opening->from}} - {{$opening->to}}
                        @endif
                    </span>
            </li>
            @endif
            @endforeach
            @foreach ($openings as $opening)
            @if($opening->day == 0)
            <li>
                <span class="name">
                    {{$days[$opening->day]}}
                </span>
                <span class="value">
                    @if($opening->from == $opening->to)
                    Zamknięte
                    @else
                    {{$opening->from}} - {{$opening->to}}
                    @endif
                </span>
            </li>
            @endif
            @endforeach
        </ul>

        @include('front.companies.widget')
      
        @if($companies->store == 1)
        <div class="box-salon text-center">
            <img src="{{asset('front/img/profil_store.png')}}" alt="">
            <span>Salon stacjonarny</span>
        </div>
        @endif
        <h2 class="text-center">Znajdź nas na</h2>
        <div class="text-center">
        <a href="{{$companies->facebook}}" target="_blank">
            <img src="{{asset('front/img/profil_facebook.png')}}" alt="">
        </a>
        <a href="{{$companies->instagram}}" target="_blank">
            <img src="{{asset('front/img/profil_instagram.png')}}" alt="">
        </a>
        </div>
        
    </div>
    </div>
    </div>
    <footer class="footer">
        <div class="container">
        <p>&copy; {{date('Y')}} - Kurkowa5. Projekt i wykonanie VirtualPeople
        <a href="{{$companies->facebook}}" target="_blank"><img src="{{asset('front/img/facebook-white.png')}}" class="inline margin0-5"/></a>
        <a href="{{$companies->instagram}}" target="_blank"><img src="{{asset('front/img/instagram-white.png')}}" class="inline margin0-5"/></a>
        </p>
        </div>
    </footer>
     
   <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                @include('front.companies.widget')
            </div>
       
        </div>
    </div>
</div>
    <div id="root-tanvisit"></div>
   @endsection