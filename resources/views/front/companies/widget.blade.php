﻿<link href="{{ asset('front/css/app.css') }}" rel="stylesheet">
<link href="{{ asset('admin/css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ asset('front/css/global.css') }}" rel="stylesheet">
<div class="zapytaj-termin">
    <h2>
        Zapytaj o termin :)
    </h2>
    <p>
        Chcesz się dowiedzieć czy mamy wolny termin? Wypełnij poniższe pola i poczekaj na kontakt z naszej strony.
    </p>
    <form method="POST" action="{{route('front_profil_visits_store',['id'=>$id,'name'=>str_slug(App\Http\Controllers\admin\AdminCompaniesController::getCompanyName($id))])}}">
        @csrf
        @include('admin/core/notification')
        <label for="id_service">
            Usługa
        </label>
        <select name="id_service" id="id_service" onchange="changeService(this)" onload="changeService(this)" data-id_company="{{$id}}" class="form-control">
            @foreach ($all_services_prices as $service)
            <option value="{{$service->id_service}}">
                {{App\Http\Controllers\admin\AdminServicesCategoriesController::getServiceCategoryName($service->id_service_category)}} > 
                {{$service->name}}</option>
            @endforeach
        </select>
        <label for="id_employee">
            Pracownik
        </label>
        <select name="id_employee"  class="form-control">
            @foreach ($employees as $employee)
            <option value="{{$employee->id_employee}}">{{$employee->firstname}}</option>
            @endforeach
        </select>
        <label for="phone">
            Telefon
        </label>
        <select name="prefix_phone" class="form-control" style="width:24%;display:inline-block;">
            @foreach ($prefix_phone as $key => $prefix)
                <option value="{{$prefix}}">{{$key}}</option>
            @endforeach
        </select>
        <input type="number" required name="phone" value="{{old('phone')}}" class="form-control" style="width:75%;display:inline-block;">
        <label for="name">
            Imię
        </label>
        <input type="text" required name="firstname" value="{{old('firstname')}}" class="form-control">
        <h6>
            Proponowany termin
        </h6>
        <label for="date">
            Dzień
        </label>
        <input type="date" required id="date" onchange="setEventDate()" class="form-control" value="{{date('Y-m-d')}}" class="date">

        <label for="hour">
            Godzina
        </label>
        <input type="radio"   name="godzina" value="0" checked="checked" /> Dowolna
        <br>

        <input type="radio" name="godzina" value="1" />
        <input type="time" id="hour"  value="00:00" onchange="setEventDate()" style="display: inline-block;width: auto;" class="form-control">


         <div style="display: none;"> 
            <label>Czas wizyty:</label>
            <input id="hours" type="time" name="hours" class="form-control"
                value="00:00:00" step="1">
            <label>Termin wizyty:</label>
        
            
            <input id="datetimepicker_from" type="datetime" name="datetime_from" class="form-control"
                value="{{date('Y-m-d H:i')}}">
            
                {{-- <div style="display:none;"> --}}
                    <input type="datetime" id="datetimepicker_to" name="datetime_to" class="form-control"
                        value="{{date('Y-m-d H:i')}}">
                {{-- </div> --}}

         </div> 

        
        <button class="btn-primary">Umów się na wizytę</button>
    </form>
</div>
<script>
    function changeService(object) {
        $.get("/backoffice/api/servicesprices/"+object.value+"/"+object.getAttribute('data-id_company')+"/time/", function(data) {
            console.log(data);
            document.querySelector("#hours").value = data;
            setEventDate();
        })
        
    }
    changeService(document.querySelector("#id_service"));
</script>

<script type="text/javascript">
    function addHoursToDate(date, hours, minutes) {
//  return new Date(new Date(date).setHours(date.getHours() + hours));
var set_h = new Date(new Date(date).setHours(date.getHours() + hours));

var set_m = new Date(new Date(set_h).setMinutes(date.getMinutes() + minutes));
//alert(set_m);
return new Date(set_m);
}

    function setEventDate() {
      
        var getDate =  document.querySelector('#date').value;
   var getHour = document.querySelector('#hour').value;
   var getMinute = "00";

   document.querySelector("#datetimepicker_from").value=""+getDate+" "+getHour+":"+getMinute;

//alert(getDate+ " "+getHour+":"+getMinute+":00");
console.log("SELECTED DATE:"+getDate+ " "+getHour+":"+getMinute);
var selectedDate = new Date(getDate+ " "+getHour+":"+getMinute);
var time = new Date("1970-01-01 "+document.querySelector("#hours").value);
var newDate = addHoursToDate(selectedDate, parseInt(time.getHours()), parseInt(time.getMinutes()));

 

if(newDate.getDate() < 10) {
var newDay = "0"+newDate.getDate();
} else {
var newDay = newDate.getDate();
}

if(newDate.getMonth()+1 < 10) {
var newMonth = "0"+(parseInt(newDate.getMonth())+1);
} else {
var newMonth = (parseInt(newDate.getMonth())+1);
}

if(newDate.getHours() < 10) {
var newHour = "0"+newDate.getHours();
} else {
var newHour = newDate.getHours();
}


if(newDate.getMinutes() < 10) {
var newMinute = "0"+newDate.getMinutes();
} else {
var newMinute = newDate.getMinutes();
}

//alert(newDate.getFullYear()+"-"+newMonth+"-"+newDay+" "+newHour+":"+newMinute);
console.log(newDate.getFullYear()+"-"+newMonth+"-"+newDay+" "+newHour+":"+newMinute+"");
console.log(selectedDate);
   document.querySelector("#datetimepicker_to").value=newDate.getFullYear()+"-"+newMonth+"-"+newDay+" "+newHour+":"+newMinute+":00";//'2021-08-01 01:00:00';//newDate.getFullYear()+"-"+newMonth+"-"+newDay+" "+newHour+":"+newMinute+"";
    }
</script>