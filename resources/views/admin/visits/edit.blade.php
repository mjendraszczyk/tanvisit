@extends('admin.layouts.app')

@section('admin_section')
{{-- <form class="inline" method="POST" action="{{route('admin_visits_destroy',['id'=>$visits->id_visit])}}">
    @csrf
    @method('DELETE')
    <button type="submit" class="btn btn-danger">
        <i class="fa fa-remove"></i>
        Usuń wizytę
    </button>
</form> --}}
<form method="POST" action="{{route('admin_visits_update', ['id' => $visits->id_visit])}}">
    @method('PUT')
    @include('admin.visits.form')
</form>

@if(Route::currentRouteName()=='admin_visits_edit' )

@endif
@endsection
