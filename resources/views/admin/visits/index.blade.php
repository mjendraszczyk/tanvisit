﻿@extends('admin.layouts.app')
@section('title')
Wizyty
@endsection
<a href="{{route('admin_visits_create')}}" class="btn btn-success">
    Dodaj
</a>
@section('admin_section')
<table class="table">
    <thead>
        <th>ID</th>
        <th>Usługa</th>
        <th>Klient</th>
        <th>Czas od</th>
        <th>Czas do</th>
        <th class="text-right">Opcje</th>
    </thead>
    {{-- @foreach ($services_prices as $services_price)
    <tr>
        <td>
            {{$services_price->id_service_price}}
        </td>
        <td>
            {{$services_price->name}}
        </td>
        <td>
            {{$services_price->time}}
        </td>
        <td>
            {{$services_price->price}}
        </td>
        <td class="text-right">
            <a href="{{route('admin_services_edit',['id'=>$services_price->id_service])}}" class="btn btn-default">
                <i class="fa fa-edit"></i>
            </a>
            <form class="inline" action="{{route('admin_services_destroy',['id'=>$services_price->id_service])}}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-default">
                    <i class="fa fa-remove"></i>
                </button>
            </form>
        </td>
    </tr>
    @endforeach --}}
    {{ $visits->links() }}
</table>
@endsection