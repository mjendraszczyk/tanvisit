@extends('admin.layouts.app')

@section('admin_section')
    <form method="POST" action="{{route('admin_visits_store')}}">
        @include('admin.visits.form')
    </form>
@endsection