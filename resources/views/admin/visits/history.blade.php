﻿@extends('admin.layouts.app')
@section('title')
Historia Wizyt
@endsection

@section('admin_section')
<div class="panel">
    <div class="panel-default">
        <form action="{{route('admin_visits_history_filter')}}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-5">
                    <label>Data od:</label>
                    <input type="date" name="datetime_from" @if($datetime_from) value="{{$datetime_from}}" @else
                        value="{{date('Y-m-d')}}" @endif id="date_from" class="form-control">
                </div>

                <div class="col-md-5">
                    <label>Data do:</label>
                    <input type="date" name="datetime_to" @if($datetime_to) value="{{$datetime_to}}" @else
                        value="{{date('Y-m-d')}}" @endif id="date_to" class="form-control">
                </div>


                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary btn-lg"
                        style="margin:25px 0 0 0;min-width: 100%">Filtruj</button>
                </div>


            </div>
        </form>
    </div>
</div>
<table class="table">
    <thead>
        <th>ID</th>
        <th>Usługa</th>
        <th>Klient</th>
        <th>Data</th>
        <th>Status</th>
        <th class="text-right">Opcje</th>
    </thead>
    @foreach ($visits as $visit)
    <tr>
        <td>
            {{$visit->id_visit}}
    </td>
    <td>
        {{\App\Http\Controllers\admin\AdminServicesController::getServiceName($visit->id_service)}}
    </td>
    <td>
        {{\App\Http\Controllers\admin\AdminCustomersController::getCustomerName($visit->id_customer)}}
    </td>
    <td>
        {{$visit->datetime_from}}
    </td>
    <td>
        {{$visit->confirm}}
    </td>
    <td class="text-right">
        <a href="{{route('admin_visits_edit',['id'=>$visit->id_visit])}}" class="btn btn-default">
            <i class="fa fa-edit"></i>
        </a>

    </td>
    </tr>
    @endforeach
</table>
{{ $visits->links() }}
@endsection