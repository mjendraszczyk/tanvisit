@csrf

<div class="col s12 m12 l12">
    @include('admin/core/notification')
<label>Usługa</label>
 <select onchange="changeService(this)" data-id_company="{{\App\Http\Controllers\Controller::getCompanyId(Auth::user()->id)}}" onload="changeService(this)" name="id_service" id="id_service" class="form-control">
    @foreach ($services as $service)
    <option value="{{$service->id_service}}" @if(Route::currentRouteName()=='admin_visits_edit' )
        @if($service->id_service == $visits->id_service) selected="selected" @endif @endif>{{$service->name}}
    </option>
    @endforeach

</select>
<script>
    function changeService(object) {
        $.get("/backoffice/api/servicesprices/"+object.value+"/"+object.getAttribute('data-id_company')+"/time/", function(data) {
            console.log(data);
            document.querySelector("#hours").value = data;
            setEventDate();
        })
        
    }
    changeService(document.querySelector("#id_service"));
</script>

<label>Stan</label>
<select name="confirm" class="form-control">
     {{-- @if(Route::currentRouteName()=='admin_visits_edit' )
     {{$visits->confirm}}
     @endif --}}
     <option value="2" @if(Route::currentRouteName()=='admin_visits_edit' ) @if($visits->confirm == 2) selected="selected"
        @endif @endif>Niepotwierdzona
    </option>
    <option value="0" @if(Route::currentRouteName()=='admin_visits_edit' ) @if($visits->confirm == 0) selected="selected" @endif @endif>Anulowana
    </option>
    <option value="1" @if(Route::currentRouteName()=='admin_visits_edit' ) @if($visits->confirm == 1) selected="selected"
        @endif @endif>Potwierdzona
    </option>
   


</select>
<label>Notatka:</label>
@if(Route::currentRouteName()=='admin_visits_edit' )
<textarea class="form-control" name="private_note">{{$visits->private_note}}</textarea>
@else
<textarea class="form-control" name="private_note"></textarea>
@endif
<div style="display: none">
<label>Kolor:</label>


@if(Route::currentRouteName()=='admin_visits_edit' )

<input id="color" type="color" name="color" class="form-control" value="{{$visits->color}}" />
@else
<input id="color" type="color" name="color" class="form-control"
@if(empty(old('color'))) value="#000000" @else value="{{old('color')}}" @endif >
@endif
</div>

<div class="card" style="margin: 25px 0;">
    <div class="card-header">
        Termin
    </div>
    <div class="card-body">
<div class="row">
    <div class="col-md-4">
        <label>Dzień:</label>
            <input onchange="setEventDate()" type="date" class="form-control" name="date" id="date" @if(Route::currentRouteName()=='admin_visits_edit' ) value="{{date('Y-m-d', strtotime($visits->datetime_from))}}" @else value="{{date('Y-m-d')}}" @endif>
        </div>
        <div class="col-md-4">
            <label>Godzina</label>
                <select onchange="setEventDate()" name="hour" id="hour" class="form-control">
                    @for ($i=0;$i<24;$i++)
                    @if($i < 10) <option value="0{{$i}}" @if(Route::currentRouteName()=='admin_visits_edit') @if($i == date('H',strtotime($visits->datetime_from))) selected="selected" @endif @endif>0{{$i}}</option>
                    @else
                    <option value="{{$i}}" @if(Route::currentRouteName()=='admin_visits_edit') @if($i == date('H',strtotime($visits->datetime_from))) selected="selected" @endif @endif>{{$i}}</option>
                    @endif
                    @endfor
                </select>
            </div>
    <div class="col-md-4">
        <label>Minuta</label>
<select onchange="setEventDate()" name="minute" id="minute" class="form-control">
    @for ($j=0;$j<60;$j++) 
    @if($j < 10)
        <option value="0{{$j}}" @if(Route::currentRouteName()=='admin_visits_edit') @if($j == date('i',strtotime($visits->datetime_from))) selected="selected" @endif @endif>0{{$j}}</option>
        @else
        <option value="{{$j}}" @if(Route::currentRouteName()=='admin_visits_edit') @if($j == date('i',strtotime($visits->datetime_from))) selected="selected" @endif @endif>{{$j}}</option>
        @endif
    @endfor
</select>
</div>
</div>
</div>
</div>
<div style="display: none;">
<label>Termin wizyty:</label>

@if(Route::currentRouteName()=='admin_visits_edit' )
<input id="datetimepicker_from" type="datetime" name="datetime_from" class="form-control"
   value="{{$visits->datetime_from}}">
@else
<input id="datetimepicker_from" type="datetime" name="datetime_from" class="form-control"
@if(empty(old('datetime_from'))) value="{{date('Y-m-d H:i')}}" @else value="{{old('datetime_from')}}" @endif >

@endif
</div>

    <script type="text/javascript">
    function addHoursToDate(date, hours, minutes) {
//  return new Date(new Date(date).setHours(date.getHours() + hours));
var set_h = new Date(new Date(date).setHours(date.getHours() + hours));

var set_m = new Date(new Date(set_h).setMinutes(date.getMinutes() + minutes));
//alert(set_m);
return new Date(set_m);
}

    function setEventDate() {
      
        var getDate =  document.querySelector('#date').value;
   var getHour = document.querySelector('#hour').value;
   var getMinute = document.querySelector('#minute').value;

   document.querySelector("#datetimepicker_from").value=""+getDate+" "+getHour+":"+getMinute;

//alert(getDate+ " "+getHour+":"+getMinute+":00");
var selectedDate = new Date(getDate+ " "+getHour+":"+getMinute+":00");
var time = new Date("1970-01-01 "+document.querySelector("#hours").value);
var newDate = addHoursToDate(selectedDate, parseInt(time.getHours()), parseInt(time.getMinutes()));

// alert('set event');
// alert(parseInt(document.querySelector("#hours").value));
   
//document.querySelector("#hours").value = '01:00'; // czas trawania wizyty

if(newDate.getDate() < 10) {
var newDay = "0"+newDate.getDate();
} else {
var newDay = newDate.getDate();
}

if(newDate.getMonth()+1 < 10) {
var newMonth = "0"+(parseInt(newDate.getMonth())+1);
} else {
var newMonth = (parseInt(newDate.getMonth())+1);
}

if(newDate.getHours() < 10) {
var newHour = "0"+newDate.getHours();
} else {
var newHour = newDate.getHours();
}


if(newDate.getMinutes() < 10) {
var newMinute = "0"+newDate.getMinutes();
} else {
var newMinute = newDate.getMinutes();
}

   document.querySelector("#datetimepicker_to").value=newDate.getFullYear()+"-"+newMonth+"-"+newDay+" "+newHour+":"+newMinute+"";
    }
    </script>
{{-- <label>Czas wizyty do:</label> --}}

<label>Czas wizyty</label>
@if(Route::currentRouteName()=='admin_visits_edit' )

<input id="hours" type="time" name="hours" class="form-control" value="{{date('H:i:s', strtotime($visits->hours))}}" step="1" />
@else
<input id="hours" type="time" name="hours" class="form-control" @if(empty(date('H:i:s', strtotime(old('hours'))))) value="00:00:00" step="1" @else
    value="{{date('H:i:s', strtotime(old('hours')))}}" step="1" @endif>
@endif

<div style="display:none;">
<input type="datetime" id="datetimepicker_to" name="datetime_to"  class="form-control" @if(Route::currentRouteName()=='admin_visits_edit' ) value="{{$visits->datetime_to}}" @else value="{{old('datetime_to')}}" @endif>
</div>
 

<label>Pracownik</label>
<select name="id_employee" class="form-control">
    @foreach ($employees as $employee)
    <option value="{{$employee->id_employee}}" @if(Route::currentRouteName()=='admin_visits_edit' ) @if($visits->id_employee == $employee->id_employee) selected="selected" @endif @endif>{{$employee->firstname}}
        {{$employee->lastname}}
    </option>
    @endforeach

</select>

<label>Kierunkowy</label>
<select name="prefix_phone" id="prefix_phone" onchange="addPhonePrefix()" class="form-control">
    @foreach ($prefix_phone as $key => $prefix)
    <option value="{{$prefix}}" @if(Route::currentRouteName()=='admin_customers_edit' ) @if($prefix==$customers->
        prefix_phone) selected="selected" @endif @endif>{{$key}}</option>
    @endforeach
</select>

<label>Telefon klienta</label>
@if(Route::currentRouteName()=='admin_visits_edit' )

<input  pattern="[0-9]{9,16}" type="text" name="phone" onfocus="addPhonePrefix()" id="phone" onchange="getCustomerName()" class="form-control" value="{{\App\Http\Controllers\admin\AdminCustomersController::getCustomerPhone($visits->id_customer)}}">
@else
<input  pattern="[0-9]{9,16}" type="text" name="phone" onfocus="addPhonePrefix()" id="phone" onchange="getCustomerName()" onblur="getCustomerName()" onfocus="getCustomerName()" onload="getCustomerName()" class="form-control" value="{{old('phone')}}">
@endif
 <input type="hidden" id="app_url" value="{{Request::root()}}"/>
<script type="text/javascript">
function addPhonePrefix() { 
    //if() 
    document.getElementById('phone').value = document.getElementById('prefix_phone').value;
}
//addPhonePrefix();

function getCustomerName() {
    console.log("init");
    $.ajax({
        url : $("#app_url").val()+'/backoffice/api/customers/'+$('#phone').val()+'/name',
        method : 'GET',
        data: {'_token': $('meta[name="_token"]').attr('content')},
        success: function(data) {
            console.log("OK");
            console.log(data);
            $("#firstname").val(data[0]);
        }

    });
}
</script>

<label>Imię klienta</label>
@if(Route::currentRouteName()=='admin_visits_edit' )

<input type="text" name="firstname" id="firstname"  required class="form-control"
    value="{{\App\Http\Controllers\admin\AdminCustomersController::getCustomerName($visits->id_customer)}}">
@else
<input type="text" name="firstname" id="firstname"  required class="form-control" value="{{old('firstname')}}">
@endif


@if(Route::currentRouteName()=='admin_visits_edit' )
<label>Wysłac powiadomienie o aktualizacji?</label>
<br>
<label><input type="checkbox" name="update_visit" id="update_visit" value="1">Tak, wyslij SMS oraz email do klienta</label>
@endif

<script type="text/javascript">

var getPhones = $.get('/backoffice/api/customers/phones', function (response) {
    $( "#phone" ).autocomplete({
autoFocus: true,
    source: response,
    
    });
})


</script>

</div>
<div class="panel-footer">
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>
</div>