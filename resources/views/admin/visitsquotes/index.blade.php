﻿@extends('admin.layouts.app')
@section('title')
Zapytania o termin
@endsection
@section('options')
@endsection
@section('admin_section')
<div class="panel">
    <div class="panel-default">
        <form action="{{route('admin_visitsquotes_filter')}}" method="POST">
            @csrf
            <div class="row text-center">
                <div class="col-md-5">
                    <label>Data od:</label>
                    <input type="date" name="datetime_from" @if($datetime_from) value="{{$datetime_from}}" @endif
                        id="date_from" class="form-control">
                </div>

                <div class="col-md-5">
                    <label>Data do:</label>
                    <input type="date" name="datetime_to" @if($datetime_to) value="{{$datetime_to}}" @endif id="date_to"
                        class="form-control">
                </div>


                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary btn-lg"
                        style="margin:25px 0 0 0;min-width: 100%">Filtruj</button>
                </div>


            </div>
        </form>
    </div>
</div>
<table class="table">
    <thead>
        <th>ID</th>
        <th>Klient</th>
        <th>Termin</th>
        <th class="text-right">Opcje</th>
    </thead>
@foreach ($visitsquotes as $quote)
    <tr>
        <td>
            {{$quote->id_visit}}
        </td>
        <td>
        {{\App\Http\Controllers\admin\AdminCustomersController::getCustomerPhone($quote->id_customer)}}
        </td>
        <td>
            {{$quote->datetime_from}}
        </td>

        <td class="text-right">
            <form class="inline" method="POST" action="{{route('admin_visitsquotes_update',['id'=>$quote->id_visit])}}">
                @csrf
                @method('PUT')
                <button type="submit" class="btn green">
                    <i class="fa fa-check"></i>
                </button>
            </form>
            <form class="inline" method="POST" action="{{route('admin_visitsquotes_destroy',['id'=>$quote->id_visit])}}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-remove"></i>
            </button>
            </form>
        </td>
    </tr>
@endforeach
{{ $visitsquotes->links() }}
</table>
@endsection