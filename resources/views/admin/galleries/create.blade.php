@extends('admin.layouts.app')
@section('title')
Galeria - Tworzenie
@endsection
@section('options')
<a href="{{route('admin_galleries_create')}}" class="btn btn-success">
    Dodaj
</a>
@endsection
@section('admin_section')
    <form method="POST" enctype="multipart/form-data" action="{{route('admin_galleries_store')}}">
        @include('admin.galleries.form')
    </form>
@endsection