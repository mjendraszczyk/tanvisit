﻿@extends('admin.layouts.app')
@section('title')
Galeria
@endsection
@section('options')
<a href="{{route('admin_galleries_create')}}" class="btn btn-success">
    Dodaj
</a>
@endsection
@section('admin_section')
<table class="table">
    <thead>
        <th>ID</th>
        <th>Obrazek</th>
        <th>Nazwa</th>
        <th>Pozycja</th>
        <th class="text-right">Opcje</th>
    </thead>
@foreach ($galleries as $gallerie)
    <tr>
        <td>
            {{$gallerie->id_gallery}}
        </td>
        <td>
                   <img src="{{asset('front/img/uploads/galleries/'.$gallerie->filename)}}" class="img-thumbnail" style="width:120px;"/>
        </td>
        <td>
            {{$gallerie->label}}
        </td>
        <td>
            {{$gallerie->position}}
        </td>
      
        <td class="text-right">
            <a href="{{route('admin_galleries_edit',['id'=>$gallerie->id_gallery])}}" class="btn btn-default">
                <i class="fa fa-edit"></i>
            </a>
            <form class="inline" method="POST" action="{{route('admin_galleries_destroy',['id'=>$gallerie->id_gallery])}}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-remove"></i>
            </button>
            </form>
        </td>
    </tr>
@endforeach
{{ $galleries->links() }}
</table>
@endsection