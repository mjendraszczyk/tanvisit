@extends('admin.layouts.app')
@section('title')
Galeria - Edycja
@endsection
@section('options')
<a href="{{route('admin_galleries_create')}}" class="btn btn-success">
    Dodaj
</a>
@endsection
@section('admin_section')
<form method="POST" action="{{route('admin_galleries_update', ['id' => $galleries->id_gallery])}}">
    @method('PUT')
    @include('admin.galleries.form')
</form>
@endsection
