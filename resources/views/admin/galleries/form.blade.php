@csrf
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>

<style type="text/css">
img {
display: block;
max-width: 100%;
}
.preview {
overflow: hidden;
width: 160px; 
height: 160px;
margin: 10px;
border: 1px solid red;
}
.modal-lg{
max-width: 1000px !important;
}
</style>

<div class="col s12 m12 l12">
    @include('admin/core/notification')
 
  <label>Nazwa</label>
    <input required placeholder="Nazwa" name="label" @if(Route::currentRouteName()=='admin_galleries_edit' )
        value="{{$galleries->label}}" @else value="" @endif type="text" class="form-control-input form-control validate">

        
        @if(Route::currentRouteName()=='admin_galleries_edit')
        <label>Pozycja</label>
        <input required placeholder="Pozycja" name="position" 
            value="{{$galleries->position}}" type="number"
            class="form-control-input form-control validate">
            @endif


            @if(Route::currentRouteName()=='admin_galleries_edit')
            <label>Okładka</label>
            <input name="feature" @if($galleries->feature == 1) checked="checked" @endif value="1" type="checkbox"
                class="form-control-input" > Tak
            @endif

            
            <label>Obrazek</label>
            @if(Route::currentRouteName()=='admin_galleries_create' )
            <input type="file" required name="filename" 
             class="image form-control-input form-control validate" accept="image/*" onchange="loadFile(event)">
                <img id="output" class="img-fluid img-thumbnail" style="margin: 15px 0;">
             @endif

             @if(Route::currentRouteName()=='admin_galleries_edit' )
                    <img src="{{asset('front/img/uploads/galleries/'.$galleries->filename)}}" class="img-thumbnail" style="width:250px;" />
             @endif



             <script>
                var loadFile = function(event) {
                                        var output = document.getElementById('output');
                                        output.src = URL.createObjectURL(event.target.files[0]);
                                        output.onload = function() {
                                          URL.revokeObjectURL(output.src) // free memory
                                        }
                                      };
            </script>
 


 <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Wykadruj zdjęcie
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-8">
                            <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                        </div>
                        <div class="col-md-4">
                            <div class="preview"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> --}}
                <button type="button" class="btn btn-primary" id="crop">Wykadruj</button>
            </div>
        </div>
    </div>
</div>
</div>

<script>
    var $modal = $('#modal');
var image = document.getElementById('image');
var cropper;

$("body").on("change", ".image", function(e){
var files = e.target.files;
var done = function (url) {
image.src = url;
$modal.modal('show');
};


var reader;
var file;
var url;
if (files && files.length > 0) {
file = files[0];
if (URL) {
done(URL.createObjectURL(file));
} else if (FileReader) {
reader = new FileReader();
reader.onload = function (e) {
done(reader.result);
};
reader.readAsDataURL(file);
}
}
});


$modal.on('shown.bs.modal', function () {
cropper = new Cropper(image, {
aspectRatio: 1,
viewMode: 1,
preview: '.preview'
});
}).on('hidden.bs.modal', function () {
cropper.destroy();
cropper = null;
});


$("#crop").click(function(){
canvas = cropper.getCroppedCanvas({
width: 800,
height: 800,
});
canvas.toBlob(function(blob) {
url = URL.createObjectURL(blob);
var reader = new FileReader();
reader.readAsDataURL(blob); 
reader.onloadend = function() {
var base64data = reader.result; 

$.ajax({
type: "POST",
dataType: "json",
url: "{{route('admin_galleries_croped')}}",
data: {'_token': $('meta[name="csrf-token"]').attr('content'), 'image': base64data},
success: function(data){
console.log(data);
$modal.modal('hide');
//alert("Crop image successfully uploaded");
$("#output").attr('src',data.path);
$("#croped").val(data.name);
}
});
}
});
})
</script>

<input type="hidden" id="croped" name="croped">
<div class="panel-footer">
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>
</div>