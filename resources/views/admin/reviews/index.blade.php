﻿@extends('admin.layouts.app')
@section('title')
Opinie
@endsection
@section('options')

@endsection
@section('admin_section')
<table class="table">
    <thead>
        <th>ID</th>
        <th>Imię</th>
        <th>Ocena</th>
        <th>Status</th>
        <th class="text-right">Opcje</th>
    </thead>
@foreach ($reviews as $review)
    <tr>
        <td>
            {{$review->id_review}}
        </td>
        <td>
            {{$review->name}}
        </td>
        <td>
            {{$review->rate}}
        </td>
         <td>
            {{$review->status}}
        </td>
            
         
        <td class="text-right">
            <a href="{{route('admin_reviews_edit',['id'=>$review->id_review])}}" class="btn btn-default">
                <i class="fa fa-edit"></i>
            </a>
           <form class="inline" method="POST" action="{{route('admin_reviews_destroy',['id'=>$review->id_review])}}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-remove"></i>
            </button>
            </form>  
        </td>
    </tr>
@endforeach
{{ $reviews->links() }}
</table>
@endsection