@extends('admin.layouts.app')
@section('title')
Opinie - Edycja
@endsection
@section('admin_section')
<form method="POST" action="{{route('admin_reviews_update', ['id' => $id])}}">
    @method('PUT')
    @include('admin.reviews.form')
</form>
@endsection
