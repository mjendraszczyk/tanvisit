@extends('admin.layouts.app')
@section('title')
Opinie - Tworzenie
@endsection
@section('admin_section')
    <form method="POST" action="{{route('admin_reviews_store')}}">
        @include('admin.reviews.form')
    </form>
@endsection