@csrf

<div class="col s12 m12 l12">
    @include('admin/core/notification')
 
    <label>Usługa</label>
    
    <select name="status" class="form-control">
 
        <option value="0" @if(Route::currentRouteName()=='admin_reviews_edit' ) @if($reviews->status == 0) selected="selected" @endif @endif>Nieopubliowane
        </option>

        <option value="1" @if(Route::currentRouteName()=='admin_reviews_edit' ) @if($reviews->status == 1)
            selected="selected" @endif @endif>Opublikowane
        </option>
    </select>

    <label>Imię</label>
    <input required placeholder="Imię" name="name" @if(Route::currentRouteName()=='admin_reviews_edit' )
        value="{{$reviews->name}}" @else value="{{old('name')}}" @endif type="text"
        class="form-control-input form-control validate">

        <label>Komentarz</label>
        @if(Route::currentRouteName() =='admin_reviews_edit')
        <textarea required name="comment" class="form-control-input form-control validate">{{$reviews->comment}}</textarea>
            @else 
<textarea required name="comment" class="form-control-input form-control validate">{{old('comment')}}</textarea>
            @endif

        <label>Ocena</label>
        <input required placeholder="Ocena" min="1" max="5" name="rate" @if(Route::currentRouteName()=='admin_reviews_edit' )
            value="{{$reviews->rate}}" @else value="{{old('rate')}}" @endif type="number"
            class="form-control-input form-control validate">
{{--
  

    <label>Czas trwania</label>

            <input required placeholder="Czas" name="time" @if(Route::currentRouteName()=='admin_servicesprices_edit' )
                value="{{date('H:i:s', strtotime($services_prices->time))}}" @else value="" @endif type="time" step="1" class="form-control-input form-control validate">


        <label>Cena</label>
        <input required placeholder="Cena" name="price" @if(Route::currentRouteName()=='admin_servicesprices_edit' )
            value="{{$services_prices->price}}" @else value="" @endif type="text" class="form-control-input form-control validate">


        <label>Firma</label>
        <select name="id_user" class="form-control">
            @foreach ($users as $user)
            <option value="{{$user->id}}" @if(Route::currentRouteName()=='admin_servicesprices_edit' )
                @if($user->id == $services_prices->id_user) selected="selected" @endif @endif>{{$user->name}} ({{$user->email}})
            </option>
            @endforeach
        
        </select> --}}
</div>
<div class="panel-footer">
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>
    </div>