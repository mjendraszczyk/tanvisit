@extends('admin.layouts.app')
@section('title')
Pracownicy - Edycja
@endsection
@section('admin_section')
<form method="POST" action="{{route('admin_employees_update', ['id' => $employees->id_employee])}}">
    @method('PUT')
    @include('admin.employees.form')
</form>
@endsection
