@csrf

<div class="col s12 m12 l12">
    @include('admin/core/notification')
 
  <label>Imię</label>
    <input required placeholder="Imię" name="firstname" @if(Route::currentRouteName()=='admin_employees_edit' )
        value="{{$employees->firstname}}" @else value="" @endif type="text" class="form-control-input form-control validate">

        
        <label>Nazwisko</label>
        <input required placeholder="Nazwisko" name="lastname" @if(Route::currentRouteName()=='admin_employees_edit' )
            value="{{$employees->lastname}}" @else value="" @endif type="text"
            class="form-control-input form-control validate">


            <label>Telefon</label>
            <input required placeholder="Telefon" name="phone" @if(Route::currentRouteName()=='admin_employees_edit' )
                value="{{$employees->phone}}" @else value="" @endif type="text" class="form-control-input form-control validate">

                <label>Firma</label>
     <select name="id_company" class="form-control">
        @foreach ($companies as $company)
            <option value="{{$company->id_company}}" @if(Route::currentRouteName()=='admin_employees_edit') @if($company->id_company == \App\Http\Controllers\admin\AdminEmployeesController::getCompanyByUser(Auth::user()->id)->id_company) selected="selected" @endif @endif>ID: {{$company->id_company}} | {{$company->name}}</option>
        @endforeach
         
    </select>
@if(Route::currentRouteName()=='admin_employees_create' )
<label>E-mail</label>
<input class="form-control" type="email" name="email" id="email" />

    <label>Hasło</label>
    <input class="form-control" type="password" name="password" id="password"/>
    <label>Powtórz hasło</label>
    <input class="form-control" type="password" name="password_repeat" id="password_repeat"/>
@endif
</div>
<div class="panel-footer">
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>
</div>