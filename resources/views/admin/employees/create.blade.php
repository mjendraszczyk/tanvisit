@extends('admin.layouts.app')
@section('title')
Pracownicy - Tworzenie
@endsection
@section('admin_section')
    <form method="POST" action="{{route('admin_employees_store')}}">
        @include('admin.employees.form')
    </form>
@endsection