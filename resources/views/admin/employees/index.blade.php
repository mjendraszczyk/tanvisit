﻿@extends('admin.layouts.app')
@section('title')
Pracownicy
@endsection
@section('options')
<a href="{{route('admin_employees_create')}}" class="btn btn-success">
    Dodaj
</a>
@endsection
@section('admin_section')
<table class="table">
    <thead>
        <th>ID</th>
        <th>Imię</th>
        <th>Nazwisko</th>
        <th>E-mail</th>
        <th>Phone</th>
        <th class="text-right">Opcje</th>
    </thead>
@foreach ($employees as $employee)
    <tr>
        <td>
            {{$employee->id_employee}}
        </td>
        <td>
                    {{$employee->firstname}}
        </td>
        <td>
            {{$employee->lastname}}
        </td>
        <td>
            {{$employee->email}}
        </td>
        <td>
            {{$employee->phone}}
        </td>
        <td class="text-right">
            <a href="{{route('admin_employees_edit',['id'=>$employee->id_employee])}}" class="btn btn-default">
                <i class="fa fa-edit"></i>
            </a>
            <form class="inline" method="POST" action="{{route('admin_employees_destroy',['id'=>$employee->id_employee])}}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-remove"></i>
            </button>
            </form>
        </td>
    </tr>
@endforeach
{{ $employees->links() }}
</table>
@endsection