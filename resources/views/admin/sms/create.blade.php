@extends('admin.layouts.app')
@section('title')
Szablon sms - Tworzenie
@endsection
@section('admin_section')
    <form method="POST" action="{{route('admin_sms_store')}}">
        @include('admin.sms.form')
    </form>
@endsection