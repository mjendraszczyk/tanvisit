@csrf

<div class="col s12 m12 l12">
    @include('admin/core/notification')

    <h6>Dostępne tagi: {customer} {employee} {phone} {service} {status} {datetime_from} {phoneEmployee} {company} {active_link}</h6>
    <label>Zawartosc</label>
    @if(Route::currentRouteName()=='admin_sms_edit' )
    <textarea class="form-control" name="content">{{$sms->content}}</textarea>
    @else
    <textarea class="form-control" name="content"></textarea>
    @endif
    
    <label>Typ</label>
     <select name="id_type" class="form-control">
        @foreach ($types as $key => $type)
            <option value="{{$type['value']}}" @if(Route::currentRouteName()=='admin_sms_edit') @if($type['value'] == $sms->id_type) selected="selected" @endif @endif>{{$type['label']}}</option>
        @endforeach
         
    </select> 
</div>
<div class="panel-footer">
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>
</div>