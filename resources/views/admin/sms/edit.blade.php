@extends('admin.layouts.app')
@section('title')
Szablon sms - Edycja
@endsection
@section('admin_section')
<form method="POST" action="{{route('admin_sms_update', ['id' => $sms->id_sms])}}">
    @method('PUT')
    @include('admin.sms.form')
</form>
@endsection
