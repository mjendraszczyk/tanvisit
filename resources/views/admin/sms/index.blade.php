﻿@extends('admin.layouts.app')
@section('title')
Szablony sms
@endsection
@section('options')
<a href="{{route('admin_sms_create')}}" class="btn btn-success">
    Dodaj
</a>
@endsection
@section('admin_section')
<table class="table">
    <thead>
        <th>ID</th>
        <th>Typ</th>
        <th class="text-right">Opcje</th>
    </thead>
@foreach ($sms as $s)
    <tr>
        <td>
            {{$s->id_sms}}
        </td>
        <td>
            {{-- {{($types[$s->id_type]['label'])}} --}}
            <span class="badge rounded-pill bg-success">
            @foreach ($types as $type)
                @if($type['value'] == $s->id_type)
                {{$type['label']}}
                @endif
            @endforeach
            </span>
            {{-- {{print_r($types)}} --}}
                    {{-- {{$types[$s->id_type]}} --}}
        </td>
        <td class="text-right">
            <a href="{{route('admin_sms_edit',['id'=>$s->id_sms])}}" class="btn btn-default">
                <i class="fa fa-edit"></i>
            </a>
            <form class="inline" method="POST" action="{{route('admin_sms_destroy',['id'=>$s->id_sms])}}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-remove"></i>
            </button>
            </form>
        </td>
    </tr>
@endforeach
{{ $sms->links() }}
</table>
@endsection