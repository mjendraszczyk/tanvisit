@extends('admin.layouts.app')
@section('title')
Szablon email - Edycja
@endsection
@section('admin_section')
<form method="POST" action="{{route('admin_emailtemplates_update', ['id' => $emailtemplates->id_emailtemplate])}}">
    @method('PUT')
    @include('admin.emailtemplates.form')
</form>
@endsection
