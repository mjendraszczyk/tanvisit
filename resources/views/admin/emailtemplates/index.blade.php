﻿@extends('admin.layouts.app')
@section('title')
Szablony email
@endsection
@section('options')
<a href="{{route('admin_emailtemplates_create')}}" class="btn btn-success">
    Dodaj
</a>
@endsection
@section('admin_section')
<table class="table">
    <thead>
        <th>ID</th>
        <th>Typ</th>
        <th class="text-right">Opcje</th>
    </thead>
@foreach ($emailtemplates as $template)
    <tr>
        <td>
            {{$template->id_emailtemplate}}
        </td>
        <td>
                    <span class="badge rounded-pill bg-success">
                        @foreach ($types as $type)
                        @if($type['value'] == $template->id_type)
                        {{$type['label']}}
                        @endif
                        @endforeach
                    </span>
        </td>
        <td class="text-right">
            <a href="{{route('admin_emailtemplates_edit',['id'=>$template->id_emailtemplate])}}" class="btn btn-default">
                <i class="fa fa-edit"></i>
            </a>
            <form class="inline" method="POST" action="{{route('admin_emailtemplates_destroy',['id'=>$template->id_emailtemplate])}}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-remove"></i>
            </button>
            </form>
        </td>
    </tr>
@endforeach
{{ $emailtemplates->links() }}
</table>
@endsection