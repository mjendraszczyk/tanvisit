@extends('admin.layouts.app')
@section('title')
Szablon email - Tworzenie
@endsection
@section('admin_section')
    <form method="POST" action="{{route('admin_emailtemplates_store')}}">
        @include('admin.emailtemplates.form')
    </form>
@endsection