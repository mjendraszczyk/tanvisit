@csrf

<div class="col s12 m12 l12">
    @include('admin/core/notification')

    <h6>Dostępne tagi: {customer} {employee} {phone} {service} {status} {datetime_from} {phoneEmployee} {company} {active_link}</h6>
    <label>Zawartosc</label>
    @if(Route::currentRouteName()=='admin_emailtemplates_edit' )
    <textarea class="form-control" name="content">{{$emailtemplates->content}}</textarea>
    @else
    <textarea class="form-control" name="content"></textarea>
    @endif
    
    <label>Typ</label>
     <select name="id_type" class="form-control">
        @foreach ($types as $key => $type)
            <option value="{{$type['value']}}" @if(Route::currentRouteName()=='admin_emailtemplates_edit') @if($type['value'] == $emailtemplates->id_type) selected="selected" @endif @endif>{{$type['label']}}</option>
        @endforeach
         
    </select> 
</div>
<div class="panel-footer">
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>
</div>