﻿@extends('admin.layouts.app')
@section('title')
Godziny otwarcia
@endsection
@section('options')

@endsection
@section('admin_section')
<table class="table">
    <thead>
        <th>ID</th>
        <th>Firma</th>
        <th class="text-right">Opcje</th>
    </thead>
@foreach ($openings as $opening)
    <tr>
        <td>
            {{$opening->id_company}}
        </td>
        <td>
            {{-- {{\App\Http\Controllers\admin\AdminServicesController::getServiceName($services_price->id_service)}} --}}
            
        </td>
        <td class="text-right">
            <a href="{{route('admin_servicesprices_edit',['id'=>$services_price->id_service_price])}}" class="btn btn-default">
                <i class="fa fa-edit"></i>
            </a>
            {{-- <form class="inline" method="POST" action="{{route('admin_servicesprices_destroy',['id'=>$services_price->id_service_price])}}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-remove"></i>
            </button>
            </form> --}}
        </td>
    </tr>
@endforeach
{{ $services_prices->links() }}
</table>
@endsection