@csrf

<div class="col s12 m12 l12">
    @include('admin/core/notification')

    @if($countOpenings == 0)
    @for($i=0;$i<7;$i++)
    <div class="card" style="margin:15px 0;">
        <div class="card-header">
            <label>Dzień {{$days[$i]}}</label>
        </div>
        <div class="card-body">
    <label>Godzina od:</label>
    <input type="time" name="from_{{$i}}" value="00:00">
    <label>Godzina do:</label>
    <input type="time" name="to_{{$i}}" value="00:00">
    </div>
    </div>
    @endfor
    @else 
    @foreach($openings as $opening)
    <div class="card" style="margin:15px 0;">
        <div class="card-header">
            <label>Dzień {{$days[$opening->day]}}</label>
        </div>
        <div class="card-body">
            <label>Godzina od:</label>
            <input type="time" name="from_{{$opening->day}}" value="{{$opening->from}}">
            <label>Godzina do:</label>
            <input type="time" name="to_{{$opening->day}}" value="{{$opening->to}}">
        </div>
    </div>
    @endforeach
    @endif
    {{-- <label>Usługa</label>
    
    <select name="id_service" class="form-control">
        @foreach ($services as $service)
        <option value="{{$service->id_service}}" @if(Route::currentRouteName()=='admin_servicesprices_edit' ) @if($services_prices->id_service == $service->id_service) selected="selected" @endif @endif>{{$service->name}}
        </option>
        @endforeach
    
    </select>

  

    <label>Czas trwania</label>

            <input required placeholder="Czas" name="time" @if(Route::currentRouteName()=='admin_servicesprices_edit' )
                value="{{date('H:i:s', strtotime($services_prices->time))}}" @else value="" @endif type="time" step="1" class="form-control-input form-control validate">


        <label>Cena</label>
        <input required placeholder="Cena" name="price" @if(Route::currentRouteName()=='admin_servicesprices_edit' )
            value="{{$services_prices->price}}" @else value="" @endif type="text" class="form-control-input form-control validate">


        <label>Firma</label>
        <select name="id_user" class="form-control">
            @foreach ($users as $user)
            <option value="{{$user->id}}" @if(Route::currentRouteName()=='admin_servicesprices_edit' )
                @if($user->id == $services_prices->id_user) selected="selected" @endif @endif>{{$user->name}} ({{$user->email}})
            </option>
            @endforeach
        
        </select> --}}
</div>
<div class="panel-footer">
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>
    </div>