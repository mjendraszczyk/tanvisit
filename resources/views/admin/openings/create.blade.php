@extends('admin.layouts.app')
@section('title')
Godziny otwarcia - Tworzenie
@endsection
@section('admin_section')
    <form method="POST" action="{{route('admin_openings_store')}}">
        @include('admin.openings.form')
    </form>
@endsection