@extends('admin.layouts.app')
@section('title')
Godzny otwarcia - Edycja
@endsection
@section('admin_section')
<form method="POST" action="{{route('admin_openings_update', ['id' => $id])}}">
    @method('PUT')
    @include('admin.openings.form')
</form>
@endsection
