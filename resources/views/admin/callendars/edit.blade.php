@extends('admin.layouts.admin_form')

@section('admin_section')
<form method="POST" action="{{route('admin_callendar_update', ['id' => $callendar->id_callendar])}}">
    @include('admin.callendars.form')
</form>
@endsection
