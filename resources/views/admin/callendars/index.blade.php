﻿@extends('admin.layouts.app')
@section('title')
W tym tygodniu
@endsection
@section('header')
<div class="row cards">
    <div class="col-md-4">
<div class="card">
    <div class="card-body"> 
        <i class="fa fa-suitcase" aria-hidden="true"></i>
{{$wizyty}} wizyt 
</div>


</div>
</div>
<div class="col-md-4">
    <div class="card">
    <div class="card-body">
        <i class="fa fa-clock-o" aria-hidden="true"></i>
        {{$godziny}} godzin pracy
    </div>
    </div>
</div>

<div class="col-md-4">
    <div class="card">
        <div class="card-body">
            <i class="fa fa-dollar"></i>
            {{\App\Http\Controllers\Controller::formatPrice($przychod)}}  przychodu
        </div>
    </div>
</div>
</div>
<div class="panel">
    <div class="panel-header">
        <a href="{{route('admin_visits_create')}}" class="btn add_visit-btn">
        <i class="fa fa-plus"></i></a>
    </div>
</div>
@endsection
@section('content')
 
 
<div class="panel">
    <div class="panel-default">
    <form action="{{route('admin_callendars_filter')}}" method="POST">
        @csrf
     <div class="row text-center">
    <div class="col-md-6">
<label>Filtruj po pracowniku</label>
    <select id="id_employee" name="id_employee" onchange="filterCallendar(this)" class="form-control">
      
        <option value="0">wszystie</option>
      @foreach($employees as $employee)
    <option @if($id_employee == $employee->id_employee) selected="selected" @endif value="{{$employee->id_employee}}">{{$employee->firstname}}{{$employee->lastname}}</option>
    @endforeach
    </select>
    </div>

    <div class="col-md-6">
<label>Filtruj po usłudze</label>
    <select id="id_service" name="id_service" onchange="filterCallendar(this)" class="form-control">
        <option value="0">wszystie</option>
    @foreach($services as $service)
    <option value="{{$service->id_service}}" @if($id_service == $service->id_service) selected="selected" @endif>{{$service->name}}</option>
    @endforeach
    </select>
    </div>

    {{-- <div class="col-md-12">
        <button type="submit" class="btn btn-primary btn-lg" style="margin:25px 0 0 0;min-width: 250px">Filtruj</button>
    </div> --}}


    </div> 
    </form>
    </div>
    </div> 
<div class="panel">
    <div class="panel-default">
        <div class="calendar-content">
<div id="calendar">
   {!! $calendar->calendar() !!}
</div>
{!! $calendar->script() !!}
</div>
</div>
</div>
<script type="text/javascript">
function filterCallendar(object) {
console.log("TEST");
console.log('/backoffice/api/callendars/1/'+$('#id_employee').val()+'/'+$('#id_service').val());

$.ajaxSetup({
headers: {
"X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content")
}
});

$.ajax({
url: '/backoffice/api/callendars/1/'+$('#id_employee').val()+'/'+$('#id_service').val(),
method: 'POST',
data : {
    id_service : $("#id_service").val(), 
    id_employee : $("#id_employee").val()
    },
beforeSend: function() {
    $('.calendar-content').css('opacity',0.5);
},
success: function(data) {
console.log(data);
$(".calendar-content").html(data);
$('.calendar-content').css('opacity',1);
}
})
}
</script>
@endsection