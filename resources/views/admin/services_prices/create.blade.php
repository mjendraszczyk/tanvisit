@extends('admin.layouts.app')
@section('title')
Cennik usług - Tworzenie
@endsection
@section('admin_section')
    <form method="POST" action="{{route('admin_servicesprices_store')}}">
        @include('admin.services_prices.form')
    </form>
@endsection