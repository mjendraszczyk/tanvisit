@extends('admin.layouts.app')
@section('title')
Cennik usług - Edycja
@endsection
@section('admin_section')
<form method="POST" action="{{route('admin_servicesprices_update', ['id' => $services_prices->id_service_price])}}">
    @method('PUT')
    @include('admin.services_prices.form')
</form>
@endsection
