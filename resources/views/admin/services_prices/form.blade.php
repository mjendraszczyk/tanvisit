@csrf

<div class="col s12 m12 l12">
    @include('admin/core/notification')
 
    <label>Usługa</label>
    
    <select name="id_service" class="form-control">
        @foreach ($services as $service)
        <option value="{{$service->id_service}}" @if(Route::currentRouteName()=='admin_servicesprices_edit' ) @if($services_prices->id_service == $service->id_service) selected="selected" @endif @endif>{{$service->name}}
        </option>
        @endforeach
    
    </select>

  

    <label>Czas trwania</label>

            <input required placeholder="Czas" name="time" @if(Route::currentRouteName()=='admin_servicesprices_edit' )
                value="{{date('H:i:s', strtotime($services_prices->time))}}" @else value="" @endif type="time" step="1" class="form-control-input form-control validate">


        <label>Cena</label>
        <input required placeholder="Cena" name="price" @if(Route::currentRouteName()=='admin_servicesprices_edit' )
            value="{{$services_prices->price}}" @else value="" @endif type="text" class="form-control-input form-control validate">


  
        @if(\App\Http\Controllers\Controller::getCompanyId(Auth::user()->id))
        <input type="hidden" name="id_user" value="{{\App\Http\Controllers\Controller::getCompanyId(Auth::user()->id)}}" />
        @else 
      <label>Firma</label>
        <select name="id_user" class="form-control">
            @foreach ($users as $user)
            <option value="{{$user->id_company}}" @if(Route::currentRouteName()=='admin_servicesprices_edit' )
                @if($user->id == $services_prices->id_user) selected="selected" @endif @endif>{{$user->name}} ({{$user->email}})
            </option>
            @endforeach
        
        </select>
        @endif
</div>
<div class="panel-footer">
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>
    </div>