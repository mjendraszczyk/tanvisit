﻿@extends('admin.layouts.app')
@section('title')
Cennik usług
@endsection
@section('options')
<a href="{{route('admin_servicesprices_create')}}" class="btn btn-success">
    Dodaj
</a>
@endsection
@section('admin_section')
<table class="table">
    <thead>
        <th>ID</th>
        <th>Nazwa</th>
        <th>Czas</th>
        <th>Cena</th>
        <th class="text-right">Opcje</th>
    </thead>
@foreach ($services_prices as $services_price)
    <tr>
        <td>
            {{$services_price->id_service_price}}
        </td>
        <td>
            {{\App\Http\Controllers\admin\AdminServicesController::getServiceName($services_price->id_service)}}
            
        </td>
        <td>
            {{$services_price->time}}
        </td>
        <td>
            {{\App\Http\Controllers\Controller::formatPrice($services_price->price)}}
        </td>
        <td class="text-right">
            <a href="{{route('admin_servicesprices_edit',['id'=>$services_price->id_service_price])}}" class="btn btn-default">
                <i class="fa fa-edit"></i>
            </a>
            <form class="inline" method="POST" action="{{route('admin_servicesprices_destroy',['id'=>$services_price->id_service_price])}}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-remove"></i>
            </button>
            </form>
        </td>
    </tr>
@endforeach
{{ $services_prices->links() }}
</table>
@endsection