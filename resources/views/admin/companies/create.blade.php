@extends('admin.layouts.app')
@section('title')
Firmy - Tworzenie
@endsection
@section('admin_section')
    <form id="form_companies" method="POST" action="{{route('admin_companies_store')}}">
        @include('admin.companies.form')
    </form>
@endsection