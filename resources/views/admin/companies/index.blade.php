﻿@extends('admin.layouts.app')
@section('title')
Firmy
@endsection
@section('options')
<a href="{{route('admin_companies_create')}}" class="btn btn-success">
    Dodaj
</a>
@endsection
@section('admin_section')
<table class="table">
    <thead>
        <th>ID</th>
        <th>Nazwa</th>
        <th>NIP</th>
        <th>E-mail</th>
        <th>Phone</th>
        <th class="text-right">Opcje</th>
    </thead>
@foreach ($companies as $company)
    <tr>
        <td>
            {{$company->id_company}}
        </td>
        <td>
                    {{$company->name}}
        </td>
        <td>
            {{$company->vat_number}}
        </td>
        <td>
            {{$company->email}}
        </td>
        <td>
            {{$company->phone}}
        </td>
        <td class="text-right">
            <a href="{{route('admin_companies_edit',['id'=>$company->id_company])}}" class="btn btn-default">
                <i class="fa fa-edit"></i>
            </a>
            <form class="inline" method="POST" action="{{route('admin_companies_destroy',['id'=>$company->id_company])}}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-remove"></i>
            </button>
            </form>
        </td>
    </tr>
@endforeach
{{ $companies->links() }}
</table>
@endsection