@extends('admin.layouts.app')
@section('title')
Firmy - Edycja
@endsection
@section('admin_section')
<form id="form_companies" method="POST" action="{{route('admin_companies_update', ['id' => $companies->id_company])}}">
    @method('PUT')
    @include('admin.companies.form')
</form>
@endsection
