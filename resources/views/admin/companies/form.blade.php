@csrf

<div class="col s12 m12 l12">
    @include('admin/core/notification')
 
  <label>Nazwa</label>
    <input required placeholder="Nazwa" name="name" @if(Route::currentRouteName()=='admin_companies_edit' )
        value="{{$companies->name}}" @else value="{{old('name')}}" @endif type="text" class="form-control-input form-control validate">

        
        <label>NIP</label>
        <input required placeholder="NIP" name="vat_number" @if(Route::currentRouteName()=='admin_companies_edit' )
            value="{{$companies->vat_number}}" @else value="{{old('vat_number')}}" @endif type="text"
            class="form-control-input form-control validate">


            <label>Email</label>
            <input required placeholder="Email" name="email" @if(Route::currentRouteName()=='admin_companies_edit' )
                value="{{$companies->email}}" @else value="{{old('email')}}" @endif type="text" class="form-control-input form-control validate">


                <label>Instagram</label>
            <input  placeholder="Instagram" name="instagram" @if(Route::currentRouteName()=='admin_companies_edit' )
                value="{{$companies->instagram}}" @else value="{{old('instagram')}}" @endif type="text" class="form-control-input form-control validate">

<label>Facebook</label>
                <input  placeholder="Facebook" name="facebook" @if(Route::currentRouteName()=='admin_companies_edit' )
                    value="{{$companies->facebook}}" @else value="{{old('facebook')}}" @endif type="text"
                    class="form-control-input form-control validate">

<label>Opis</label>
@if(Route::currentRouteName()=='admin_companies_edit' )
<textarea class="form-control" name="description">{{$companies->description}}</textarea>
@else
<textarea class="form-control" name="description">{{old('description')}}</textarea>
@endif 
                    
<label>Adres</label>
<input required placeholder="Adres" name="address" @if(Route::currentRouteName()=='admin_companies_edit' )
    value="{{$companies->address}}" @else value="{{old('address')}}" @endif type="text" class="form-control-input form-control validate">

<label>Telefon</label>
<input required placeholder="Telefon" name="phone" @if(Route::currentRouteName()=='admin_companies_edit' )
    value="{{$companies->phone}}" @else value="{{old('phone')}}" @endif type="text" class="form-control-input form-control validate">

    <label>Kod pocztowy</label>
    <input required placeholder="00-000" name="postalcode" @if(Route::currentRouteName()=='admin_companies_edit' )
        value="{{$companies->postalcode}}" @else value="{{old('postalcode')}}" @endif type="text" class="form-control-input form-control validate">

    <label>Miasto</label>
    <input required placeholder="Miasto" name="city" @if(Route::currentRouteName()=='admin_companies_edit' )
        value="{{$companies->city}}" @else value="{{old('city')}}" @endif type="text" class="form-control-input form-control validate">


        <input required placeholder="Lat" id="lat" name="lat" @if(Route::currentRouteName()=='admin_companies_edit' )
            value="{{$companies->lat}}" @else value="{{old('lat')}}" @endif type="hidden"
            class="form-control-input form-control validate">


            <input required placeholder="Lon" id="lon" name="lon" @if(Route::currentRouteName()=='admin_companies_edit' )
                value="{{$companies->lon}}" @else value="{{old('lon')}}" @endif type="hidden"
                class="form-control-input form-control validate">

                <label>Kraj</label>
     <select name="id_country" class="form-control">
        @foreach ($countries as $country)
            <option value="{{$country->id_country}}" @if(Route::currentRouteName()=='admin_companies_edit') @if($companies->id_country == $country->id_country) selected="selected" @endif @endif>{{$country->name}}</option>
        @endforeach
     </select>

        <label>Certyfikat</label>
            <select name="certificate" class="form-control">
                 
                <option value="0" @if(Route::currentRouteName()=='admin_companies_edit' ) @if(0 == $companies->certificate) selected="selected" @endif @endif>Brak</option>
            
                <option value="1" @if(Route::currentRouteName()=='admin_companies_edit' ) @if(1==$companies->certificate)
                    selected="selected" @endif @endif>Certyfikat TanExpert</option>
            </select>
                <label>Sklep stacjonarny</label>
                    <select name="store" class="form-control">
                        <option value="0" @if(Route::currentRouteName()=='admin_companies_edit' ) @if($companies->store == '0') selected="selected" @endif @endif>Brak sklepu stacjonarnego</option>
                       <option value="1" @if(Route::currentRouteName()=='admin_companies_edit' ) @if($companies->store == '1') selected="selected" @endif @endif>Posiadam sklep stacjonarny</option>
         
    </select>
  
    <script type="text/javascript">
    //alert("G");
    function getLatLon() {
    //alert("A");
    var ulica = document.querySelector("input[name=address]").value;
    var getMiasto = document.querySelector("input[name=city]").value;
    var address = getMiasto + ", "+ulica;
   // alert(address);
    $.get(location.protocol + '//nominatim.openstreetmap.org/search?format=json&q=' + address, function (data) {
        if(data.length == 0) {
            $.get(location.protocol + '//nominatim.openstreetmap.org/search?format=json&q='+ getMiasto, function (data) {
                //alert(data[0].lat + " "+data[0].lon);
                alert(data);
                document.querySelector("#lat").value = data[0].lat;
               document.querySelector("#lon").value = data[0].lon;
               AjaxCall();
            
            });
        } else {
            //alert(data[0].lat + " "+data[0].lon);
         document.querySelector("#lat").value = data[0].lat;
        document.querySelector("#lon").value = data[0].lon;
      AjaxCall();
        }
    
    })
    }

    function AjaxCall() {
        $.ajaxSetup({
        headers: {
        "X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content")
        }
        });
        
        $.ajax({
        url: '/backoffice/companies/{{\App\Http\Controllers\Controller::getCompanyId(Auth::user()->id)}}/edit',
        method: 'POST',
        data : $("#form_companies").serialize(),
        beforeSend: function() {
        $('#form_companies').css('opacity',0.5);
        },
        success: function(data) {
        $.growl.notice({ title: "", message: "Zapisano pomyślnie!" });
        $('#form_companies').css('opacity',1);
        }
        })
    }

window.onload = function() {
    document.getElementById("btn_companies").addEventListener("click", function(event){
    event.preventDefault();
    
    getLatLon();

  

//    window.location.href = document.querySelector("#form_companies").getAttribute("action");
    });
}

//getLatLon();
    </script>
</div>
<div class="panel-footer">
<button id="btn_companies" type="submit"  class="btn  waves-effect waves-light btn-large">
    Zapisz</button>
</div>