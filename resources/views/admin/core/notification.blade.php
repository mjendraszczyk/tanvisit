﻿<div class="notification">
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if (!empty(Session::get('success_message')))
<div class="alert alert-success">
    <ul>
        <li>{{Session::get('success_message')}}</li>
    </ul>
</div>
@endif

@if (!empty(Session::get('warning_message')))
<div class="alert alert-warning">
    <ul>
        <li>{{Session::get('warning_message')}}</li>
    </ul>
</div>
@endif

@if (!empty(Session::get('info_message')))
<div class="alert alert-info">
    <ul>
        <li>{{Session::get('info_message')}}</li>
    </ul>
</div>
@endif

@if (!empty(Session::get('danger_message')))
<div class="alert alert-danger">
    <ul>
        <li>{{Session::get('danger_message')}}</li>
    </ul>
</div>
@endif
</div>