<nav class="shadow-sm navbar navbar-expand-lg navbar-light bg-light">
    <div class="col-md-4">
        <a class="navbar-brand" href="{{route('admin_callendars_index')}}">{{env('APP_NAME')}}</a>
<a href="#menu-toggle" class="btn" id="menu-toggle">
    <i class="fa fa-bars"></i>Menu
</a>
    </div>
    <div class="col-md-8">
        <div class="collapse navbar-collapse" id="navbarSupportedContent" style="">
            @if(Auth::check())
            <ul class="navbar-nav mr-auto" style="margin-right: 0 !important;">
                <li class="nav-item cloud">
                    <a class="nav-link" target="_blank" href="{{route('front_profil',['id'=>\App\Http\Controllers\Controller::getCompanyId(Auth::user()->id),'name'=>str_slug(\App\Http\Controllers\Controller::getCompanyNameByUser(Auth::user()->id))])}}"> <i class="fa fa-cloud" aria-hidden="true"></i> twoja strona www</a>
                </li>
                
                <li class="nav-item dropdown" style="padding: 0 25px;">
                    <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <span class="fa fa-user"></span>
                        {{Auth::user()->email}}
                        <span class="badge rounded-pill bg-success">
                            {{App\Http\Controllers\Controller::getRoleName(Auth::user()->id_role)}}
                        </span>
                        (firma: {{\App\Http\Controllers\admin\AdminEmployeesController::getCompanyByUser(Auth::user()->id)->name}})
                    </a>
                </li>
                <li style="border-left: 1px solid #eee;padding: 0 15px;">
                    <a class="nav-link" href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"   aria-hidden="true"></i>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
                </li>
        
            </ul>
            @endif
        </div>
    </div>
</nav>