
{{-- <script src="{{ asset('js/backend/admin.js?v=') }}{{time()}}">
</script> --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

<script type="text/javascript">
  tinymce.init({
  selector: '.editor',
  height: 300,
   branding: false,
  menubar: true,
   menu: {
     edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
    insert: {title: 'Insert', items: 'link media | template hr'},
    view: {title: 'View', items: 'visualaid'},
    format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
    table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
    tools: {title: 'Tools', items: 'spellchecker code'}
  },
  plugins: [
    'advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code help wordcount'
  ],
  toolbar: 'spellchecker code | insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css']
});
</script>
<!-- Menu Toggle Script -->


<script type="text/javascript">
  // function filterCallendar(object) {
  //   console.log("TEST");
  //   // $.ajax({
  //   //   url: '/backoffice/api/callendars/1/'+$('#id_employee').val()+'/'++$('#id_service').val(),
  //   //   method: 'POST',
  //   //   success: function(data) {
  //   //     console.log(data);
  //   //   }
  //   // })
  // }
  function autocompeteFnc(source,input,output) { 

var src = source;
 $(input).autocomplete({
     source: function(request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term : request.term
                },
                success: function(data) {
                    response(data);
                   
                }
            });
        },
        minLength: 3,
    select: function(event, ui) {
	  	$(output).val(ui.item.id);
    },
    autoFocus: true,
     open: function() {
        $(".ui-autocomplete").width( $(this).innerWidth() );
         
    }
       
});

  }
$(document).ready(function() {
 

});
   $("#menu-toggle").click(function(e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");

              });

              $(document).ready(function() {

// $('#calendar').fullCalendar({
//   'lang' : 'pl-pl',
//   })
  
var table_data_sort = '';
var table_data_sort_type = '';
var attr = $('#tableSort,.tableSort').attr('table-data-sort'); 
  
if (typeof attr !== typeof undefined && attr !== false) {
table_data_sort = $('#tableSort, .tableSort').attr('table-data-sort');
   table_data_sort_type = $('#tableSort, .tableSort').attr('table-data-sort-type');
}  
else { 
   table_data_sort = '0';
   table_data_sort_type = 'asc';
}

    // $('#tableSort, .tableSort').DataTable( {
    //    "language": {
    //         "lengthMenu": "Pokaż _MENU_ wyników na stronie",
    //         "zeroRecords": "Nic nie znaleziono",
    //         "info": "Wyświetlam _PAGE_ stronę z _PAGES_",
    //         "infoEmpty": "Brak wyników",
    //         "infoFiltered": "(Przefiltrowano do _MAX_ wyników)",
    //         "search" : "Szukaj",
    //          "paginate": {
    //     "first":      "Pierwszy",
    //     "last":       "Ostatni",
    //     "next":       "Następny",
    //     "previous":   "Poprzedni"
    // },
   
 
    //     },
    //      "order": [[ table_data_sort, table_data_sort_type ]]
    //   }
    // );
} );
</script>

</body>

</html>