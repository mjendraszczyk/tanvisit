<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ env('APP_NAME') }}</title>
    <!-- Styles -->
    <link href="{{ asset('admin/css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.17/css/dataTables.bootstrap4.min.css" />
    <link media="none" onload="if(media!='all')media='all'" href="{{asset('admin/css/font-awesome.css')}}" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('admin/css/global.css?v=') }}{{time()}}" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
    <link href="{{ asset('admin/css/sidebar.css') }}?v=1.0" rel="stylesheet">
   <link href="{{ asset('admin/css/growl.css') }}" rel="stylesheet">

    {{-- JS Scripts --}}
    <script src="{{asset('admin/js/jquery.js')}}"></script>
    <script src="{{asset('admin/js/moment.js')}}"></script>
    
 

    <script src="{{ asset('admin/js/app.js') }}"></script>
    <script src="{{ asset('admin/js/jquery-ui.js') }}"></script>

    <script src="{{asset('admin/js/fullCalendar.js')}}"></script>
    <script src="{{asset('admin/js/fullCalendar-locale-all.js')}}"></script>
    <script src="{{asset('admin/js/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('admin/js/growl.js')}}"></script>

</head>