<div id="sidebar-wrapper">

    <ul class="sidebar-nav">
        @if(Auth::check())
        @if((Auth::user()->id_role == '3'))
        <li class="menu--category">
            <span>
                Zarządzanie trescia
            </span>
            <ul class="submenu" style="display:block;" >
                <li>
                    <a href="{{route('admin_home_index')}}"><i class="fa fa-home" aria-hidden="true"></i>
                        Home</a>
                </li>
                <li>
                    <a href="{{route('admin_callendars_index')}}"><i class="fa fa-calendar-o" aria-hidden="true"></i>
                        Kalendarz</a>
                </li>
                <li>
                    <a href="{{route('admin_visitsquotes_index')}}"><i class="fa fa-comments-o" aria-hidden="true"></i>
                        Zapytania o termin</a>
                </li>
                <li>
                    <a href="{{route('admin_visits_history')}}">
                        <i class="fa fa-history" aria-hidden="true"></i>
                        Historia rezerwacji</a>
                </li>
                
                <li>
                    <a href="{{route('admin_customers_index')}}"><i class="fa fa-users" aria-hidden="true"></i>
                        Klienci</a>
                </li>
                @if(Auth::user()->id_role == '3')
                <li>
                    <a href="{{route('admin_configurations_index')}}">
                        <i class="fa fa-cog" aria-hidden="true"></i>
                        Konfiguracja
                    </a>
                </li>
                <li>
                    <a href="{{route('admin_companies_index')}}">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                       Firmy
                    </a>
                </li>
                @endif
                {{-- <li>
                    <a href="{{route('admin_countries_index')}}"><i class="fa fa-flag" aria-hidden="true"></i>
                        Kraje</a>
                </li> --}}
                
                <li>
                    <a href="{{route('admin_employees_index')}}">
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                        Pracownicy
                    </a>
                </li>
                @if(Auth::user()->id_role == '3')
                <li>
                    <a href="{{route('admin_users_index')}}">
                        <i class="fa fa-sitemap" aria-hidden="true"></i>
                        Role
                    </a>
                </li>
                
                <li>
                    <a href="{{route('admin_services_index')}}"><i class="fa fa-wrench" aria-hidden="true"></i>
                        Usługi
                    </a>
                </li>
                @endif
                <li>
                    <a href="{{route('admin_sms_index')}}"><i class="fa fa-phone" aria-hidden="true"></i>
                        Szablony SMS</a>
                </li>
                <li>
                        <a href="{{route('admin_emailtemplates_index')}}"><i class="fa fa-picture-o" aria-hidden="true"></i>
                            Szablony email
                        </a>
                    </li>
@if(Auth::user()->id_role == '3')
<li>
    <a href="{{route('admin_servicescategories_index')}}"><i class="fa fa-book" aria-hidden="true"></i>

        Kategorie usług</a>
</li>
@endif
<li>
    <a href="{{route('admin_servicesprices_index')}}"><i class="fa fa-dollar" aria-hidden="true"></i>

        Cennik usług</a>
</li>

<li>
    <a href="{{route('admin_reviews_index')}}"><i class="fa fa-star" aria-hidden="true"></i>

        Opinie</a>
</li>

                <li>
                    <a href="{{route('admin_subscriptions_index')}}"><i class="fa fa-credit-card" aria-hidden="true"></i>

                        Abonament</a>
                </li>
                

            </ul>
            <li class="menu--category">
                <span>
                    Profil
                </span>
                <ul class="submenu" style="display:block;">
                    <li>
                        <a href="{{route('admin_galleries_index')}}"><i class="fa fa-image" aria-hidden="true"></i>
                            Galeria
                        </a>
                    </li>
                    <li>
                        <a href="{{route('admin_openings_edit',['id'=>\App\Http\Controllers\Controller::getCompanyId(Auth::user()->id)])}}"><i class="fa fa-clock-o" aria-hidden="true"></i>
                            Godziny otwarcia
                        </a>
                    </li>
                    <li>
                        <a href="{{route('admin_companies_edit',['id'=>\App\Http\Controllers\Controller::getCompanyId(Auth::user()->id)])}}"><i class="fa fa-user" aria-hidden="true"></i>
                            Dane
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu--category">
                <span>
                    Raporty
                </span>
                <ul class="submenu" style="display:block;">
                    <li>
                        <a href="{{route('admin_raport_employees')}}"><i class="fa fa-bar-chart" aria-hidden="true"></i>
                            Statystyki pracowników
                        </a>
                    </li>
                    <li>
                        <a href="{{route('admin_raport_customers')}}"><i class="fa fa-bar-chart" aria-hidden="true"></i>
                            Statystyki klientów
                        </a>
                    </li>
                </ul>
            </li>
        </li>
        @endif
        @endif
    </ul>
</div>