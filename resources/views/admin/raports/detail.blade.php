﻿@extends('admin.layouts.app')
@section('title')
Statystyki - 
@foreach ($customers as $customer)
{{$customer->firstname}} {{$customer->lastname}}
@endforeach - Szczegóły
@endsection
@section('options')
<a href="{{route('admin_raport_customers')}}" class="btn btn-secondary">
    Powrót
</a>
@endsection
@section('admin_section')
 <div class="panel">
    <div class="panel-default">
        @if(Route::currentRouteName() == 'admin_raports_employee_detail' || Route::currentRouteName() == 'admin_raports_employees_details_filter')
        <form action="{{route('admin_raports_employees_details_filter',['id' => $id])}}" method="POST">
        @else
            <form action="{{route('admin_raports_customers_details_filter',['id' => $id])}}" method="POST">
        @endif
            @csrf
            <div class="row text-center">
                <div class="col-md-5">
                    <label>Data od:</label>
                    <input type="date" name="datetime_from" @if($datetime_from) value="{{$datetime_from}}" @else
                        value="{{date('Y-m-d')}}" @endif id="date_from" class="form-control">
                </div>

                <div class="col-md-5">
                    <label>Data do:</label>
                    <input type="date" name="datetime_to" @if($datetime_to) value="{{$datetime_to}}" @else
                        value="{{date('Y-m-d')}}" @endif id="date_to" class="form-control">
                </div>


                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary btn-lg"
                        style="margin:25px 0 0 0;min-width: 100%">Filtruj</button>
                </div>


            </div>
        </form>
    </div>
</div>

        @foreach ($customers as $customer)
        <div class="card">
            <div class="card-header">
                <i class="fa fa-user"></i>
                Dane klienta
            </div>
            <div class="card-body">
                {{$customer->firstname}}

                {{$customer->lastname}}
                </div>
                </div>


                {{-- {{dd($customerDetails)}} --}}
<div class="row" style="margin-top: 50px">
    <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-suitcase"></i>
                       Wizyty
                    </div>
                    <div class="card-body">
                       {{$customerDetails['count'][$id]}}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-clock-o"></i>
                        Godziny
                    </div>
                    <div class="card-body">
                       {{date('H',strtotime($customerDetails['hours'][$id]))}} :
                       {{($customerDetails['hours'][$id]-(int)$customerDetails['hours'][$id])*100}}
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-money"></i>
                        Przychód
                    </div>
                    <div class="card-body">
                       {{\App\Http\Controllers\Controller::formatPrice($customerDetails['incoming'][$id])}}
                    </div>
                </div>
                </div>
            </div>



            <div class="row" style="margin-top: 50px">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-check"></i>
                            Wizyty potwierdzone
                        </div>
                        <div class="card-body">
                       {{$customerDetails['accepted'][$id]}}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-close"></i>
                            Wizyty anulowane
                        </div>
                        <div class="card-body">
                            {{$customerDetails['canceled'][$id]}}
                        </div>
                    </div>
                </div>
            
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-minus"></i>
                            Wizyty niepotwierdzone
                        </div>
                        <div class="card-body">
                       {{$customerDetails['unconfirmation'][$id]}}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
  
@endsection