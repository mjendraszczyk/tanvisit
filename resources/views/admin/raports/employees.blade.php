﻿@extends('admin.layouts.app')
@section('title')
Statystyki - pracownicy
@endsection
@section('options')
<a href="{{route('admin_raports_employees_export')}}" class="btn btn-secondary">
    Eksport CSV
</a>
@endsection
@section('admin_section')
<div class="panel">
    <div class="panel-default">
        <form action="{{route('admin_raports_employees_filter')}}" method="POST">
            @csrf
            <div class="rowr">
                <div class="col-md-5">
                    <label>Data od:</label>
                    <input type="date" name="date_from" @if($datetime_from) value="{{$datetime_from}}" @else value="{{date('Y-m-d')}}" @endif id="date_from" class="form-control">
                </div>

                <div class="col-md-5">
                    <label>Data do:</label>
                    <input type="date" name="date_to" @if($datetime_to) value="{{$datetime_to}}" @else value="{{date('Y-m-d')}}" @endif id="date_to" class="form-control">
                </div>


                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary btn-lg"
                        style="margin:25px 0 0 0;min-width: 100%">Filtruj</button>
                </div>


            </div>
        </form>
    </div>
</div>
<table class="table">
    <thead>
        <th>ID</th>
        <th>Imię</th>
        <th>Nazwisko</th>
        <th>Wizyty</th>
        <th>Przychody</th>
        <th>Opcje</th>
    </thead>
    @foreach ($employees as $employee)
    <tr>
        <td>
            {{$employee->id_employee}}
        </td>
        <td>
            {{$employee->firstname}}
        </td>
        <td>
            {{$employee->lastname}}
        </td>
        <td>
            {{$employeesDetails['count'][$employee->id_employee]}}
        </td>
        <td>
            {{\App\Http\Controllers\Controller::formatPrice($employeesDetails['incoming'][$employee->id_employee])}}
        </td>
        <td>
                    <a href="{{route('admin_raports_employee_detail',['id_employee' => $employee->id_employee])}}"
                        class="btn btn-default">Szczegóły</a>
                </td>
    </tr>
    @endforeach
    {{ $employees->links() }}
</table>
@endsection