﻿@extends('admin.layouts.app')
@section('title')
Statystyki - klienci
@endsection
@section('options')
<a href="{{route('admin_raports_customers_export')}}" class="btn btn-secondary">
    Eksport CSV
</a>
@endsection
@section('admin_section')
<div class="panel">
    <div class="panel-default">
        <form action="{{route('admin_raports_customers_filter')}}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-5">
                    <label>Data od:</label>
                    <input type="date" name="datetime_from" @if($datetime_from) value="{{$datetime_from}}" @else value="{{date('Y-m-d')}}" @endif
                        id="date_from" class="form-control">
                </div>

                <div class="col-md-5">
                    <label>Data do:</label>
                    <input type="date" name="datetime_to" @if($datetime_to) value="{{$datetime_to}}" @else value="{{date('Y-m-d')}}" @endif id="date_to"
                        class="form-control">
                </div>


                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary btn-lg"
                        style="margin:25px 0 0 0;min-width: 100%">Filtruj</button>
                </div>


            </div>
        </form>
    </div>
</div>
<table class="table">
    <thead>
        <th>ID</th>
        <th>Imię</th>
        <th>Nazwisko</th>
        <th>Wizyty</th>
        <th>Przychody</th>
        <th>Opcje</th>
    </thead>
    @foreach ($customers as $customer)
    <tr>
        <td>
            {{$customer->id_customer}}
        </td>
        <td>
            {{$customer->firstname}}
        </td>
        <td>
            {{$customer->lastname}}
        </td>
        <td>
            {{-- {{dd($customerDetails)}} --}}
            {{$customerDetails['count'][$customer->id_customer]}}
        </td>
        <td>
           {{\App\Http\Controllers\Controller::formatPrice($customerDetails['incoming'][$customer->id_customer])}}
        </td>
        <td>
            <a href="{{route('admin_raports_customer_detail',['id_customer' => $customer->id_customer])}}" class="btn btn-default">Szczegóły</a>
        </td>
    </tr>
    @endforeach
    {{ $customers->links() }}
</table>
@endsection