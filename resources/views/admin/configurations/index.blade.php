﻿@extends('admin.layouts.app')
@section('title')
Konfiguracja
@endsection
@section('options')
    <a href="{{route('admin_configurations_create')}}" class="btn btn-success">
        Dodaj
    </a>
@endsection
@section('admin_section')
<table class="table">
    <thead>
        <th>ID</th>
        <th>Klucz</th>
        <th>Wartosc</th>
        <th class="text-right">Opcje</th>
    </thead>
@foreach ($configurations as $configuration)
    <tr>
        <td>
            {{$configuration->id_configuration}}
        </td>
        <td>
            {{$configuration->key}}
        </td>
        <td>
            {{$configuration->value}}
        </td>
        <td class="text-right">
            <a href="{{route('admin_configurations_edit',['id'=>$configuration->id_configuration])}}" class="btn btn-default">
                <i class="fa fa-edit"></i>
            </a>
             <form class="inline" method="POST" action="{{route('admin_configurations_destroy',['id'=>$configuration->id_configuration])}}">
            @csrf
            @method('DELETE')
            <button type="submit" disabled class="btn btn-danger">
                <i class="fa fa-remove"></i>
            </button>
            </form>
        </td>
    </tr>
@endforeach
{{ $configurations->links() }}
</table>
@endsection