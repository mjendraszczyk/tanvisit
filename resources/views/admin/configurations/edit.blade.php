@extends('admin.layouts.app')
@section('title')
Konfiguracja - Edycja
@endsection
@section('admin_section')
<form method="POST" action="{{route('admin_configurations_update', ['id' => $configurations->id_configuration])}}">
    @method('PUT')
    @include('admin.configurations.form')
</form>
@endsection
