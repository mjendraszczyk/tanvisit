@csrf

<div class="col s12 m12 l12">
    @include('admin/core/notification')
 
    <label>Klucz</label>
    <input required placeholder="Klucz" name="key" @if(Route::currentRouteName()=='admin_configurations_edit' )
        value="{{$configurations->key}}" @else value="{{old('key')}}" @endif type="text" class="form-control-input form-control validate">

        <label>Wartosc</label>
        @if(Route::currentRouteName()=='admin_configurations_edit' )
        <textarea required placeholder="Wartosc" name="value"
            class="form-control-input form-control validate">{{$configurations->value}}</textarea>
            @else 
            <textarea required placeholder="Wartosc" name="value"
                class="form-control-input form-control validate">{{old('value')}}</textarea>
            @endif

        {{-- <input required placeholder="Wartosc" name="value" @if(Route::currentRouteName()=='admin_configurations_edit' )
            value="{{$countries->value}}" @else value="{{old('value')}}" @endif type="text"
            class="form-control-input form-control validate"> --}}
</div>
<div class="panel-footer">
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>
</div>