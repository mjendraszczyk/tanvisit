@extends('admin.layouts.app')
@section('title')
Kongiguracja - Tworzenie
@endsection
@section('admin_section')
    <form method="POST" action="{{route('admin_configurations_store')}}">
        @include('admin.configurations.form')
    </form>
@endsection