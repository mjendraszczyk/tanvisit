@extends('admin.layouts.app')
@section('title')
Kraje - Edycja
@endsection
@section('admin_section')
<form method="POST" action="{{route('admin_countries_update', ['id' => $countries->id_country])}}">
    @method('PUT')
    @include('admin.countries.form')
</form>
@endsection
