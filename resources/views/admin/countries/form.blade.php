@csrf

<div class="col s12 m12 l12">
    @include('admin/core/notification')
 
    <label>Nazwa</label>
    <input required placeholder="Nazwa" name="name" @if(Route::currentRouteName()=='admin_countries_edit' )
        value="{{$countries->name}}" @else value="{{old('name')}}" @endif type="text" class="form-control-input form-control validate">

</div>
<div class="panel-footer">
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>
</div>