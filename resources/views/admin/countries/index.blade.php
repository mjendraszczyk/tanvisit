﻿@extends('admin.layouts.app')
@section('title')
Kraje
@endsection
@section('options')
    <a href="{{route('admin_countries_create')}}" class="btn btn-success">
        Dodaj
    </a>
@endsection
@section('admin_section')
<table class="table">
    <thead>
        <th>ID</th>
        <th>Nazwa</th>
        <th class="text-right">Opcje</th>
    </thead>
@foreach ($countries as $country)
    <tr>
        <td>
            {{$country->id_country}}
        </td>
        <td>
            {{$country->name}}
        </td>
        <td class="text-right">
            <a href="{{route('admin_countries_edit',['id'=>$country->id_country])}}" class="btn btn-default">
                <i class="fa fa-edit"></i>
            </a>
             <form class="inline" method="POST" action="{{route('admin_countries_destroy',['id'=>$country->id_country])}}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-remove"></i>
            </button>
            </form>
        </td>
    </tr>
@endforeach
{{ $countries->links() }}
</table>
@endsection