@extends('admin.layouts.app')
@section('title')
Kraje - Tworzenie
@endsection
@section('admin_section')
    <form method="POST" action="{{route('admin_countries_store')}}">
        @include('admin.countries.form')
    </form>
@endsection