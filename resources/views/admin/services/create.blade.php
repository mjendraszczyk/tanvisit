@extends('admin.layouts.app')
@section('title')
Usługi - Tworzenie
@endsection
@section('admin_section')
    <form method="POST" action="{{route('admin_services_store')}}">
        @include('admin.services.form')
    </form>
@endsection