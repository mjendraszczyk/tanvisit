@extends('admin.layouts.app')
@section('title')
Usługi - Edycja
@endsection
@section('admin_section')
<form method="POST" action="{{route('admin_services_update', ['id' => $services->id_service])}}">
    @method('PUT')
    @include('admin.services.form')
</form>
@endsection
