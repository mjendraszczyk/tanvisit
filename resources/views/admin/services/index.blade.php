﻿@extends('admin.layouts.app')
@section('title')
Usługi
@endsection
@section('options')
<a href="{{route('admin_services_create')}}" class="btn btn-success">
    Dodaj
</a>
@endsection
@section('admin_section')
<table class="table">
    <thead>
        <th>ID</th>
        <th>Nazwa</th>
        <th class="text-right">Opcje</th>
    </thead>
@foreach ($services as $service)
    <tr>
        <td>
            {{$service->id_service}}
        </td>
        <td>
                    {{$service->name}}
        </td>
        <td class="text-right">
            <a href="{{route('admin_services_edit',['id'=>$service->id_service])}}" class="btn btn-default">
                <i class="fa fa-edit"></i>
            </a>
            <form class="inline" method="POST" action="{{route('admin_services_destroy',['id'=>$service->id_service])}}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-remove"></i>
            </button>
            </form>
        </td>
    </tr>
@endforeach
{{ $services->links() }}
</table>
@endsection