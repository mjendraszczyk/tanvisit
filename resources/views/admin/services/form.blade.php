@csrf

<div class="col s12 m12 l12">
    @include('admin/core/notification')
 
    <label>Nazwa</label>
    <input required placeholder="Nazwa" name="name" @if(Route::currentRouteName()=='admin_services_edit' )
        value="{{$services->name}}" @else value="" @endif type="text" class="form-control-input form-control validate">


    <label>Kategoria</label>
     <select name="id_service_category" class="form-control">
        @foreach ($services_category as $sc)
            <option value="{{$sc->id_service_category}}" @if(Route::currentRouteName()=='admin_services_edit') @if($services->id_service_category == $sc->id_service_category) selected="selected" @endif @endif>{{$sc->name}}</option>
        @endforeach
         
    </select> 
</div>
<div class="panel-footer">
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>
</div>