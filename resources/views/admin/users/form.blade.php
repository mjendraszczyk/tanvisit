@csrf

<div class="col s12 m12 l12">
    @include('admin/core/notification')
 
    @if(Route::currentRouteName()=='admin_users_edit')
    <h5>Edycja roli dla {{$users->email}}</h5>
    @endif
    {{-- <label>E-mail</label>
    <input required placeholder="E-mail" name="email" @if(Route::currentRouteName()=='admin_services_edit' )
        value="{{$services->name}}" @else value="" @endif type="text" class="form-control-input form-control validate"> --}}


    <label>Rola</label>
     <select name="id_role" class="form-control">
        @foreach ($roles as $role)
            <option value="{{$role->id_role}}" @if(Route::currentRouteName()=='admin_users_edit') @if($users->id_role == $role->id_role) selected="selected" @endif @endif>{{$role->name}}</option>
        @endforeach
         
    </select> 
</div>
<div class="panel-footer">
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>
</div>