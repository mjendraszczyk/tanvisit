@extends('admin.layouts.app')
@section('title')
Role - Edycja
@endsection
@section('admin_section')
<form method="POST" action="{{route('admin_users_update', ['id' => $users->id])}}">
    @method('PUT')
    @include('admin.users.form')
</form>
@endsection
