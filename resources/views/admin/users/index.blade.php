﻿@extends('admin.layouts.app')
@section('title')
Role
@endsection
@section('options')
@endsection
@section('admin_section')
<table class="table">
    <thead>
        <th>ID</th>
        <th>E-mail</th>
        <th>Rola</th>
        <th class="text-right">Opcje</th>
    </thead>
@foreach ($users as $user)
    <tr>
        <td>
            {{$user->id}}
        </td>
        <td>
                    {{$user->email}}
        </td>
        <td>
            {{App\Http\Controllers\Controller::getRoleName($user->id_role)}}
        </td>
        <td class="text-right">
            <a href="{{route('admin_users_edit',['id'=>$user->id])}}" class="btn btn-default">
                <i class="fa fa-edit"></i>
            </a>
        </td>
    </tr>
@endforeach
{{ $users->links() }}
</table>
@endsection