﻿@extends('admin.layouts.app')
@section('title')
Usługi kategorie
@endsection
@section('options')
<a href="{{route('admin_servicescategories_create')}}" class="btn btn-success">
    Dodaj
</a>
@endsection
@section('admin_section')
<table class="table">
    <thead>
        <th>ID</th>
        <th>Nazwa</th>
        <th class="text-right">Opcje</th>
    </thead>
@foreach ($services_categories as $category)
    <tr>
        <td>
            {{$category->id_service_category}}
        </td>
        <td>
                    {{$category->name}}
        </td>
        <td class="text-right">
            <a href="{{route('admin_servicescategories_edit',['id'=>$category->id_service_category])}}" class="btn btn-default">
                <i class="fa fa-edit"></i>
            </a>
            <form class="inline" method="POST" action="{{route('admin_servicescategories_destroy',['id'=>$category->id_service_category])}}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-remove"></i>
            </button>
            </form>
        </td>
    </tr>
@endforeach
{{ $services_categories->links() }}
</table>
@endsection