@extends('admin.layouts.app')
@section('title')
Kategorie usług - Tworzenie
@endsection
@section('admin_section')
    <form method="POST" action="{{route('admin_servicescategories_store')}}">
        @include('admin.services_categories.form')
    </form>
@endsection