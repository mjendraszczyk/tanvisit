@extends('admin.layouts.app')
@section('title')
Kaegorie usług - Edycja
@endsection
@section('admin_section')
<form method="POST" action="{{route('admin_servicescategories_update', ['id' => $services_categories->id_service_category])}}">
    @method('PUT')
    @include('admin.services_categories.form')
</form>
@endsection
