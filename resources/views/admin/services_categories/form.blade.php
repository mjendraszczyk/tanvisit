@csrf

<div class="col s12 m12 l12">
    @include('admin/core/notification')
 
    <label>Nazwa</label>
    <input required placeholder="Nazwa" name="name" @if(Route::currentRouteName()=='admin_servicescategories_edit' )
        value="{{$services_categories->name}}" @else value="" @endif type="text" class="form-control-input form-control validate">

</div>
<div class="panel-footer">
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>
</div>