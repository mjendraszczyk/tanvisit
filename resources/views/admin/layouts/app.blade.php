@include('admin.core.head')

<body>
    @include('admin.core.nav')
    <div @if(Auth::user()) id="wrapper" @endif @if(\App\Http\Controllers\Controller::checkMobile()) @else class="toggled" @endif>
        @include('admin.core.menu') 
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div id="app">
                    <div class="app-content">
                        <div class="col-md-12">
                            @if(View::hasSection('title'))
                            <div class="panel-heading">
                                <div class="row">
                                <div class="col-md-8">
                                @yield('title')
                                </div>
                                <div class="col-md-4 text-right">
                                    @if(View::hasSection('options'))
                                        @yield('options')
                                    @endif
                                </div>
                            </div>
                            </div>                        
                            @endif
                            @yield('header')
                            {{--<div class="panel-body">--}}
                            @yield('content')
                            {{--</div>--}}
                            @if(View::hasSection('admin_section'))
                                    <div class="panel">
                                        <div class="panel-default">
                                            @yield('admin_section')
                                        </div>
                                    </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.core.footer')