@extends('admin.layouts.app')
@section('title')
Klient - Tworzenie
@endsection
@section('admin_section')
    <form method="POST" action="{{route('admin_customers_store')}}">
        @include('admin.customers.form')
    </form>
@endsection