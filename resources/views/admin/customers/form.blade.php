@csrf

<div class="col s12 m12 l12">
    @include('admin/core/notification')
   
 
    <label>Imię</label>
    <input required placeholder="Imię" name="firstname" @if(Route::currentRouteName()=='admin_customers_edit' )
        value="{{$customers->firstname}}" @else value="" @endif type="text" class="form-control-input form-control validate">

<label>Nazwisko</label>
<input placeholder="Nazwisko" name="lastname" @if(Route::currentRouteName()=='admin_customers_edit' )
    value="{{$customers->lastname}}" @else value="" @endif type="text" class="form-control-input form-control validate">

<label>E-mail</label>
<input placeholder="E-mail" name="email" @if(Route::currentRouteName()=='admin_customers_edit' )
    value="{{$customers->email}}" @else value="" @endif type="text" class="form-control-input form-control validate">

<label>Kierunkowy</label>
<select name="prefix_phone" class="form-control">
    @foreach ($prefix_phone as $key => $prefix)
        <option value="{{$prefix}}" @if(Route::currentRouteName()=='admin_customers_edit' ) @if($prefix == $customers->prefix_phone) selected="selected" @endif @endif>{{$key}}</option>
    @endforeach
</select>
    <label>Telefon</label>
    <input required placeholder="Telefon" name="phone" @if(Route::currentRouteName()=='admin_customers_edit' )
        value="{{$customers->phone}}" @else value="" @endif type="text" class="form-control-input form-control validate">


        <label>Notatka:</label>
        @if(Route::currentRouteName()=='admin_customers_edit' )
        <textarea class="form-control" name="private_note">{{$customers->private_note}}</textarea>
        @else
        <textarea class="form-control" name="private_note"></textarea>
        @endif

</div>
<div class="panel-footer">
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>
</div>