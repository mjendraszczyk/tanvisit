﻿@extends('admin.layouts.app')
@section('title')
Klienci
@endsection
@section('options')
<a href="{{route('admin_customers_create')}}" class="btn btn-success">
    Dodaj
</a>
<a href="{{route('admin_customers_export')}}" class="btn btn-secondary">
    Eksport CSV
</a>
@endsection
@section('admin_section')
<table class="table">
    <thead>
        <th>ID</th>
        <th>Imię</th>
        <th>Nazwisko</th>
        <th>Telefon</th>
        <th>E-mail</th>
        <th class="text-right">Opcje</th>
    </thead>
      @foreach ($customers as $customer)
    <tr>
        <td>
            {{$customer->id_customer}}
        </td>
    <td>
        {{$customer->firstname}}
    </td>
    <td>
        {{$customer->lastname}}
    </td>
    <td>
        {{$customer->phone}}
    </td>
    <td>
        {{$customer->email}}
    </td>
    <td class="text-right">
        <a href="{{route('admin_customers_edit',['id'=>$customer->id_customer])}}" class="btn btn-default">
            <i class="fa fa-edit"></i>
        </a>
        <form class="inline" method="POST" action="{{route('admin_customers_destroy',['id'=>$customer->id_customer])}}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-remove"></i>
            </button>
        </form>
    </td>
    </tr>
    @endforeach  
    {{ $customers->links() }}
</table>
@endsection