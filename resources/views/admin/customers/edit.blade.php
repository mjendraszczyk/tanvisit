@extends('admin.layouts.app')
@section('title')
Klient - Edycja
@endsection
@section('admin_section')
<form method="POST" action="{{route('admin_customers_update', ['id' => $customers->id_customer])}}">
@method('PUT')
    @include('admin.customers.form')
</form>
@endsection
