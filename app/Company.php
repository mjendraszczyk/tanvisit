<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $primaryKey = 'id_company';
    public $incrementing = true;
    protected $table = 'companies';
     protected $fillable = [
        'name',
        'vat_number',
        'email',
        'address',
        'phone',
        'city',
        'id_country',
        'description',
        'facebook',
        'instagram',
        'certificate',
        'store'
    ];
}
