<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $primaryKey = 'id_subscription';
    public $incrementing = true;
    protected $table = 'subscriptions';
     protected $fillable = [
        'name',
        'value',
    ];
}
