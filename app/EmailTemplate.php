<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
      protected $primaryKey = 'id_emailtemplate';
    public $incrementing = true;
    protected $table = 'emailtemplates';
     protected $fillable = [
        'id_company',
        'content',
        'id_type',
    ];
}
