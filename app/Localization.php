<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Localization extends Model
{
      protected $primaryKey = 'id_localization';
    public $incrementing = true;
    protected $table = 'localizations';
     protected $fillable = [
        'name',
    ];
}
