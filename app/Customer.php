<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $primaryKey = 'id_customer';
    public $incrementing = true;
    protected $table = 'customers';
     protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'phone',
        'private_note',
        'prefix_phone',
        //'id_company'
    ];
}
