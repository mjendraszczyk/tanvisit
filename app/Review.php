<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
         protected $primaryKey = 'id_review';
    public $incrementing = true;
    protected $table = 'reviews';
     protected $fillable = [
        'id_company',
        'rate',
        'comment',
        'status',
        'id_customer',
        'name',
    ];
}
