<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
    protected $primaryKey = 'id_sms';
    public $incrementing = true;
    protected $table = 'sms';
     protected $fillable = [
        'id_type',
        'content',
        'id_company',
    ];
}
