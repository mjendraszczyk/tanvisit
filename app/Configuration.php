<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
       protected $primaryKey = 'id_configuration';
    public $incrementing = true;
    protected $table = 'configurations';
     protected $fillable = [
        'key',
        'value',
    ];
}
