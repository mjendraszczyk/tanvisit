<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicePrice extends Model
{
    protected $primaryKey = 'id_service_price';
    public $incrementing = true;
    protected $table = 'services_prices';
     protected $fillable = [
        'id_user',
        'id_service',
        'price',
        'time'
    ];
}
