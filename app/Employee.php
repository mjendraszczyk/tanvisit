<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
      protected $primaryKey = 'id_employee';
    public $incrementing = true;
    protected $table = 'employees';
     protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'phone',
        'id_company',
        'id_user'
    ];
}
