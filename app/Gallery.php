<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $primaryKey = 'id_gallery';
    public $incrementing = true;
    protected $table = 'galeries';
     protected $fillable = [
        'id_company',
        'position',
        'label',
        'filename',
        'feature',
    ];
}
