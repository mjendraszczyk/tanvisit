<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $primaryKey = 'id_service';
    public $incrementing = true;
    protected $table = 'services';
     protected $fillable = [
        'id_service_category',
        'name',
    ];
}
