<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCategory extends Model
{
    protected $primaryKey = 'id_service_category';
    public $incrementing = true;
    protected $table = 'services_categories';
     protected $fillable = [
        'name',
    ];
}
