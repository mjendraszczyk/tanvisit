<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $primaryKey = 'id_country';
    public $incrementing = true;
    protected $table = 'countries';
     protected $fillable = [
        'name',
    ];
}
