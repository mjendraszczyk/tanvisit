<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Role;

use App\EmailTemplate;
use ShortenerManager;

use Jenssegers\Agent\Agent;

use Smsapi\Client\Curl\SmsapiHttpClient;
use Smsapi\Client\Service\SmsapiComService;
use Smsapi\Client\SmsapiClient;
use Smsapi\Client\Feature\Sms\Bag\SendSmsBag;
use Illuminate\Support\Facades\Mail;

use App\Http\Controllers\admin\AdminServicesController;
use App\Http\Controllers\admin\AdminCustomersController;
use App\Http\Controllers\admin\AdminEmployeesController;
use App\Http\Controllers\admin\AdminVisitsController;
use App\Http\Controllers\admin\AdminCompaniesController;

use App\Employee;
use App\Sms;
use App\Company;

use Illuminate\Support\Facades\Input;
use Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // public $visit_status = array(
    //     "1" => "Potwierdzona",
    //     "2" => "Niepotwierdzona",
    //     "0" => "Anulowana",
    // );
  
    public $types;
    public static $types_static;
    
  
 

    public $phone_prefix = array(
        "+48" => "48", // Polska
        "+49" => "49", // Niemcy
        "+40" => "40", // Rumunia
        "+46" => "46", // Szwecja
        "+36" => "36", // Węgry
        "+39" => "39", // Włochy
        "+44" => "44", // Wielka brytania
        "+41" => "41", // Szwajcaria
        "+380" => "380", // Ukraina
        "+351" => "351", // Portugalia
        "+47" => "47", // Norwegia
        "+370" => "370", // Litwa
        "+371" => "371", //Łotwa
        "+352" => "352", // Luksemburg
        "+353" => "353", // Irlandia
        "+354" => "354", //Islandia
        "+31" => "31", // Holandia
        "+34" => "34", // Hiszpania
        "+30" => "30", // Grecja
        "+33" => "33", //Francja
        "+358" => "358", //Finlandia
        "+45" => "45", // Dania
        "+43" => "43",  // Austria
        "+32" => "32",  //Belgia
        "+359" => "359", //Bułgaria
        "+375" => "375", //Białorus
        "+420" => "420",   //Czechy
        "+421" => "421",   //Slowacja
    );
    public $FileName;

    public $days = array(
        0 => "Niedziela",
        1 => "Poniedziałek",
        2 => "Wtorek",
        3 => "Środa",
        4 => "Czwartek",
        5 => "Piątek",
        6 => "Sobota",
    );
    public $pagination_limit = 25;

   
    public function __construct() {
        $types_proto = array(
        "create" => array(
           "label" => "create",
           "value" => "1"
        ),
        "remind" =>  array(
           "label" => "remind",
           "value" => "2"
        ),
        "update" =>  array(
           "label" => "update",
           "value" => "3"
        )
    );

        $this->types = $types_proto;
        self::$types_static = $types_proto;
    }
    public static function getRola($id_role) {
        return Role::find($id_role)->first();
    }
    public static function getRoleName($id_role) {
        try {
            return Role::where('id_role', $id_role)->first()->name;
        } catch(\Exception $e) {
            return false;
        }
    }
    public static function checkMobile() {
         $agent = new Agent();
         if($agent->isMobile()) {
             return true;
         } else {
             return false;
         }
    }
    public static function getCompanyId($id_user) {

        try {
            $getEmployee = Employee::where('id_user', $id_user)->first();

            return $getEmployee->id_company;
        } catch (\Excepton $e) {
            return false;
        }
    }

   

      public function upload($input, $extensions, $path, $tytul, $request, $sizeX, $sizeY)
    {
        if ($extensions == 'jpg,jpeg,png') {
            $this->validate($request, [
            $input => 'image|mimes:'.$extensions.'|max:2048',
        ]);
        } else {
            $this->validate($request, [
            $input => 'file|mimes:'.$extensions.'|max:2048',
            ]);
        }


        $file = $request->file($input);
        $this->FileName = md5(str_slug($tytul.time())).'.'.$file->getClientOriginalExtension();
        $destinationPath = public_path($path);

        if ($extensions == 'jpg,jpeg,png') {
            $img = Image::make($file->path());
            $img->resize($sizeX, $sizeY, function ($constraint) {//200,200
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$this->FileName);
        } else {
            $filePath = $destinationPath.'/'.$this->FileName;
            $file->move($destinationPath, $this->FileName);
        }
    }

    public function getFileName()
    {
        return $this->FileName;
    }

    public static function getCompanyNameByUser($id_user) {
        try {
            $getEmployee = Employee::where('id_user', $id_user)->first();

            $getCompany = Company::where('id_company', $getEmployee->id_company)->first();
            return $getCompany->name;
        } catch (\Excepton $e) {
            return false;
        }
    }
    public static function sendEmail($visit, $customer, $status, $unit) {
        try {
            $getEmployee = Employee::where('id_employee', $visit->id_employee)->first();
            $email = trim($customer->email);
            
            $konto = array(
                    'service' =>  AdminServicesController::getServiceName($visit->id_service),
                    'employee' =>  AdminEmployeesController::getEmployeeName($visit->id_employee),
                    'customer' =>  AdminCustomersController::getCustomerName($visit->id_customer),
                    'status' => AdminVisitsController::getVisitStatus($visit->confirm),
                    'company' => AdminCompaniesController::getCompanyName($getEmployee->id_company),
                    'phone' => AdminCustomersController::getCustomerPhone($visit->id_customer),
                    'datetime_from' => $visit->datetime_from,
                    'id_visit' => $visit->id_visit,
                    'phoneEmployee' => AdminEmployeesController::getEmployeePhone($visit->id_employee),
            );
            
            //
            //sprawdz czy istnieje dla tej firmy szablon z okreslonym typem, jesli tak wybierz go, jesli nie wyslij z domyslnym schematem

            $countEmailTemplate = EmailTemplate::where('id_company', $getEmployee->id_company)->where('id_type', self::$types_static[$status]['value'])->count();
            // echo "TYP".self::$types_static[$status]['value'];
            // echo "COMPANY".$getEmployee->id_company;
            // echo "COUNT".count($getEmailTemplate);
            // exit();
              $shortener = url()->shortener();
//dd($countEmailTemplate);
            if(($countEmailTemplate) > 0) {
                  $getEmailTemplate = EmailTemplate::where('id_company', $getEmployee->id_company)->where('id_type', self::$types_static[$status]['value'])->first();

                $custom = 1;

                $email_content  = $getEmailTemplate->content;
$email_tags = ["{service}", "{employee}", "{customer}", "{status}", "{company}", "{phone}" , "{datetime_from}", "{phoneEmployee}", "{active_link}"];
$email_func   = [AdminServicesController::getServiceName($visit->id_service), AdminEmployeesController::getEmployeeName($visit->id_employee), 
AdminCustomersController::getCustomerName($visit->id_customer),
AdminVisitsController::getVisitStatus($visit->confirm),
AdminCustomersController::getCustomerPhone($visit->id_customer),
$visit->datetime_from,
AdminEmployeesController::getEmployeePhone($visit->id_employee),
$shortener->shorten(route('front_visits_accept', ['id'=>$visit->id_visit]))
];

$content = str_replace($email_tags, $email_func, $email_content);

                
            } else {
                $custom = 0;
                $content = '';
            }
            //
            if ($status == 'create') {
                $temat = 'Utworzenie wizyty w serwisie '.env('APP_NAME');
            } elseif ($status == 'remind') {
                $temat = 'Przypomnienie zarezerwowanej wizyty '.env('APP_NAME');
            } else {
                $temat = 'Aktualizacja wizyty w serwisie '.env('APP_NAME');
            }
          
            $konto['custom'] =  $custom;
            $konto['content'] =  $content;
            // dd($konto);
            // exit();
            $data = array('temat'=>$temat, 'email'=>$email, 'konto'=>$konto,'short_link' => $shortener->shorten(route('front_visits_accept', ['id'=>$visit->id_visit])));
            if (($unit == 'employee') && ($status == 'create')) {
                $path_admin = '_admin';
            } else {
                $path_admin = '';
            }
           
            try {
                Mail::send('mails.'.$status.'_visits'.$path_admin, $data, function ($message) use ($temat, $email) {
                    $message->from(env('MAIL_USERNAME'), $temat);
                    $message->to($email)->subject($temat);
                });
            } catch (\Exception $e) {
                return false;
                //return false;
            }
        } catch (\Exception $e) {
            dd($e);
            //return false;
        }
    }
    public static function sendSms($visit, $customer, $status, $unit) {
        try {
            $apiToken = 'IO6jW9yXiYxPFM978pvvHa10anOur6ZfCWHnCDEV';
            $client = new SmsapiHttpClient();
            $service = $client->smsapiPlService($apiToken);

            $getEmployee = Employee::where('id_employee', $visit->id_employee)->first();


            /// własne szablony sms


                $countSmsTemplate = Sms::where('id_company', $getEmployee->id_company)->where('id_type', self::$types_static[$status]['value'])->count();
       
                $shortener = url()->shortener();

                if (($countSmsTemplate) > 0) {
                    $getSmsTemplate = Sms::where('id_company', $getEmployee->id_company)->where('id_type', self::$types_static[$status]['value'])->first();

                    $custom = 1;

                    $sms_content  = $getSmsTemplate->content;
                    
                    $sms_tags = ["{service}", "{employee}", "{customer}", "{status}", "{company}", "{phone}" , "{datetime_from}", "{phoneEmployee}", "{active_link}"];
                
                    $sms_func   = [
                        Controller::removeStaticPolishChars(AdminServicesController::getServiceName($visit->id_service)), 
                        Controller::removeStaticPolishChars(AdminEmployeesController::getEmployeeName($visit->id_employee)),
                        Controller::removeStaticPolishChars(AdminCustomersController::getCustomerName($visit->id_customer)),
                Controller::removeStaticPolishChars(AdminVisitsController::getVisitStatus($visit->confirm)),
                Controller::removeStaticPolishChars(AdminCustomersController::getCustomerPhone($visit->id_customer)),
                $visit->datetime_from,
                AdminEmployeesController::getEmployeePhone($visit->id_employee),
                $shortener->shorten(route('front_visits_accept', ['id'=>$visit->id_visit]))
                ];

                    $content = str_replace($sms_tags, $sms_func, $sms_content);
                }  else {
                    $custom = 0;
                    $content = '';
                }
            ////
            $messageRemind = 'Przypominamy o wizycie: '.Controller::removeStaticPolishChars(AdminServicesController::getServiceName($visit->id_service)).' '.$visit->datetime_from. '. Kliknij aby potwierdzic lub odwolac';
            $messageCreate = 'Potwierdzamy Twoja wizyte : '.Controller::removeStaticPolishChars(AdminServicesController::getServiceName($visit->id_service)).' '.$visit->datetime_from. '. Do zobaczenia :)';
            $messageUpdate = 'Zmiana terminu wizyty na '.Controller::removeStaticPolishChars(AdminServicesController::getServiceName($visit->id_service)).' zostal zakutalizowany: '.$visit->datetime_from;

            if ($status == 'remind') {
                $message = $messageRemind;
            } else if($status == 'update') {
                $message = $messageUpdate;
            }  else {
                $message = $messageCreate;
            }

                 $shortener = url()->shortener();
      

                $potwierdzenie = '';
                if ((($visit->confirm == 0) || ($visit->confirm == 2)) && ($status == 'remind')) {
                    $potwierdzenie = " ".$shortener->shorten(route('front_visits_accept', ['id'=>$visit->id_visit]))."\n lub zadzwon ".AdminEmployeesController::getEmployeePhone($visit->id_employee);
                } else {
                    $potwierdzenie = '';
                }

                // Jezeli mamy wlasny szablon SMS
                if($custom == 1) {
                    $message = $content;
                } else {
                    $message .= $potwierdzenie;
                }
                //$message = 'Zostala zarezerwoana wizyta na: '.AdminServicesController::getServiceName($visit->id_service).". \n Termin: ".$visit->datetime_from.". \n".$potwierdzenie."";
            
            //$tel = '794746611';
            $tel = $customer->phone_prefix.$customer->phone;
            // $tel = '732715809';
            $sms = SendSmsBag::withMessage($tel, $message);
            $sms->from = "Kurkowa5";

            $service->smsFeature()->sendSms($sms);
            } catch(\Exception $e) {
                return false;
            }
    }


    public static function formatPrice($price){ 
        return number_format($price, 2, '.', ' ').' zł';
    }
    public static function removeStaticPolishChars($string) {
        $a = array('ą', 'ę', 'ó', 'ź', 'ł', 'ń', 'ś', 'ć', 'ż', 'Ł', 'Ń', 'Ż', 'Ć', 'Ź', 'Ó', 'Ę', 'Ą', 'Ś');
        $b = array('a', 'e', 'o', 'z', 'l', 'n', 's', 'c', 'z', 'L', 'N', 'Z', 'C','Z', 'O', 'E', 'A', 'S');
         return str_replace($a, $b, $string);
    }
    public function removePolishChars($string) {
        return Controller::removeStaticPolishChars($string);
    }
}
