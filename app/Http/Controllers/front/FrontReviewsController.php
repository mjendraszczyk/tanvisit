<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Review;
use App\Company;

class FrontReviewsController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $review = new Review();
        $review->comment = $request->input('comment');
        $review->rate = $request->input('rate');
        $review->id_company = $request->input('id_company');
        $review->name = $request->input('name');
        $review->status = 0;
        $review->id_customer = null;
        $review->save();

        $getCompany = Company::where('id_company', $request->input('id_company'))->first();

        return redirect()->route('front_profil',['id'=>$request->input('id_company'),'name'=>str_slug($getCompany->name)]);
    }
}
