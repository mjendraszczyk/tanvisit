<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminVisitsRequest;
use App\Visit;
use App\Service;
use App\Employee;
use App\Customer;
use Session;

class FrontVisitsController extends Controller
{
    public function acceptVisit($id_visit) {
        return view('front.visits.process')->with('id_visit', $id_visit);
        
    }
    public function processVisit(Request $request, $id_visit) {
        //return view('front.visits.process');
       
        if($request->input('visits_process') == 1) {
        try {
            $getVisit = Visit::where('id_visit', $id_visit)->update([
                'confirm' => 1,
                'color' => 'green'
            ]);
        } catch (\Exception $e) {
            return false;
        }

        return view('front.visits.confirmed')->with('id_visit', $id_visit);
        } else if($request->input('visits_process') == 0) {

             try {
            $getVisit = Visit::where('id_visit', $id_visit)->update([
                'confirm' => 0,
                'color' => 'red'
            ]);
        } catch (\Exception $e) {
            return false;
        }

        return view('front.visits.canceled')->with('id_visit', $id_visit);
        } else {

            try {
                $getVisit = Visit::where('id_visit', $id_visit)->first();
                $date_from = $request->input('inny_termin');
                $addHour = date('H', strtotime($getVisit->hours));
                $addMinute = date('i', strtotime($getVisit->hours));
                $dateTo = date('Y-m-d H:i:s', strtotime($date_from.' +'.$addHour.' hour + '.$addMinute.' minutes'));
            
            $updateVisit = Visit::where('id_visit', $id_visit)->update([
            'confirm' => 2,
            'color' => 'grey',
            'datetime_from' => $request->input('inny_termin'),
            'datetime_to' => $dateTo,
        ]);
        } catch (\Exception $e) {
            return false;
        }

            return view('front.visits.delay')->with('id_visit', $id_visit);
        }
    
    }
    // public function confirmVisit($id_visit) {
    //     try {
    //         $getVisit = Visit::where('id_visit', $id_visit)->update([
    //         'confirm' => 1,
    //         'color' => 'green'
    //     ]);
    //         return view('front.visits.confirmed');
    //     } catch(\Exception $e) {
    //      return print_r($e);
    //     }
    // }
   
   
    public function index() {
        $services = Service::get();
        $employees = Employee::get();
        return view('front.visits.index')->with('services',$services)->with('employees',$employees);
    }
    public function store(AdminVisitsRequest $request, $id) {
        try {
         
            date_default_timezone_set('Europe/Warsaw');

            $getCustomer = Customer::where('phone',$request->input('prefix_phone').$request->input('phone'))->count();

            if($getCustomer > 0) {
                if(!empty($request->input('firstname'))) {
                   Customer::where('phone', $request->input('prefix_phone').$request->input('phone'))->update([
                        'firstname' => $request->input('firstname')
                   ]);
                     $customer = Customer::where('phone', $request->input('prefix_phone').$request->input('phone'))->first();
                } else {
                    $customer = Customer::where('phone', $request->input('prefix_phone').$request->input('phone'))->first();
                }
            } else {
                $customer = new Customer();
                $customer->phone = $request->input('prefix_phone').$request->input('phone');
                $customer->prefix_phone = $request->input('prefix_phone');
                $customer->save();
            }


            $visit = new Visit(); 
            $visit->id_customer = $customer->id_customer;
            $visit->datetime_from = $request->input('datetime_from');
            $visit->datetime_to = $request->input('datetime_to');
            $visit->hours = date('H:i:s', strtotime($request->input('hours')));
            $visit->id_callendar = $id;
            $visit->color = 'grey';
            $visit->id_service = $request->input('id_service');
            $visit->confirm = 2; // niepotwierdzona
            $visit->id_employee = $request->input('id_employee'); // niepotwierdzona
            $visit->valid = 0;
            $visit->save();

              Session::flash('success_message', 'Zapytano o wizytę');

            $employee = Employee::where('id_employee', $request->input('id_employee'))->first();
/**
  * Wyslij email
  */

   
  Controller::sendEmail($visit, $customer, 'create', 'customer');
    Controller::sendEmail($visit, $employee, 'create', 'employee');

             /**
             * Wyslij sms
             */

//             Controller::sendSms($visit, $customer, 'create', 'customer');
            // Controller::sendSms($visit, $employee, 'create', 'employee');

        } catch (\Exception $e) {
            dd($e);
        }
        //Session::flash('success_message', 'Wizyta została zarezerwowana');
        return redirect()->back();
    }
    public function cronSmsEmail() {
        $getVisit = Visit::where('datetime_from', '>=', date('Y-m-d H:i:s', strtotime('-1 day')))->where('confirm','!=','0')->where('mail', '=', '0')->get();
      
        foreach ($getVisit as $visit) {

            if($visit->datetime_from > date('Y-m-d H:i:s')) {
                try {
                    $customer = Customer::where('id_customer', $visit->id_customer)->first();
                    $employee = Employee::where('id_employee', $visit->id_employee)->first();

                    $Visit = Visit::where('id_visit', $visit->id_visit)->update([
                    'mail' => 1
                ]);
            
             // dd("test");
                    /**
                     * Email
                     */
                    Controller::sendEmail($visit, $customer, 'remind', 'customer');
                    Controller::sendEmail($visit, $employee, 'remind', 'employee');
                    
                    /**
                     * Sms
                     */
                    Controller::sendSms($visit, $customer, 'remind', 'customer');
                   // Controller::sendSms($visit, $employee, 'remind', 'employee');


                } catch (\Exception $e) {
                //    dd($e);
                    //return false;
                }
            }
        } 
        echo "process successfully()";
    }
}
