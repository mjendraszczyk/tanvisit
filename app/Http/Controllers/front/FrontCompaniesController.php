<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company;
use App\Service;
use App\ServiceCategory;
use App\Review;
use App\ServicePrice;
use App\Opening;
use App\Employee;
use App\Gallery;

class FrontCompaniesController extends Controller
{

         public function renderWidget($id) {
            $services = Service::join('services_prices', 'services.id_service','services_prices.id_service')->where('id_user',$id)->get();

            return view('front.companies.widget')->with('services', $services);
        }
        
    public function profil($id, $name) {
        try {
              
            $services = Service::get();
            $galleries = Gallery::where('id_company', $id)->get();
            $companies = Company::where('id_company', $id)->first();
            $services_categories = ServiceCategory::get();
            // dd("TES");
            $reviews = Review::where('status', '1')->where('id_company', $id)->get();

            
            $countGallery = Gallery::where('id_company', $id)->where('feature', 1)->count();
            $featureGallery = Gallery::where('id_company', $id)->where('feature', 1)->first();
            
            $services_prices = ServicePrice::join('services', 'services.id_service','services_prices.id_service')->where('id_user', $id)->groupBy('services.id_service_category')->get();

             $all_services_prices = ServicePrice::join('services', 'services.id_service','services_prices.id_service')->where('id_user', $id)->orderBy('id_service_category','asc')->get();
            // dd($services_prices);
            //->join('services_categories', 'services_categories.id_service_category', 'services.id_service_category')

            $openings = Opening::where('id_company', $id)->get();
            $employees = Employee::where('id_company', $id)->get();

            
            return view('front.companies.profil')->with('companies', $companies)->with('services', $services)->with('id', $id)
            ->with('reviews', $reviews)->with('services_prices', $services_prices)->with('openings', $openings)->with('days', $this->days)->with('employees', $employees)->with('galleries', $galleries)->with('countGallery', $countGallery)->with('featureGallery', $featureGallery)->with('prefix_phone', $this->phone_prefix)->with('all_services_prices',$all_services_prices);
        } catch(\Exception $e) {
            return false;
        }
    }
    public static function getServicesFromCategoryCompany($id_company, $id_service_category) {
        try {
            return  ServicePrice::join('services', 'services.id_service','services_prices.id_service')->where('id_user', $id_company)->where('id_service_category', $id_service_category)->get();
        } catch(\Excepton $e) {
            return false;
        }
    }
}
