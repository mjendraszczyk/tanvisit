<?php

namespace App\Http\Controllers\admin;
use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;


class AdminCountriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::paginate($this->pagination_limit);
        return view('admin.countries.index')->with('countries',$countries);
    }

    public static function getCountryName($id_country) {
        try {
            return Country::findOrFail($id_country)->first()->name;
        } catch (\Exception $e) {
            return false;
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.countries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         try {
            $country = new Country();
            $country->name = $request->input('name');
            $country->save();

            Session::flash('success_message', 'Dodano pomyślnie.');
            return redirect()->route('admin_countries_edit',['id'=>$country->id_country]);
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $countries = Country::where('id_country', $id);
        return view('admin.countries.edit')->with('countries',$countries);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
           try {
            $countries = Country::findOrFail($id)->update([
                'name' => $request->input('name'),
            ]);
            Session::flash('success_message', 'Zaktualizowano pomyślnie.');
            return redirect()->route('admin_countries_edit',['id'=>$id]);
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          try {
            $countries = Country::findOrFail($id)->delete();
            return redirect()->route('admin_countries_index');
        } catch (\Exception $e) {
            return redirect()->route('admin_countries_index');
        }
    }
}
