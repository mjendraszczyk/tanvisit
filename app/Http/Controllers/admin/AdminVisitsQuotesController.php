<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Visit;

class AdminVisitsQuotesController extends Controller
{
    public function index() {
        $datetime_from = date('Y-m-d');
        $datetime_to = date('Y-m-d');
        $visitsquotes = Visit::where('valid', '0')->orderBy('id_visit','desc')->paginate($this->pagination_limit);
        return view('admin.visitsquotes.index')->with('visitsquotes', $visitsquotes)->with('datetime_from', $datetime_from)->with('datetime_to', $datetime_to);
    }
    public function filtr(Request $request) {
        $datetime_from = $request->input('datetime_from');
        $datetime_to = $request->input('datetime_to');
        $visitsquotes = Visit::where('valid', '0')->whereBetween('datetime_from', [$request->input('datetime_from'),$request->input('datetime_to')])->paginate($this->pagination_limit);
        //->where('datetime_from', '<=', $request->input('datetime_to'))
        return view('admin.visitsquotes.index')->with('visitsquotes', $visitsquotes)->with('datetime_from', $datetime_from)->with('datetime_to', $datetime_to);
    }
    public function destroy($id) {
        try {
            Visit::where('id_visit', $id)->delete();
            return redirect()->back();
        } catch (\Exception $e) {
            return false;
        }
    }
    public function update(Request $request, $id) {
        try {
            Visit::where('id_visit', $id)->update([
            'valid' => 1
        ]);
            return redirect()->route('admin_visits_edit', ['id' => $id]);
        } catch(\Exception $e) { 
            return false;
        }
    }
}
