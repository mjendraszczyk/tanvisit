<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Review;
use Auth;

class AdminReviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(\App\Http\Controllers\Controller::getCompanyId(Auth::user()->id)) {
           $reviews = Review::where('id_company', \App\Http\Controllers\Controller::getCompanyId(Auth::user()->id))->paginate($this->pagination_limit);
        } else {
            $reviews = Review::paginate($this->pagination_limit);
        }
       return view('admin.reviews.index')->with('reviews',$reviews);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.reviews.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
        $reviews = new Review();
        $reviews->name = $request->input('name');
        $reviews->comment = $request->input('comment');
        $reviews->rate = $request->input('rate');
        $reviews->status = $request->input('status');
        $reviews->save();
        
        return redirect()->route('admin_reviews_edit',['id_review'=>$id]);
        } catch (\Exception $e) {
        return false;
    }
}

    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reviews = Review::where('id_review', $id)->first();
        return view('admin.reviews.edit')->with('reviews', $reviews)->with('id', $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $reviews = Review::where('id_review', $id)->update([
                'name' => $request->input('name'),
                'rate' => $request->input('rate'),
                'comment' => $request->input('comment'),
                'status' => $request->input('status'),
            ]);
            return redirect()->route('admin_reviews_edit',['id_review'=>$id]);
        } catch (\Exception $e) {
       return false;
  }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $reviews = Review::where('id_review', $id)->delete();
            return redirect()->route('admin_reviews_index');
        } catch (\Exception $e) {
            return false;
        }
    }
}
