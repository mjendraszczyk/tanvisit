<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;
use App\ServiceCategory;
use App\ServicePrice;
use App\User;
use App\Employee;
use App\Company;
use Session;
use Auth;

class AdminServicesPricesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services_prices = ServicePrice::where('id_user', \App\Http\Controllers\admin\AdminEmployeesController::getCompanyByUser(Auth::user()->id)->id_company)->paginate(25);
        return view('admin.services_prices.index')->with('services_prices',$services_prices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = Service::get();
        $companies = Company::get();
        return view('admin.services_prices.create')->with('users',$companies)->with('services',$services);
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          try {
            $services_prices = new ServicePrice(); 
             $services_prices->id_user = $request->input('id_user');
              $services_prices->price = $request->input('price');
            $services_prices->time = $request->input('time');
             $services_prices->id_service = $request->input('id_service');
            $services_prices->save();

             Session::flash('success_message', 'Zapisano pomyślnie.');

            return redirect()->route('admin_servicesprices_edit',['id'=>$services_prices->id_service_price]);
        } catch (\Exception $e) {
            dd($e);
            return view('admin.core.404');
        }
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
                 try {
                     $services = Service::get();
              $services_prices = ServicePrice::findOrFail($id);

              $companies = Company::get();
            return view('admin.services_prices.edit')->with('services_prices',$services_prices)->with('services',$services)->with('users',$companies);
        } catch (\Exception $e) {
            return view('admin.core.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
                try {
              $services_prices = ServicePrice::findOrFail($id)->update([
                'time' => $request->input('time'),
                'id_service' => $request->input('id_service'),
                'price' => $request->input('price'),
                'id_user' => $request->input('id_user'),
              ]);

Session::flash('success_message', 'Zaktualizowano pomyślnie.');
            return redirect()->route('admin_servicesprices_edit',['id'=>$id]);
        } catch (\Exception $e) {
            dd($e);
            //return view('admin.core.404');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            try {
            $services_prices = ServicePrice::findOrFail($id)->delete();
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }
    public function getServiceTime($id_service, $id_company) {
        try {
            //echo "F".$id_service;
            return ServicePrice::where('id_service', $id_service)->where('id_user',$id_company)->first()->time;
            
        } catch(\Exception $e) {
            //return false;
            dd($e);
        }
    }
}
