<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EmailTemplate;
use Auth;
use Session;

class AdminEmailTemplatesController extends Controller
{
  

 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emailtemplates = EmailTemplate::paginate(25);
        return view('admin.emailtemplates.index')->with('emailtemplates',$emailtemplates)->with('types',$this->types);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $emailtemplates = EmailTemplate::get();
        return view('admin.emailtemplates.create')->with('types',$this->types);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $emailtemplates = new EmailTemplate(); 
            $emailtemplates->id_type = $request->input('id_type');
            $emailtemplates->content = $request->input('content');
            $emailtemplates->id_company = Controller::getCompanyId(Auth::user()->id);
            $emailtemplates->save();

            return redirect()->route('admin_emailtemplates_edit',['id'=>$emailtemplates->id_emailtemplate]);
        } catch (\Exception $e) {
           // dd($e);
           return false;
            //return view('admin.core.404');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          try {
            $emailtemplates = EmailTemplate::where('id_emailtemplate', $id)->first();

            return view('admin.emailtemplates.edit')->with('emailtemplates',$emailtemplates)->with('types',$this->types);
        } catch (\Exception $e) {
            return view('admin.core.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $emailtemplates = EmailTemplate::findOrFail($id)->update([
                'content' => $request->input('content'),
                'id_type' => $request->input('id_type')
            ]);
            Session::flash('success_message', 'Zaktualizowano pomyślnie.');
            return redirect()->route('admin_emailtempates_edit',['id'=>$id]);
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $emailtemplates = EmailTemplate::findOrFail($id)->delete();
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }

}
