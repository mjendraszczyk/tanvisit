<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ServiceCategory;
use Session;

class AdminServicesCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services_categories = ServiceCategory::paginate(25);
        return view('admin.services_categories.index')->with('services_categories',$services_categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           try {
            $services_category = new ServiceCategory(); 
            
            $services_category->name = $request->input('name');
            $services_category->save();

            return redirect()->route('admin_servicescategories_edit',['id'=>$services_category->id_service_category]);
        } catch (\Exception $e) {
            return view('admin.core.404');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
               try {
              $services_categories = ServiceCategory::findOrFail($id);

            return view('admin.services_categories.edit')->with('services_categories',$services_categories);
        } catch (\Exception $e) {
            return view('admin.core.404');
        }
    }

    public static function getServiceCategoryName($id_service_category) {
        try {
            return ServiceCategory::where('id_service_category', $id_service_category)->first()->name;
        } catch(\Exception $e) {
            return false;
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            try {
            $services_categories = ServiceCategory::findOrFail($id)->update([
                'name' => $request->input('name')
            ]);
            Session::flash('success_message', 'Zaktualizowano pomyślnie.');
            return redirect()->route('admin_servicescategories_edit',['id'=>$id]);
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
             try {
            $services = ServiceCategory::findOrFail($id)->delete();
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }
}
