<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Employee;
use App\Company;
use App\User;
use App\Callendar;
use Session;
use App\Service;
use Illuminate\Support\Facades\Mail;
class AdminEmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::paginate($this->pagination_limit);
        return view('admin.employees.index')->with('employees',$employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::get();
        return view('admin.employees.create')->with('companies',$companies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          try {
              if ($request->input('password') == $request->input('password_repeat')) {
                  $services = Service::get();
                  $user = new User();
                  $user->email = $request->input('email');
                  $user->password = bcrypt($request->input('password'));
                  $user->save();


                  $callendar = new Callendar();
                  $callendar->id_user = $user->id;
                  $callendar->save();

                  $employees = new Employee();
                  $employees->firstname = $request->input('firstname');
                  $employees->lastname = $request->input('firstname');
                  $employees->email = $request->input('email');
                  $employees->phone = $request->input('phone');
                  $employees->id_user = $user->id;
                  $employees->save();

                     $email = trim($employees->email);
                $konto = array(
                        'firstname' =>  $employees->firstname,
                        'lastname' =>  $employees->lastname,
                        'email' => $email,
                        'phone' => $employees->phone
                );
            
                $temat = 'Utworzenie pracownika w serwisie '.env('APP_NAME');
                $data = array('temat'=>$temat, 'email'=>$email, 'konto'=>$konto);
                 
            try {
                Mail::send('mails.create_employees', $data, function ($message) use ($temat, $email) {
                    $message->from(env('MAIL_USERNAME'), $temat);
                    $message->to($email)->subject($temat);
                });
            } catch(\Exception $e) {
                
            }

            Session::flash('success_message', 'Dodano pomyślnie.');
                  return redirect()->route('admin_employees_edit', ['id'=>$employees->id_employee])->with('services', $services);
              } else {
                  Session::flash('danger_message', 'Hasła do siebie nie pasują');
                  return redirect()->back();
              }
        } catch (\Exception $e) {
            dd($e);
            return view('admin.core.404');
        }
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $employees = Employee::where('id_employee', $id)->first();
            $companies = Company::get();
            return view('admin.employees.edit')->with('employees',$employees)->with('companies',$companies);
        } catch(\Exception $e) {
            return view('admin.core.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $employees = Employee::where('id_employee', $id)->update([
                'firstname' => $request->input('firstname'),
                'lastname' => $request->input('lastname'),
                'id_company' => $request->input('id_company'),
                // 'email' => $request->input('email'),
                'phone' => $request->input('phone'),
            ]);

            Session::flash('success_message', 'Zaktualizowano pomyślnie.');
            return redirect()->route('admin_employees_edit', ['id' => $id]);
        } catch(\Exception $e) {
            return view('admin.core.404');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          try {
            $employees = Employee::findOrFail($id)->delete();
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }

    public static function getEmployeeName($id_employee) {
        try {
            $getEmployee = Employee::findOrFail($id_employee)->first();
            return $getEmployee->firstname.' '.$getEmployee->lastname;
        } catch(\Exception $e) {
            dd($e);
        }
    }
    public static function getEmployeePhone($id_employee) {
        try {
            $getEmployee = Employee::findOrFail($id_employee)->first();
            return $getEmployee->phone;
        } catch(\Exception $e) {
            dd($e);
        }
    }
    public static function getCompanyByUser($id_user) {
        try {
            $getEmployee = Employee::where('id_user', $id_user)->first();

            $getCompany = Company::where('id_company', $getEmployee->id_company)->first();
            return $getCompany;
        } catch(\Exception $e) {
            dd($e);
        }
    }
  
}
