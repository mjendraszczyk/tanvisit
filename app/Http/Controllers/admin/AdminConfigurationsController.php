<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Configuration;
class AdminConfigurationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $configurations = Configuration::paginate($this->pagination_limit);
        return view('admin.configurations.index')->with('configurations',$configurations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.configurations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $configurations = new Configuration();
            $configurations->key = $request->input('key');
            $configurations->value = $request->input('value');
            $configurations->save();
            
            Session::flash('success_message', 'Zaktualizowano pomyślnie.');
            return redirect()->route('admin_configurations_edit',['id'=>$configurations->id_configuration]);

        } catch (\Exception $e) {
            // dd($e);
            return redirect()->route('admin_confiugrations_index');
        }
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         try {
            $configurations = Configuration::findOrFail($id)->first();
            return view('admin.configurations.edit')->with('configurations',$configurations);
        } catch(\Exception $e) {
            dd($e);
            return view('admin.core.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $configurations = Configuration::findOrFail($id)->update([
                'value' => $request->input('value')
            ]);
            Session::flash('success_message', 'Zaktualizowano pomyślnie.');
            return redirect()->route('admin_configurations_edit',['id'=>$id]);
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          try {
            $configurations = Configuration::findOrFail($id)->delete();
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }
}
