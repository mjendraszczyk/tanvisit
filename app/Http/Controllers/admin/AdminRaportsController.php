<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Employee;
use App\Visit;

class AdminRaportsController extends Controller
{
    public $customers;
    public $customersDetails;
    public $employees;
    public $employeesDetails;

    public function coreCustomers($data) {
        $this->customerDetails = array();
        if (isset($data['id_customer'])) {
            if($data['id_customer']) {
               // dd("1");
                $this->customers = Customer::where('id_customer', $data['id_customer'])->paginate($this->pagination_limit);
            } else {
              //  dd("2");
                $this->customers = Customer::paginate($this->pagination_limit);
            }
        } else {
            //dd("3");
            $this->customers = Customer::paginate($this->pagination_limit);
        }
         
        foreach ($this->customers as $customer) { 

            if (($data != null) && (isset($data['datetime_from'])) && (isset($data['datetime_to']))) {
                 //dd($data);
                $countVisits = Visit::where('id_customer', $customer->id_customer)->where('datetime_from', '>=', $data['datetime_from'])->where('datetime_from', '<=', $data['datetime_to'])->count();
                $incoming  = Visit::join('services_prices', 'services_prices.id_service', 'visits.id_service')->where('id_customer', $customer->id_customer)->where('datetime_from', '>=', $data['datetime_from'])->where('datetime_from', '<=', $data['datetime_to'])->sum('price');

                $acceptedVisits = Visit::where('id_customer', $customer->id_customer)->where('confirm', '1')->where('datetime_from', '>=', $data['datetime_from'])->where('datetime_from', '<=', $data['datetime_to'])->count();

                $canceledVisits = Visit::where('id_customer', $customer->id_customer)->where('datetime_from', '>=', $data['datetime_from'])->where('datetime_from', '<=', $data['datetime_to'])->where('confirm', '0')->count();

                $unconfirmationVisits = Visit::where('id_customer', $customer->id_customer)->where('confirm', '2')->where('datetime_from', '>=', $data['datetime_from'])->where('datetime_from', '<=', $data['datetime_to'])->count();

                $hoursVisit = Visit::where('id_customer', $customer->id_customer)->where('datetime_from', '>=', $data['datetime_from'])->where('datetime_from', '<=', $data['datetime_to'])->sum('hours')/10000;

            } else {
                $countVisits = Visit::where('id_customer', $customer->id_customer)->count();
                $incoming  = Visit::join('services_prices', 'services_prices.id_service', 'visits.id_service')->where('id_customer', $customer->id_customer)->sum('price');

                $acceptedVisits = Visit::where('id_customer', $customer->id_customer)->where('confirm', '1')->count();
                $canceledVisits = Visit::where('id_customer', $customer->id_customer)->where('confirm', '0')->count();
                $unconfirmationVisits = Visit::where('id_customer', $customer->id_customer)->where('confirm', '2')->count();

                $hoursVisit = Visit::where('id_customer', $customer->id_customer)->sum('hours')/10000;
            }
            $this->customerDetails['count'][$customer->id_customer] = $countVisits;

            $this->customerDetails['hours'][$customer->id_customer] = $hoursVisit;

            $this->customerDetails['accepted'][$customer->id_customer] = $acceptedVisits;
            $this->customerDetails['canceled'][$customer->id_customer] = $canceledVisits;
            $this->customerDetails['unconfirmation'][$customer->id_customer] = $unconfirmationVisits;

            $this->customerDetails['incoming'][$customer->id_customer] = $incoming;
            //dd($this->customerDetails);
        }
    }
    public function customers() {
       $this->coreCustomers(null);
        return view('admin.raports.customers')->with('customers', $this->customers)->with('customerDetails', $this->customerDetails)->with('datetime_from', null)->with('datetime_to', null);
    }

    public function coreEmployees($data) {
        $this->employeesDetails = array();
        if ($data != null && isset($data['id_employee'])) {
            if($data['id_employee']) {
                $this->employees = Employee::where('id_employee', $data['id_employee'])->paginate($this->pagination_limit);
            } else {
                $this->employees = Employee::paginate($this->pagination_limit);
            }
        } else {
            $this->employees = Employee::paginate($this->pagination_limit);
        }
        foreach ($this->employees as $employee) { 

            if (($data != null) && (isset($data['datetime_from'])) && (isset($data['datetime_to']))){
                $countVisits = Visit::where('id_employee', $employee->id_employee)->where('datetime_from', '>=', $data['datetime_from'])->where('datetime_from', '<=', $data['datetime_to'])->count();
                $incoming  = Visit::join('services_prices', 'services_prices.id_service', 'visits.id_service')->where('id_employee', $employee->id_employee)->where('datetime_from', '>=', $data['datetime_from'])->where('datetime_from', '<=', $data['datetime_to'])->sum('price');

                $hours  = Visit::join('services_prices', 'services_prices.id_service', 'visits.id_service')->where('id_employee', $employee->id_employee)->where('datetime_from', '>=', $data['datetime_from'])->where('datetime_from', '<=', $data['datetime_to'])->sum('time');
            } else { 
                $countVisits = Visit::where('id_employee', $employee->id_employee)->count();
                $incoming  = Visit::join('services_prices', 'services_prices.id_service', 'visits.id_service')->where('id_employee', $employee->id_employee)->sum('price');

                $hours  = Visit::join('services_prices', 'services_prices.id_service', 'visits.id_service')->where('id_employee', $employee->id_employee)->sum('time');
            }
                $this->employeesDetails['hours'][$employee->id_employee] = $hours;
                $this->employeesDetails['count'][$employee->id_employee] = $countVisits;
                $this->employeesDetails['incoming'][$employee->id_employee] = $incoming;
            

                 $this->employeesDetails['accepted'][$employee->id_employee] = '';
            $this->employeesDetails['canceled'][$employee->id_employee] = '';
            $this->employeesDetails['unconfirmation'][$employee->id_employee] = '';
        }
      
    }
    public function employees() {
        $this->coreEmployees(null);
       
        return view('admin.raports.employees')->with('employees', $this->employees)->with('employeesDetails', $this->employeesDetails)->with('datetime_from', null)->with('datetime_to', null);
    }

    public function customers_export() {
       
        header('Content-Type: text/csv');
        header('Content-Type: application/force-download; charset=UTF-8');
        header('Cache-Control: no-store, no-cache');
        header('Content-Disposition: attachment; filename="customers.csv"');


        $this->coreCustomers(null);
 
 $list = array();
        $list[] = array('firstname','lastname','visits','accepted visits', 'unconfirmation visits','canceled visits','sales');
 
       
        $fp = fopen("php://output", 'w');//fopen('customers.csv', 'w');
        foreach ($this->customers as $customer) {
           // fputcsv($fp, $fields);
            $list[] = array (
    $customer->firstname, $customer->lastname, $this->customerDetails['count'][$customer->id_customer], $this->customerDetails['accepted'][$customer->id_customer], $this->customerDetails['canceled'][$customer->id_customer], $this->customerDetails['unconfirmation'][$customer->id_customer], $this->customerDetails['incoming'][$customer->id_customer] 
    );
        }

         foreach ($list as $fields) {
            fputcsv($fp, $fields);
        }
    }
    public function employees_export() {
        header('Content-Type: text/csv');
        header('Content-Type: application/force-download; charset=UTF-8');
        header('Cache-Control: no-store, no-cache');
        header('Content-Disposition: attachment; filename="employees.csv"');

        $this->coreEmployees(null);
        $list = array();
        $list[] = array('firstname','lastname','visits','hours','sales');
 
       
        $fp = fopen("php://output", 'w');//fopen('customers.csv', 'w');
        foreach ($this->employees as $employee) {
           // fputcsv($fp, $fields);
            $list[] = array (
            $employee->firstname, $employee->lastname, $this->employeesDetails['count'][$employee->id_employee], date('H', strtotime($this->employeesDetails['hours'][$employee->id_employee])), $this->employeesDetails['incoming'][$employee->id_employee] 
            );
            }

            foreach ($list as $fields) {
                fputcsv($fp, $fields);
            }
    }
      public function employees_filter(Request $request) {
          $data = array();
          $data['datetime_from'] = $request->input('date_from');
          $data['datetime_to'] = $request->input('date_to');
          
        $this->coreEmployees($data);

        return view('admin.raports.employees')->with('employees', $this->employees)->with('employeesDetails', $this->employeesDetails)->with('datetime_from', $data['datetime_from'])->with('datetime_to', $data['datetime_to']);
    }
    public function customers_filter(Request $request) {
        $data = array();
        $data['datetime_from'] = $request->input('datetime_from');
        $data['datetime_to'] = $request->input('datetime_to');
          
        $this->coreCustomers($data);

        return view('admin.raports.customers')->with('customers', $this->customers)->with('customerDetails', $this->customerDetails)->with('datetime_from', $data['datetime_from'])->with('datetime_to', $data['datetime_to']);
    }

    public function customer_detail($id_customer) {
        $data = array();
        $data['id_customer'] = $id_customer;

        $this->coreCustomers($data);

        return view('admin.raports.detail')->with('customers', $this->customers)->with('customerDetails', $this->customerDetails)->with('id', $id_customer)->with('datetime_from','')->with('datetime_to','');
    }
     public function employee_detail($id_employee) {
        $data = array();
        $data['id_employee'] = $id_employee;
        $this->coreEmployees($data);


        return view('admin.raports.detail')->with('customers', $this->employees)->with('customerDetails', $this->employeesDetails)->with('id', $id_employee)->with('datetime_from','')->with('datetime_to','');
    }
     public function employees_detail_filter(Request $request, $id_employee) {
        $data = array();

        $data['datetime_from'] = $request->input('datetime_from');
        $data['datetime_to'] = $request->input('datetime_to');
        $data['id_employee'] = $id_employee;
        // dd($data);
        $this->coreEmployees($data);


        return view('admin.raports.detail')->with('customers', $this->employees)->with('customerDetails', $this->employeesDetails)->with('id', $id_employee)->with('datetime_from','')->with('datetime_to','');
    }
    public function customers_detail_filter(Request $request, $id_customer) {
        $data = array();

        $data['datetime_from'] = $request->input('datetime_from');
        $data['datetime_to'] = $request->input('datetime_to');
        $data['id_customer'] = $id_customer;
        $this->coreCustomers($data);
        //dd($this->customerDetails);

        return view('admin.raports.detail')->with('customers', $this->customers)->with('customerDetails', $this->customerDetails)->with('id', $id_customer)->with('datetime_from',$data['datetime_from'])->with('datetime_to',$data['datetime_to']);
    }
}
