<?php

namespace App\Http\Controllers\admin;

use Jenssegers\Agent\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Callendar;
use App\Service;
use App\Company;
use App\Employee;
use App\User;
use App\Visit;

use Session;
use Auth;

class AdminCallendarsController extends Controller
{


    public function core($resource) { 
        $agent = new Agent();
         
           $getEmployee = Employee::where('id_user', Auth::user()->id)->first();

           $First_date = date_create('this week')->format('Y-m-d H:i:s');
$Last_date = date_create('this week +6 days')->format('Y-m-d H:i:s');
// echo $First_date;
// echo $Last_date;

        $getWizyty = Visit::where('datetime_from', '>=', $First_date)->where('datetime_to', '<=', $Last_date)->where('valid', 1)->count();
        $getPrzychod = Visit::join('services_prices', 'services_prices.id_service', 'visits.id_service')->where('datetime_from', '>=', $First_date)->where('datetime_to', '<=', $Last_date)->where('valid', 1)->sum('price');
        $getIloscGodzin = Visit::where('datetime_from', '>=', $First_date)->where('datetime_to', '<=', $Last_date)->where('valid', 1)->get();
        $getGodziny = 0;
        foreach($getIloscGodzin as $godzin) {
            $getGodziny += date('H', strtotime($godzin->hours));
        }

         $id_employee = 0;
            $id_service = 0;
            $id_company = 0;

        if($resource != null) {

            $id_employee = $resource['id_employee'];
            $id_service = $resource['id_service'];
            $id_company = $resource['id_company'];

            if($resource['id_employee'] != '0') {
                if ($resource['id_service'] != '0') {
                    if($resource['id_company'] != '0') {

                        //$getEmployees = Employee::where('id_company', $request->input('id_company'))->get();

                        // $employees_id = array();
                        // foreach($getEmployees as $employee) {
                        //     $employees_id[] = $employee->id_employee;
                        // }
                        // $getVisits = Visit::whereIn('id_employee', $employees_id)->where('id_service', $resource['id_service'])->get();
                         $getVisits = Visit::where('id_employee', $resource['id_employee'])->where('id_service', $resource['id_service'])->where('valid', 1)->get();
                    } else {
                        $getVisits = Visit::where('id_employee', $resource['id_employee'])->where('id_service', $resource['id_service'])->where('valid', 1)->get();
                    }
                } else {
                    $getVisits = Visit::where('id_employee', $resource['id_employee'])->where('valid', 1)->get();
                }
            }
            else if($resource['id_service'] != '0') {
                $getVisits = Visit::where('id_service', $resource['id_service'])->where('valid', 1)->get(); 
            } else {
                $getVisits = Visit::where('valid', 1)->get();
            }
        } else {
            $getVisits = Visit::where('valid', 1)->get();
        }
$events = [];
foreach($getVisits as $visit) {
    if($visit->confirm == 1) {
        $state = '✓';
    } else {
        $state = '✗';
    }
       $events[] = \Calendar::event(
    AdminServicesController::getServiceName($visit->id_service)."\n tel:".AdminCustomersController::getCustomerPhone($visit->id_customer)."\n ".$state, //event title
    false, //full day event?
    $visit->datetime_from, //start time, must be a DateTime object or valid DateTime format (http://bit.ly/1z7QWbg)
    $visit->datetime_to, //end time, must be a DateTime object or valid DateTime format (http://bit.ly/1z7QWbg),
	$visit->id_visit, //optional event ID
	[
		'url' => route('admin_visits_edit',['id_visit'=>$visit->id_visit]),
        'color' => ''.$visit->color,
	]
);
}
     
// if($agent->isMobile()) {
//     $defaultView = 'agendaDay';
// } else {
    $defaultView = 'agendaWeek';
//}


          $calendar = \Calendar::addEvents($events) //add an array with addEvents
    ->setOptions([ //set fullcalendar options
        'firstDay' => 1,
         'buttonIcons' => false, // show the prev/next text
    'weekNumbers' => true,
    'navLinks' => true,
        'locale' => 'pl',
        'scrollTime' => '08:00',
        'defaultView' => $defaultView,//agendaWeek
        'height' => 'auto',
        'editable' => true,
        'eventLimit' => true,

    ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
        'eventDrop' => 'function(event, delta, revertFunc) {
  //  alert(event.title + " was dropped on " + event.start.format());
            //console.log(event);
  
             $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content")
                    }
                });

               
             $.ajax({
                url: "/backoffice/api/visits/"+event.id+"/update",
                method: "POST",
                data : {datetime_from:event.start.format()},
                success: function(response) {
                    console.log(response);
                }, 
                error: function(xhr) {
                    console.log(xhr);
                }
            }); 
             
    }',
        'viewRender' => 'function() {
            var currentWidth = $(window).width();
            if(currentWidth < 500) {
                //alert($(window).width());
                //alert($("#calendar").fullCalendar("getView"));
                //console.log($("#calendar").fullCalendar("getView").title);
                //$("#calendar").fullCalendar("changeView", "agendaDay");
                
                //this.setOptions("changeView", "agendaDay");
            }
        }',
    ]);

    $services = Service::join('services_prices', 'services_prices.id_service', 'services.id_service')->where('id_user', \App\Http\Controllers\admin\AdminEmployeesController::getCompanyByUser(Auth::user()->id)->id_company)->get();
    $employees = Employee::join('users','users.id','employees.id_user')->where('id_company', \App\Http\Controllers\admin\AdminEmployeesController::getCompanyByUser(Auth::user()->id)->id_company)->get();
    $companies = Company::get();
    if (($resource != null)) {
        if (($resource['method'] == 'api')) {
            return view('admin.callendars.api')->with('calendar', $calendar)->with('services', $services)->with('employees', $employees)->with('companies', $companies)->with('przychod', $getPrzychod)->with('wizyty', $getWizyty)->with('godziny', $getGodziny)->with('id_employee', $id_employee)->with('id_service', $id_service)->with('id_company', $id_company);
        } else {
             return view('admin.callendars.index')->with('calendar', $calendar)->with('services', $services)->with('employees', $employees)->with('companies', $companies)->with('przychod', $getPrzychod)->with('wizyty', $getWizyty)->with('godziny', $getGodziny)->with('id_employee', $id_employee)->with('id_service', $id_service)->with('id_company', $id_company);
        }
    } else {
        return view('admin.callendars.index')->with('calendar', $calendar)->with('services', $services)->with('employees', $employees)->with('companies', $companies)->with('przychod', $getPrzychod)->with('wizyty', $getWizyty)->with('godziny', $getGodziny)->with('id_employee', $id_employee)->with('id_service', $id_service)->with('id_company', $id_company);
    }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
return $this->core(null);
    }

    public function getVisitsByFilter(Request $request) {
          $resource = array();
        $resource['id_employee'] = $request->input('id_employee');
        $resource['id_service'] = $request->input('id_service');
        $resource['id_company'] = null;
        $resource['method'] = 'api';
        //return "AA".$resource['id_employee'].' + '.$resource['id_service'];
        return $this->core($resource);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.callendars.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $calendar = Callendar::findOrFail($id)->first();
            return view('admin.callendars.create')->with('items', $calendar);
        } catch(\Exception $e) {
            return view('admin.core.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function filter(Request $request) {
        $resource = array();
        $resource['id_employee'] = $request->input('id_employee');
        $resource['id_service'] = $request->input('id_service');
        $resource['id_company'] = $request->input('id_company');
         $resource['method'] = '';
        return $this->core($resource);

    }
}

