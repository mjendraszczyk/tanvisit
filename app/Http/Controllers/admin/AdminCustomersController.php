<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests\AdminCustomersRequest;
use App\Http\Controllers\Controller;
use App\Customer;
use Session;
use Illuminate\Support\Facades\DB;

class AdminCustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::paginate($this->pagination_limit);
        return view('admin.customers.index')->with('customers',$customers);
    }

    public static function getCustomerPhone($id_customer) {
        try {
            return Customer::where('id_customer', $id_customer)->first()->phone;
        } catch (\Exception $e) {
            return '';
        }
    }
    public static function getCustomerName($id_customer) {
        try {
            $getCustomer = Customer::where('id_customer', $id_customer)->first();
            return $getCustomer->firstname.' '.$getCustomer->lastname;
        } catch (\Exception $e) {
            dd($e);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.customers.create')->with('prefix_phone', $this->phone_prefix);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminCustomersRequest $request)
    {
          try {
            $customers = new Customer(); 
            $customers->firstname = $request->input('firstname');
            $customers->lastname = $request->input('lastname');
            $customers->email = $request->input('email');
            $customers->phone = $request->input('prefix_phone').$request->input('phone');
            $customers->private_note = $request->input('private_note');
            $customers->prefix_phone = $request->input('prefix_phone');
            // $customers->id_company = Controller::getCompanyId(Auth::user()->id);
            $customers->save();

            return redirect()->route('admin_customers_edit',['id'=>$customers->id_customer]);
        } catch (\Exception $e) {
            Session::flash('danger_message', 'Nieprawidłowe dane lub numer telefonu juz istnieje');
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $customers = Customer::where('id_customer', $id)->first();
            return view('admin.customers.edit')->with('customers',$customers)->with('prefix_phone', $this->phone_prefix);
        } catch(\Exception $e) {
            return view('admin.core.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $getCustomer = Customer::where('id_customer', $id)->first();
            if(!empty($getCustomer->prefix_phone)) { 
                $output_phone = substr($request->input('phone'), strlen($getCustomer->prefix_phone));
            } else {
                $output_phone = $request->input('phone');
            }
            $customers = Customer::where('id_customer',$id)->update([
                'firstname' => $request->input('firstname'),
                'lastname' => $request->input('lastname'),
                'email' => $request->input('email'),
                'phone' => $request->input('prefix_phone').$output_phone,
                'private_note' => $request->input('private_note'),
                'prefix_phone' => $request->input('prefix_phone'),
            ]);
            Session::flash('success_message', 'Zaktualizowano pomyślnie.');
            return redirect()->route('admin_customers_edit',['id'=>$id]);
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          try {
            $customers = Customer::findOrFail($id)->delete();
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }
    public function getPhones() {
        //return Customer::all()->pluck('phone');
        $getCustomer = Customer::get();
        $response = array();
        foreach($getCustomer as $item) {
            $response[] = (string)$item['phone'];
        }
        return $response;
        //return Customer::get(['phone']);
    }
    public function getNameByPhone($phone) {
        $getCustomer = Customer::where('phone', $phone)->get();
        $response = array();
        foreach($getCustomer as $item) {
            $response[] = (string)$item['firstname'];
        }
        return $response;
        //return Customer::get(['phone']);
    }
    public function export() {

        header('Content-Type: text/csv');
        header('Content-Type: application/force-download; charset=UTF-8');
        header('Cache-Control: no-store, no-cache');
        header('Content-Disposition: attachment; filename="customers.csv"');


        $customers = Customer::get();
 $list = array();
        $list[] = array('firstname','lastname','phone','email');
 
       
        $fp = fopen("php://output", 'w');//fopen('customers.csv', 'w');
        foreach ($customers as $customer) {
           // fputcsv($fp, $fields);
            $list[] = array (
    $customer->firstname, $customer->lastname, $customer->phone, $customer->email
);
        }

         foreach ($list as $fields) {
            fputcsv($fp, $fields);
        }

       // fclose($fp);
    }
}
