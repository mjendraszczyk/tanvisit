<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests\AdminServicesRequest;
use App\Http\Controllers\Controller;
use App\Service;
use App\ServiceCategory;
use Session;
class AdminServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::paginate(25);
        return view('admin.services.index')->with('services',$services);
    }
public static function getServiceName($id_service) {
    try {
        $getService = Service::where('id_service', $id_service)->first();
        return $getService->name;
    } catch(\Exception $e) {
        //dd($e);
        return '--';
    }
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services_category = ServiceCategory::get();
        return view('admin.services.create')->with('services_category',$services_category);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminServicesRequest $request)
    {
        try {
            $services = new Service(); 
            $services->id_service_category = $request->input('id_service_category');
            $services->name = $request->input('name');
            $services->save();

            return redirect()->route('admin_services_edit',['id'=>$services->id_service]);
        } catch (\Exception $e) {
            return view('admin.core.404');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          try {
              $services_category = ServiceCategory::get();
            $services = Service::where('id_service', $id)->first();

            return view('admin.services.edit')->with('services',$services)->with('services_category',$services_category);
        } catch (\Exception $e) {
            return view('admin.core.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $services = Service::findOrFail($id)->update([
                'id_service_category' => $request->input('id_service_category'),
                'name' => $request->input('name')
            ]);
            Session::flash('success_message', 'Zaktualizowano pomyślnie.');
            return redirect()->route('admin_services_edit',['id'=>$id]);
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $services = Service::findOrFail($id)->delete();
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }
}
