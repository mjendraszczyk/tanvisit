<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Opening;

class AdminOpeningsController extends Controller
{
    public function edit($id) {

        
        $openings = Opening::where('id_company', $id)->get();
        $countOpenings = Opening::where('id_company', $id)->count();
        return view('admin.openings.edit')->with('id', $id)->with('openings', $openings)->with('days', $this->days)->with('countOpenings', $countOpenings);
    }
    public function update(Request $request, $id) { 
        // dd("DD");

        $checkOpening = Opening::where('id_company', $id)->count();

        if($checkOpening == 0) { 
             for ($i=0;$i<7;$i++) {
                $opening = new Opening();
                $opening->day = $i; 
                $opening->id_company = $id; 
                $opening->from = $request->input('from_'.$i);
                $opening->to = $request->input('to_'.$i);
                $opening->save();
            
            }
        } else {
            for ($i=0;$i<7;$i++) {
                $opening = Opening::where('id_company', $id)->where('day', $i)->update([
                'from' => $request->input('from_'.$i),
                'to' => $request->input('to_'.$i),
            ]);
            }
        }
        return redirect()->route('admin_openings_edit',['id'=>$id]);
    }
}
