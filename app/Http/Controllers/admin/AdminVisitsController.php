<?php

declare(strict_types=1);

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;
use App\Customer;
use App\Visit;
use App\User;
use App\Employee;
use App\Http\Requests\AdminVisitsRequest;
use Session;
use ShortenerManager;

use Auth;

use Smsapi\Client\Curl\SmsapiHttpClient;
use Smsapi\Client\Service\SmsapiComService;
use Smsapi\Client\SmsapiClient;
use Smsapi\Client\Feature\Sms\Bag\SendSmsBag;

/**
 * @var SmsapiClient $client
 */
//require_once dirname(__FILE__).'/client.php';

class AdminVisitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filtr(Request $request)
    {
         $data = array();
          $data['datetime_from'] = $request->input('datetime_from');
          $data['datetime_to'] = $request->input('datetime_to');
          
        $visits = Visit::where('id_callendar', \App\Http\Controllers\admin\AdminEmployeesController::getCompanyByUser(Auth::user()->id)->id_callendar)->where('valid',1)->orderBy('id_visit','desc')->whereBetween('datetime_from',[$data['datetime_from'], $data['datetime_to']])->paginate(25);

        return view('admin.visits.history')->with('visits', $visits)->with('datetime_from', $data['datetime_from'])->with('datetime_to', $data['datetime_to']);
    }

    public function history() {
        $visits = Visit::where('id_callendar', \App\Http\Controllers\admin\AdminEmployeesController::getCompanyByUser(Auth::user()->id)->id_callendar)->where('valid',1)->orderBy('id_visit','desc')->paginate(25);
        return view('admin.visits.history')->with('visits', $visits)->with('datetime_from', '')->with('datetime_to', '');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = Service::get();
           $users = User::get();
           $employees = Employee::get();
        return view('admin.visits.create')->with('users',$users)->with('services',$services)->with('employees',$employees)->with('prefix_phone', $this->phone_prefix);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminVisitsRequest $request)
    {
          try {
            date_default_timezone_set('Europe/Warsaw');

            $getCustomer = Customer::where('phone',$request->input('phone'))->count();

            if($getCustomer > 0) {
                if(!empty($request->input('firstname'))) {
                    Customer::where('phone', $request->input('phone'))->update([
                        'firstname' => $request->input('firstname')
                    ]);
                    $customer = Customer::where('phone', $request->input('phone'))->first();
                } else {
                    $customer = Customer::where('phone', $request->input('phone'))->first();
                }
            } else {
                $customer = new Customer();
                $customer->firstname = $request->input('firstname');
                $customer->phone = $request->input('phone');
                $customer->prefix_phone = $request->input('prefix_phone');
                //$customers->id_company = Controller::getCompanyId(Auth::user()->id);
                $customer->save();
            }
            
             if($request->input('confirm') == 0) {
                    $kolor = 'red';
                 } else if($request->input('confirm') == 2) {
                    $kolor = 'grey';
                 } else {
                    $kolor = 'green';
                 }

            $visit = new Visit(); 
            $visit->id_customer = $customer->id_customer;
            $visit->datetime_from = $request->input('datetime_from');
            $visit->datetime_to = $request->input('datetime_from');
            $visit->id_service = $request->input('id_service');
            $visit->color = $kolor;
            $visit->hours = date('H:i:s', strtotime($request->input('hours')));
            $visit->id_callendar = Controller::getCompanyId(Auth::user()->id);
            $visit->id_employee = $request->input('id_employee');
            $visit->confirm = $request->input('confirm');
            $visit->private_note = $request->input('private_note');
            $visit->save();

            $employee = Employee::findOrFail($request->input('id_employee'))->first();


 /**
  * Wyslij email
  */

  Controller::sendEmail($visit, $customer, 'create', 'customer');
  Controller::sendEmail($visit, $employee, 'create', 'employee');

            /**
             * Wyslij sms
             */

             Controller::sendSms($visit, $customer, 'create', 'customer');
             //Controller::sendSms($visit, $employee, 'create', 'employee');
            
            return redirect()->route('admin_callendars_index');
        } catch (\Exception $e) {
            //dd($e);
            return false;
            //return view('admin.core.404');
        }
    }

    public static function getVisitName($id_visist) { 
 try {
$getVisit = Visit::findOrFail($id_visist)->first();
return $getVisit->name;
 } catch (\Exception $e) {
return false;
 }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
               try {
                   $employees = Employee::get();
                     $services = Service::get();
              $visits = Visit::findOrFail($id);

            return view('admin.visits.edit')->with('visits',$visits)->with('services',$services)->with('employees',$employees)->with('prefix_phone', $this->phone_prefix);
        } catch (\Exception $e) {
            return view('admin.core.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminVisitsRequest $request, $id)
    {
        try {
            date_default_timezone_set('Europe/Warsaw');
             if($request->input('confirm') == 0) {
                    $kolor = 'red';
                 } else if($request->input('confirm') == 2) {
                    $kolor = 'grey';
                 } else {
                    $kolor = 'green';
                 }
            $getVisit = Visit::where('id_visit', $id)->update([
                'datetime_from' => $request->input('datetime_from'),
                'datetime_to' => $request->input('datetime_to'),
                'id_service' => $request->input('id_service'),
                'color' => $kolor,
                'id_employee' => $request->input('id_employee'),
                'hours' => date('H:i:s', strtotime($request->input('hours'))),
                'confirm' => $request->input('confirm'),
                'private_note' => $request->input('private_note'),
            ]);

            if(!empty($request->input('firstname'))) {
                    Customer::where('phone', $request->input('phone'))->update([
                        'firstname' => $request->input('firstname')
                    ]);
                }
            if($request->input('update_visit') == '1') {
                $employee = Employee::where('id_employee', $request->input('id_employee'))->first();
                $customer = Customer::where('phone', $request->input('phone'))->first();

                $visit = Visit::where('id_visit', $id)->first();
                /**
                * Wyslij email
                */
                Controller::sendEmail($visit, $customer, 'update', 'customer');
                // echo "TT";
                // exit();
                Controller::sendEmail($visit, $employee, 'update', 'employee');
            /**
             * Wyslij sms
             */
             Controller::sendSms($visit, $customer, 'update', 'customer');
            }

            return redirect()->route('admin_callendars_index');
        } catch(\Exception $e) {
            return redirect()->route('admin_visits_edit',['id'=>$id]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $visits = Visit::findOrFail($id)->delete();
            return redirect()->route('admin_callendars_index');
        } catch (\Exception $e) {
            return redirect()->route('admin_callendars_index');
        }
    }
    public function apiVisits(Request $request, $id) { 
        try {
            $getVisit = Visit::where('id_visit', $id)->first();

            $date_from = $request->input('datetime_from');
            $addHour = date('H', strtotime($getVisit->hours));
            $addMinute = date('i', strtotime($getVisit->hours));
            $dateTo = date('Y-m-d H:i:s', strtotime($date_from.' +'.$addHour.' hour + '.$addMinute.' minutes'));

//echo $dateTo;
            //$dateTo = date($request->input('datetime_from'), strtotime('+'.$adHour.' hour'));
//date('H', $getVisit->hours)
            
            $updateVisit = Visit::findOrFail($id)->update([
            'datetime_from' => $request->input('datetime_from'),
            'datetime_to' => $dateTo,
        ]);
        //return true;
        } catch(\Exception $e) {
            return false;
        }

    }
    public static function getVisitStatus($confirm) {
        $status = array(
        "1" => "Potwierdzona",
        "2" => "Niepotwierdzona",
        "0" => "Anulowana",
        );
        return $status[$confirm];
    }
}
