<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gallery;
use Auth;
use Session;
use Illuminate\Support\Facades\Input;
use Image;

class AdminGalleriesController extends Controller
{
   public function index() {

       $galleries = Gallery::where('id_company',Controller::getCompanyId(Auth::user()->id))->paginate($this->pagination_limit);
       return view('admin.galleries.index')->with('galleries', $galleries);
   }

    public function cropUpload(Request $request) {
        $folderPath = public_path('front/img/uploads/galleries/');


        $image_parts = explode(";base64,", $request->image);

        $image_type_aux = explode("image/", $image_parts[0]);

        $image_type = $image_type_aux[1];

        $image_base64 = base64_decode($image_parts[1]);

        $name = 'croped'.uniqid() . '.png';
        $file = $folderPath . $name;


        file_put_contents($file, $image_base64);


        return response()->json(['success'=>'success','path' => '/front/img/uploads/galleries/'.$name, 'name' => $name]);

    }

   public function create() {
       return view('admin.galleries.create');
   }
   public function store(Request $request) {
        $galleries = new Gallery();
            
        $getGallery = Gallery::where('id_company', Controller::getCompanyId(Auth::user()->id))->count();

        $galleries->label = $request->input('label');
        
        $galleries->position = $getGallery+1;
        $galleries->feature = $request->input('feature');
        $galleries->id_company = Controller::getCompanyId(Auth::user()->id);
                  
        // Avatar usera
         $upload = new Controller();
        if ((Input::file('filename'))) {
            $upload->upload('filename', 'jpg,jpeg,png', '/front/img/uploads/galleries', $request->get('filename'), $request, 800, 800);
    
            // $ekspert = Ekspert::where('id_ekspert', Session::get('form_ekspert_id'))->first();
            if(!empty($request->input('croped'))) { 
                $galleries->filename = $request->input('croped');
            } else {
                $galleries->filename = $upload->getFileName();
            }
        }
        $galleries->feature = 0;
        $galleries->save();

        Session::flash('success_message', 'Dodano pomyślnie.');
        return redirect()->route('admin_galleries_edit', ['id'=>$galleries->id_gallery]);
   }
   public function update(Request $request, $id) {

    if(!empty($request->input('feature'))) {
        $feature = 1;
    } else {
        $feature = 0;
    }
        $galleries = Gallery::where('id_gallery', $id)->update([
            'label' => $request->input('label'),
            'position' => $request->input('position'),
            'feature' => $feature,
        ]);
                    
        Session::flash('success_message', 'Zaktualizowano pomyślnie.');
     return redirect()->route('admin_galleries_edit', ['id'=>$id]);

   }

   public function edit($id) {
       try {
           $getGallery = Gallery::where('id_gallery', $id)->first();

           return view('admin.galleries.edit')->with('galleries', $getGallery);
       } catch (\Exception $e) {
            return false;
       }
   }

   public function destroy($id) {
         try {
            $galleries = Gallery::where('id_gallery', $id)->delete();
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
   }
}
