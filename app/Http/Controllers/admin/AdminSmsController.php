<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Session;
use App\Sms;
use Auth;

class AdminSmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sms = Sms::paginate($this->pagination_limit);
        return view('admin.sms.index')->with('sms',$sms)->with('types',$this->types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sms.create')->with('types',$this->types);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         try {
            
            $sms = new Sms(); 
            $sms->id_type = $request->input('id_type');
            $sms->content = $request->input('content');
            $sms->id_company = Controller::getCompanyId(Auth::user()->id);
            $sms->save();
            // dd("DD");
            // dd($request);
            return redirect()->route('admin_sms_edit',['id'=>$sms->id_sms]);
        } catch (\Exception $e) {
          //  dd($e);
          return false;
            //return view('admin.core.404');
        }
    }

 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          try {
            $sms = Sms::where('id_sms', $id)->first();

            return view('admin.sms.edit')->with('sms',$sms)->with('types',$this->types);
        } catch (\Exception $e) {
            return view('admin.core.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $sms = Sms::where('id_sms',$id)->update([
                'content' => $request->input('content'),
                'id_type' => $request->input('id_type')
            ]);
            Session::flash('success_message', 'Zaktualizowano pomyślnie.');
            return redirect()->route('admin_sms_edit',['id'=>$id]);
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $sms = Sms::where('id_sms',$id)->delete();
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }
}
