<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Session;
use App\Company;
use App\Customer;
use App\Country;
use App\Visit;
use App\Callendar;
use App\Employee;

class AdminCompaniesController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::paginate($this->pagination_limit);
        return view('admin.companies.index')->with('companies',$companies);
    }

    public static function getCompanyName($id_company) {
        try {
            return Company::where('id_company', $id_company)->first()->name;
        } catch (\Exception $e) {
            return false;
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::get();
        return view('admin.companies.create')->with('countries', $countries);
    }

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $companies = new Company();
            $companies->name = $request->input('name');
            $companies->vat_number = $request->input('vat_number');
            $companies->email = $request->input('email');
            $companies->address = $request->input('address');
            $companies->postalcode = $request->input('postalcode');
            $companies->city = $request->input('city');
            $companies->phone = $request->input('phone');
            $companies->id_country = $request->input('id_country');

            $companies->description = $request->input('description');
            $companies->instagram = $request->input('instagram');
            $companies->facebook = $request->input('facebook');
            $companies->lat = $request->input('lat');
            $companies->lon = $request->input('lon');
            $companies->certificate = $request->input('certyficate');
            $companies->store = $request->input('store');
            $companies->save();

            Session::flash('success_message', 'Dodano pomyślnie.');
            return redirect()->route('admin_companies_edit',['id'=>$companies->id_company]);
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $companies = Company::where('id_company', $id)->first();
         $countries = Country::get();
        return view('admin.companies.edit')->with('companies', $companies)->with('countries', $countries);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
             try {
                $companies = Company::where('id_company', $id)->update([
                'name' => $request->input('name'),
                'vat_number' => $request->input('vat_number'),
                'address' => $request->input('address'),
                'postalcode' => $request->input('postalcode'),
                'city' => $request->input('city'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'id_country' => $request->input('id_country'),
                'description' => $request->input('description'),
                'instagram' => $request->input('instagram'),
                'facebook' => $request->input('facebook'),
                'lat' => $request->input('lat'),
                'lon' => $request->input('lon'),
                'certificate' => $request->input('certificate'),
                'store' => $request->input('store'),
            ]);
               
            Session::flash('success_message', 'Zaktualizowano pomyślnie.');
            return redirect()->route('admin_companies_edit',['id'=>$id]);
        } catch (\Exception $e) {
            //dd($e);
            return false;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          try {
              $getCompany = Company::where('id_company', $id)->first();
            // delete visits
            Visit::where('id_callendar', $getCompany->id_callendar)->delete();
            
            // delete customers
            //Customer::where('',)->delete();

            // delete callendars
            Callendar::where('id_company',$getCompany->id_callendar)->delete();

            // delete employees
            Employee::where('id_company',$id)->delete();

            // delete company
            Company::where('id_company',$id)->delete();

            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }
}
