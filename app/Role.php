<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $primaryKey = 'id_role';
    public $incrementing = true;
    protected $table = 'roles';
     protected $fillable = [
        'name',
    ];
}
