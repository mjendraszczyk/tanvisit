<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Callendar extends Model
{
    protected $primaryKey = 'id_callendar';
    public $incrementing = true;
    protected $table = 'callendars';
     protected $fillable = [
        'id_user'
    ];
}
