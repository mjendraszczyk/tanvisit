<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    protected $primaryKey = 'id_visit';
    public $incrementing = true;
    protected $table = 'visits';
     protected $fillable = [
        'id_customer',
        'id_callendar',
        'datetime_from',
        'datetime_to',
        'confirm',
        'id_service',
        'color',
        'hours',
        'id_employee',
        'mail',
        'private_note',
        'valid'
    ];
}
