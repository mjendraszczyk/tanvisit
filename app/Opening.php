<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opening extends Model
{
    protected $primaryKey = 'id_opening';
    public $incrementing = true;
    protected $table = 'openings';
     protected $fillable = [
        'id_company',
        'day',
        'from',
        'to',
    ];
}
