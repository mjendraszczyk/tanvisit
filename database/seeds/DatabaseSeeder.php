<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\ServiceCategory;
use App\Service;
use App\ServicePrice;
use App\Company;
use App\Employee;
use App\Customer;
use App\Country;
use App\Subscription;
use App\Visit;
use App\Configuration;
use App\Callendar;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // $this->call(UsersTableSeeder::class);
        /**
         * Dodanie konfiguracji
         */
        $konfiguracje = array(
            ['sms_szablon' => ''],
            ['sms_klucz_api' => 'IO6jW9yXiYxPFM978pvvHa10anOur6ZfCWHnCDEV'],
            ['payu_idpunktu' => '1722712'],
            ['payu_drugimd5' => '52a80b6f685f8a1420e3d90dcc25aa53'],
            ['payu_oauth_clientid' => '1722712'],
            ['payu_oauth_clientsecret' => 'f1917f589c9e1fb67804d2ba86b12cab'],
        );
         foreach($konfiguracje as $key => $konf) {
            $konfiguracja = new Configuration();
            $konfiguracja->key = key($konfiguracje[$key]);
            $konfiguracja->value = '';//value($konfiguracje[$key]);
            $konfiguracja->save();
         }

        $country = new Country();
        $country->name = 'Polska';
        $country->save();
        /**
         * Dodawanie ról
         */
        $role_tablica = ['Pracownik','Właściciel','Administrator'];
        foreach($role_tablica as $key => $rola) {
            $role = new Role();
            $role->id_role = ($key+1);
            $role->name = $rola;
            $role->save();
        }
         /**
          * Dodanie roota
          */
        $root = new User();
        $root->name = 'mages';
        $root->email = 'biuro@mages.pl';
        $root->password = bcrypt('1');
        $root->id_role = '3';
        $root->save();


        /**
         * Dodanie uzytkownikow
        */
        for($i=0;$i<5;$i++) {
            $user = new User();
            $user->name = 'example'.$i;
            $user->email = 'example'.$i.'@example.com';
            $user->password = bcrypt('1');
            $user->id_role = rand(1,2);
            $user->save();
        }
        /**
         * Dodanie firmy
         */
        for ($i=0;$i<2;$i++) {
            if($i == 0) {
                $company = new Company();
                $company->name = 'VirtualPeople';
                $company->vat_number = '0000000000';
                $company->email = 'company'.$i.'@example.com';
                $company->address = 'Kurkowa 5/U2';
                $company->phone = '519192748';//rand(500000000, 900000000);
                $company->postalcode = '70-535';
                $company->city = 'Szczecin';
                $company->id_country = 1;
                $company->save();
            } else {
                $company = new Company();
                $company->name = 'example'.$i;
                $company->vat_number = '0000000000';
                $company->email = 'company'.$i.'@example.com';
                $company->address = 'address '.$i;
                $company->phone = rand(500000000, 900000000);
                $company->postalcode = '';
                $company->city = '';
                $company->id_country = 1;
                $company->save();
            }
        }
        /**
         * Dodanie pracowników
         */
         for ($i=0;$i<2;$i++) {
             $employee = new Employee();
             if($i == 0) {
                $employee->firstname = 'Michal';
                 $employee->lastname = 'M';
                 $employee->id_user =  $i+1;
                 $employee->id_company = rand(1, 2);
                 $employee->email = 'michal.jendraszczyk@gmail.com';
                 $employee->phone = '794746611';
             } else {
                 $employee->firstname = 'Firstname '.$i;
                 $employee->lastname = 'Lastname '.$i;
                 $employee->id_user =  $i+1;
                 $employee->id_company = rand(1, 2);
                 $employee->email = 'employee'.$i.'@example.com';
                 $employee->phone = rand(500000000, 900000000);
             }
            $employee->save();
         }
        /**
         * Kategorie
         */
        $categories = [
            'Zabieg opalania natryskowego',
            'Dojazd do klienta',
            'Karnety',
        ];
        foreach($categories as $category) {
            $cs = new ServiceCategory();
            $cs->name = $category;
            $cs->save();
        }

        /**
         * Usługi
         */
        $services = [
            'Opalanie połowy ciała',
            'Opalanie twarzy, dekoltu, rąk',
            'Opalanie całego ciała',
            'Opalanie twarz i dekoltu',
            'Na terenie miasta',
            'Poza miastem',
            '4 zabigi',
            '6 zabigi',
            '8 zabigi'

        ];
        /**
         * Usługi
         */
        foreach($services as $service) {
            $s = new Service();
            $s->id_service_category = rand(1,count($categories));
            $s->name = $service;
            $s->save();


            $sp = new ServicePrice();
            $sp->id_service = $s->id_service;
            $sp->id_user = 1;
            $sp->price = rand(50,300);
            $sp->time = '0'.rand(1,4).':00:00';
            $sp->save();
        }

        /**
         * Klient
         */
        for ($i=0;$i<5;$i++) {
            $customer = new Customer();
            
            if($i == 0) {
            $customer->firstname = 'Michal';
            $customer->lastname = 'J';
            $customer->phone = '796257497';
            $customer->email = 'tasilvar@interia.eu';
            } else {
            $customer->firstname = 'Klient firstname '.$i;
            $customer->lastname = 'Klient lastname '.$i;
            $customer->phone = $i.'00000000';//rand(500000000,899999999);
            $customer->email = 'customer'.$i.'@example.com';
        }
            $customer->save();
        }

        /**
         * Kalendarz
         */
        $callendar = new Callendar();
        $callendar->id_user = '1';
        $callendar->save();

        /**
         * Wizyty
         */
        for($i=0;$i<10;$i++) {
            $hours = rand(1,4).':00:00';
            $dateFrom = date('Y-m-d H:i:s',strtotime('2021-06-'.rand(20,30).' '.rand(0,23).':'.rand(0,59).':00'));
            $confirm = rand(0,2);
            $visit = new Visit();
            $visit->id_callendar = '1';
            $visit->id_customer = rand(1,5);

            if($confirm == 0) {
                $kolor = 'red';
            } else if($confirm == 2) {
                $kolor = 'grey';
            } else {
                $kolor = 'green';
            }
            $visit->color = $kolor;
            $visit->datetime_from = $dateFrom;
            $visit->datetime_to = date('Y-m-d H:i:s',strtotime($dateFrom.' +'.$hours.'hours'));
            $visit->confirm = $confirm;
            $visit->id_service = rand(1,5);
            $visit->hours = $hours;
            $visit->save();
        }
    }
}
