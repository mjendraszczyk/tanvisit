<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create('companies', function (Blueprint $table) {
    $table->bigIncrements('id_company');
    $table->string('name')->nullable();
    $table->string('vat_number')->nullable();
    $table->string('email')->nullable();
    $table->string('address')->nullable();
    $table->string('phone')->nullable();
    $table->string('postalcode')->nullable();
    $table->string('city')->nullable();
    $table->integer('id_country')->nullable();
    $table->text('description')->nullable();
    $table->string('facebook')->nullable();
    $table->string('instagram')->nullable();
    $table->integer('certificate')->default(0)->nullable();
    $table->integer('store')->default(0)->nullable();
    $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
