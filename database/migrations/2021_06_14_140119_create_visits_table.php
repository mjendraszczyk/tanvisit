<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits', function (Blueprint $table) {
            $table->bigIncrements('id_visit');
            $table->integer('id_customer')->nullable();
            $table->integer('id_callendar')->nullable();
            $table->integer('id_service')->nullable();
            $table->integer('confirm')->default(0);
            $table->string('color')->nullable();
            $table->time('hours')->nullable();
            $table->integer('id_employee')->nullable();
            $table->dateTimeTz('datetime_from')->nullable();
            $table->dateTimeTz('datetime_to')->nullable();
            $table->integer('mail')->default(0);
            $table->text('private_note')->nullable();
            $table->bolean('valid')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits');
    }
}
